﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
namespace CCOA.Ctrl.SelectDepts
{
    public partial class SelectDept_zTree1 : System.Web.UI.Page
    {
        public string JsonTree
        {
            get
            {
                string sSql = "select No,Name,ParentNo from Port_Dept order by Idx";
                DataTable dt = null;
                if (BP.WF.Glo.OSModel == BP.Sys.OSModel.OneMore)
                    dt = CCPortal.DA.DBAccess.RunSQLReturnTable(sSql);
                else
                    dt = BP.DA.DBAccess.RunSQLReturnTable(sSql);
                if (dt == null) return String.Empty;
                StringBuilder sb = new StringBuilder();
                foreach (DataRow dr in dt.Rows)
                {
                    if (sb.Length > 0) sb.Append(",");
                    string f = String.Format("id: \"{0}\", pId: \"{2}\", name: \"{1}\", open: true {3} ", dr["No"], dr["Name"], dr["ParentNo"], String.Format(",{0},", this.rIn).Contains(String.Format(",{0},", dr["No"])) ? ",checked:true" : "");
                    sb.Append("{" + f + "}\r\n");
                }
                return sb.ToString();
            }
        }
        public string rIn
        {
            get
            {
                if (this.Request.QueryString["In"] == null || String.IsNullOrEmpty(this.Request.QueryString["In"].ToString()))
                    return "";
                else
                    return Convert.ToString(this.Request.QueryString["In"]);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

            }
        }
    }
}