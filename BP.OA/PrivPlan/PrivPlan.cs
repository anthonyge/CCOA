﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;              
using BP.En;
using BP.Web;

namespace BP.OA.PrivPlan
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class PrivPlanAttr : EntityOIDAttr
    {
        public const String FK_UserNo = "FK_UserNo";
        public const String AddTime = "AddTime";
        public const String PlanDate = "PlanDate";
        public const String Score = "Score";
        public const String Checked = "Checked";
        public const String CheckTime = "CheckTime";
        public const String MyDoc = "MyDoc";

    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class PrivPlan : EntityOID
    {
        #region 属性
        public string FK_UserNo
        {
            get { return this.GetValStrByKey(PrivPlanAttr.FK_UserNo); }
            set { this.SetValByKey(PrivPlanAttr.FK_UserNo, value); }
        }
        public DateTime AddTime
        {
            get { return this.GetValDateTime(PrivPlanAttr.AddTime); }
            set { this.SetValByKey(PrivPlanAttr.AddTime, value); }
        }
        public String PlanDate
        {
            get { return this.GetValStrByKey(PrivPlanAttr.PlanDate); }
            set { this.SetValByKey(PrivPlanAttr.PlanDate, value); }
        }
        public int Score
        {
            get { return this.GetValIntByKey(PrivPlanAttr.Score); }
            set { this.SetValByKey(PrivPlanAttr.Score, value); }
        }
        public bool Checked
        {
            get { return this.GetValBooleanByKey(PrivPlanAttr.Checked); }
            set { this.SetValByKey(PrivPlanAttr.Checked, value); }
        }
        public DateTime CheckTime
        {
            get { return this.GetValDateTime(PrivPlanAttr.CheckTime); }
            set { this.SetValByKey(PrivPlanAttr.CheckTime, value); }
        }
        public string MyDoc
        {
            get { return this.GetValStrByKey(PrivPlanAttr.MyDoc); }
            set { this.SetValByKey(PrivPlanAttr.MyDoc, value); }
        }
        #endregion
        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public PrivPlan() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public PrivPlan(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_PrivPlan";
                map.EnDesc = "个人计划";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBString(PrivPlanAttr.FK_UserNo, null, "用户编号", true, false, 2, 50, 20);
                map.AddTBString(PrivPlanAttr.PlanDate, null, "所在日期", true, false, 2, 20, 20);
                map.AddTBDateTime(PrivPlanAttr.AddTime, null, "添加时间", true, false);
                map.AddTBString(PrivPlanAttr.Score, "0", "分数", true, false, 0, 50, 50);
                map.AddBoolean(PrivPlanAttr.Checked, false, "是否总结", true, false, false);
                map.AddTBDateTime(PrivPlanAttr.CheckTime, null, "总结时间", true, false);
                map.AddTBStringDoc(PrivPlanAttr.MyDoc, null, "计划简介", true, false);

                //map.AddTBString(PrivPlanAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(PrivPlanAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class PrivPlans : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new PrivPlan();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public PrivPlans()
        {
        }
    }
}
