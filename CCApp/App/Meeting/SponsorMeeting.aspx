﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SponsorMeeting.aspx.cs"
    Inherits="CCOA.App.Meeting.SponsorMeeting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>会议发起</title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/Validform_v5.3.2_min.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDrag.js" type="text/javascript"></script>
    <link href="../../Ctrl/kindeditor/themes/default/default.css" rel="stylesheet" type="text/css" />
    <script src="../../Ctrl/kindeditor/kindeditor-min.js" type="text/javascript"></script>
    <script src="../../Ctrl/kindeditor/lang/zh_CN.js" type="text/javascript"></script>
    <style type="text/css">
        .table td
        {
            text-align: left;
        }
        .table input[readonly], .table textarea[readonly]
        {
            background-color: #EEEEEE;
        }
        .table input, .table select
        {
            width: 300px;
        }
        .div
        {
            float: left;
            margin-right: 5px;
        }
        .table th
        {
            width: 120px;
            text-align: right;
            padding-right: 10px;
        }
        .table textarea
        {
            width: 300px;
            height: 100px;
        }
    </style>
    <script type="text/javascript">
        var editor;
        KindEditor.ready(function (K) {
            editor = K.create('#editor_id', {
                resizeType: 1,
                allowPreviewEmoticons: false,
                allowImageUpload: false,
                items: [
						'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
						'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
						'insertunorderedlist', '|', 'emoticons', 'image', 'link']
            });
        });

        function openDialog(e) {
            var diag = new Dialog();
            diag.Width = 800;
            diag.Height = 400;
            diag.Title = "内容页为外部连接的窗口";
            diag.URL = e;
            diag.OKEvent = function () {
                var strVal = diag.innerFrame.contentWindow.GetText()
                var strs = strVal.split(';');
                $("#Fk_SponsorOID").val(strs[0]);
                $("#Title").val(strs[1]);
                $("#BeginTime").val(strs[8]);
                $("#EndTime").val(strs[9]);
                $("#MeetingRoomName").val(strs[6]);
                $("#MeetingRoomNo").val(strs[7]);
                $("#ResourceName").val(strs[10]);
                $("#MTResource").val(strs[11]);
                diag.close();
            };
            diag.show();
        }
        //添加与会人员
        function openDialog2(e) {
            var diag = new Dialog();
            diag.Width = 630;
            diag.Height = 400;
            diag.Title = "选择人员";
            diag.URL = "../../Ctrl/SelectUsers/SelectUser_Jq.aspx";
            diag.OKEvent = function () {
                var ddl = diag.innerFrame.contentWindow.document.getElementById('lbRight');
                var resultName = "";
                var resultNo = ",";
                for (i = 0; i < ddl.options.length; i++) {
                    resultName += ddl.options[i].text + ",";
                    resultNo += ddl.options[i].value + ",";
                }
                if (e == 1) {
                    $("#ParticipantsEmpNo").val(resultNo);
                    $("#ParticipantsEmpName").val(resultName);
                }
                else {
                    $("#MeetingRecorder").val(resultNo);
                    $("#MeetingRecorderName").val(resultName);
                }
                diag.close();
            };
            diag.show();
        }
        //添加会议室资源
        function openDialog3() {
            var diag = new Dialog();
            diag.Width = 800;
            diag.Height = 400;
            diag.Title = "内容页为外部连接的窗口";
            diag.URL = "ResSelect.aspx";
            diag.OKEvent = function () {
                var result1 = "", result2 = "";
                var ddl = diag.innerFrame.contentWindow.document.getElementsByName('cbNo');
                $(ddl).each(function () {
                    if (this.checked == true) {
                        result1 += this.value + ",";
                        result2 += this.title + ",";
                    }
                    $("#MTResource").val(result1);
                    $("#ResourceName").val(result2);
                });
                diag.close();
            };
            diag.show();
        }
        //初始化表单
        $(document).ready(function () {
            $(".registerform").Validform({
                tiptype: 2,
                btnSubmit: "#btn_sub",
                showAllError: true
            });
        });

        //提交表单
        function DoSubmit() {
            editor.sync();
            $("#form1").submit();
        }
        //刷新父窗体
        function RefreshParent() {
            window.parent.RefreashOrdingGrid();
        }
    </script>
</head>
<body>
    <form id="form1" enctype="multipart/form-data" class="registerform" action="?" method="post"
    runat="server">
    <table class="table">
        <tr>
            <th>
                会议主题：
            </th>
            <td>
                <div class="div">
                    <input type="text" readonly="readonly" name="Title" id="Title" datatype="*" nullmsg="会议主题不能为空！"
                        value="<%=Ording==null?string.Empty:Ording.Title%>" />
                </div>
                <%--<div><a href="#" onclick="openDialog('selectording.aspx')">从预定中选择</a></div>--%>
                <div style="color: Red" class="div">
                    <span class="Validform_checktip"><span>*</span></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                会议类型：
            </th>
            <td>
                <select name="Type">
                    <%GetOptions(mtypes);%>
                </select>
            </td>
        </tr>
        <tr>
            <th>
                开始时间：
            </th>
            <td>
                <input type="text" readonly="readonly" id="BeginTime" name="BeginTime" value="<%=Ording==null?string.Empty:Ording.DateFrom.ToString()%>" />
            </td>
        </tr>
        <tr>
            <th>
                会议结束时间：
            </th>
            <td>
                <input type="text" readonly="readonly" id="EndTime" name="EndTime" value="<%=Ording==null?string.Empty:Ording.DateTo.ToString()%>" />
            </td>
        </tr>
        <tr>
            <th>
                主持人：
            </th>
            <td>
                <input type="text" name="CompereEmpNo" />
            </td>
        </tr>
        <tr>
            <th>
                主办部门：
            </th>
            <td>
                <select name="SponsorDeptNo">
                    <%GetOptions(depts);%>
                </select>
            </td>
        </tr>
        <tr>
            <th>
                与会人员：
            </th>
            <td>
                <textarea name="ParticipantsEmpName" id="ParticipantsEmpName" readonly="readonly"></textarea>
                <a href="javascript:openDialog2(1)">添加</a>
                <input type="hidden" id="ParticipantsEmpNo" name="ParticipantsEmpNo" value="" />
            </td>
        </tr>
        <tr>
            <th>
                会议记录员：
            </th>
            <td>
                <input type="text" readonly="readonly" name="MeetingRecorderName" id="MeetingRecorderName" />
                <a href="javascript:openDialog2(2)">添加</a>
                <input type="hidden" name="MeetingRecorder" id="MeetingRecorder" value="" />
            </td>
        </tr>
        <tr>
            <th>
                会议室：
            </th>
            <td>
                <input type="text" readonly="readonly" id="MeetingRoomName" value="<%=Ording==null?string.Empty:Ording.Fk_RoomName.ToString()%>" />
                <input type="hidden" value="<%=Ording==null?string.Empty:Ording.Fk_Room.ToString()%>"
                    name="MeetingRoomNo" id="MeetingRoomNo" />
            </td>
        </tr>
        <tr>
            <th>
                会议资源：
            </th>
            <td>
                <textarea id="ResourceName" name="ResourceName" readonly="readonly"><%=Ording == null ? string.Empty : GetResName(Ording.MTResource)%></textarea>
                <input type="hidden" value="<%=Ording==null?string.Empty:Ording.MTResource%>" id="MTResource"
                    name="MTResource" />
            </td>
        </tr>
        <tr>
            <th>
                会议内容：
            </th>
            <td style="clear: both">
                <div>
                    <textarea id="editor_id" name="content" style="height: 300px; width: 100%; visibility: hidden;"
                        datatype="*" nullmsg="请输入会议内容！"></textarea>
                </div>
                <div style="color: Red">
                    <span class="Validform_checktip"><span>*</span></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                附件：
            </th>
            <td>
                <input type="file" name="Attachment" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <input type="button" value="提交" style="width: 110px" onclick="DoSubmit()" id="btn_sub" />
            </td>
        </tr>
    </table>
    <input type="hidden" value="<%=Ording==null?string.Empty:Ording.OID.ToString()%>"
        name="Fk_SponsorOID" id="Fk_SponsorOID" />
    </form>
    <iframe name="iframe1" id="iframe1" style="display: none" />
</body>
</html>
