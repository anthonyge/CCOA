﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Message/Site1.Master" AutoEventWireup="true"
    CodeBehind="RecycleBox.aspx.cs" Inherits="CCOA.App.Message.RecycleBox" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../CSS/GridviewPager.css" rel="stylesheet" type="text/css" />
    <script src="/js/zDialog/zDialog.js" type="text/javascript"></script>
    <script type="text/javascript">
        function checkAll() {
            //$(".checkdelete").attr("checked", $("#checkall").attr("checked") == "checked");
            $(".checkdelete").prop("checked", $("#checkall").prop("checked"));
        }
        function checkHasSel(oper) {
            var selCount = 0;
            $(".checkdelete").each(function () {
                if ($(this).prop("checked") == true) selCount++;
            });
            if (selCount == 0) {
                alert("你没有选择记录");
                return false;
            }
            else {
                if (oper == 1)
                    return confirm("你真要删除你所选择的邮件吗？");
                else
                    return confirm("你真要恢复你所选择的邮件吗？");
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div>
        <div style="text-align: right; margin-right: 50px;">
            <asp:Button ID="btnRestore" runat="server" Text="恢复" OnClientClick="return checkHasSel(2)"
                OnClick="btnRestore_Click" />
            <asp:Button ID="btnEmptyBox" runat="server" Text="清空垃圾箱" OnClientClick="return confirm('你真的要清空垃圾箱吗？')"
                OnClick="btnEmptyBox_Click" />
            <asp:Button ID="btnDeleteMsg" runat="server" Text="删除所选信息" OnClientClick="return checkHasSel(1)"
                OnClick="btnDeleteMsg_Click" />
            输入关键字:<asp:TextBox ID="txtKeywords" runat="server"></asp:TextBox>
            <asp:Button ID="btnSearchByKey" runat="server" Text="查询" OnClick="btnSearchByKey_Click" />
        </div>
        <asp:GridView ID="gridData" runat="server" CssClass="grid" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="全选" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                    <HeaderTemplate>
                        <input type="checkbox" title='全选' id="checkall" onclick='checkAll()' />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <input type="checkbox" name="chkSel" value='<%# Eval("No") %>' class="checkdelete" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="标记" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        <img style="height: 12px;" src="img/Attach.png" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a href="#" title="有附件" style="display: <%# String.IsNullOrEmpty(Convert.ToString(Eval("AttachFile")))?"none":""%>">
                            <img alt="有附件" style="height: 12px;" src="<%# String.IsNullOrEmpty(Convert.ToString(Eval("AttachFile")))?"":"Img/Attach.png" %>" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="主题">
                    <ItemTemplate>
                        <a style="font-weight: bold; color: #444;" href="#" title="恢复后才可查看">
                            <%#GetContentStr(Eval("Title")) %></a></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="邮件类别" ItemStyle-Width="100px">
                    <ItemTemplate>
                        <%#GetFromType(Eval("FromType"))%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="删除人" ItemStyle-Width="100px">
                    <ItemTemplate>
                        <%#this.GetUserNames(Eval("FK_UserNo"))%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="创建时间" ItemStyle-Width="120px">
                    <ItemTemplate>
                        <%#Eval("RPT") %></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
         <table cellpadding="0" cellspacing="0" align="center" width="99%" class="border">
        <tr>
            <td align="left" colspan="2">
                <webdiyer:AspNetPager ID="AspNetPager1" CssClass="paginator" CurrentPageButtonClass="cpb"
                    runat="server" AlwaysShow="false" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                     PrevPageText="上一页" ShowCustomInfoSection="Left" ShowInputBox="Never"
                    OnPageChanged="AspNetPager1_PageChanged" CustomInfoTextAlign="Left" LayoutType="Table">
                </webdiyer:AspNetPager>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>
