﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace CCOA.Ctrl.SelectDepts
{
    public partial class SelectDept : System.Web.UI.Page
    {
        private DataTable GetAllDepts()
        {
            return BP.OA.GPM.GetAllDepts_By_Tree().Copy();
        }
        private string rIn
        {
            get { return Convert.ToString(this.Request.QueryString["In"]); }
        }
        public String GetSelected(object evalValue)
        {
            if (String.IsNullOrEmpty(this.rIn)) return "";
            bool selected = String.Format(",{0},", this.rIn).Contains(String.Format(",{0},", Convert.ToString(evalValue)));
            return selected ? "checked='checked'" : "";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                DataTable dt = this.GetAllDepts();
                dt.Columns.Add("Text");
                foreach (DataRow dr in dt.Rows)
                {
                    string[] arr_v=Convert.ToString(dr["Name"]).Split(new String[]{"├"},StringSplitOptions.RemoveEmptyEntries);
                    dr["Text"] = arr_v[arr_v.Length - 1];
                }
                this.rptDepts.DataSource = dt;
                this.rptDepts.DataBind();
            }
        }
    }
}