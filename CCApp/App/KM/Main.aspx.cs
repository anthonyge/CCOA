﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.KM
{
    public partial class Main : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BP.KM.Tree tree = new BP.KM.Tree();
            tree.CheckPhysicsTable();

            BP.KM.FileInfo fileInfo = new BP.KM.FileInfo();
            fileInfo.CheckPhysicsTable();
        }
    }
}