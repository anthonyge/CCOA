using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.OA.Market
{
    public enum XMLX
    {
        a1 = 1,
        a2 = 2,
        a3 = 3
    }
    public enum GCLB
    {
        a1 = 1,
        a2 = 2,
        a3 = 3
    }
    /// <summary>
    /// 投标
    /// </summary>
    public class ProjectAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 标题
        /// </summary>
        public const string Title = "Title";
        /// <summary>
        /// 项目编号
        /// </summary>
        public const string Xmbh = "Xmbh";
        /// <summary>
        /// 项目名称
        /// </summary>
        public const string Xmmc = "Xmmc";
        /// <summary>
        /// 项目类型
        /// </summary>
        public const string XMLX = "XMLX";
        /// <summary>
        /// 工程类别
        /// </summary>
        public const string GCLB = "GCLB";


    }
    /// <summary>
    ///  投标实体类
    /// </summary>
    public class Project : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get
            {
                return this.GetValStrByKey(ProjectAttr.Title);
            }
            set
            {
                this.SetValByKey(ProjectAttr.Title, value);
            }
        }
     
        #endregion 属性

        #region 权限控制属性.
        
        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 投标信息
        /// </summary>
        public Project()
        {
        }
        /// <summary>
        /// 投标信息
        /// </summary>
        /// <param name="_No"></param>
        public Project(string _No) : base(_No) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        #endregion 重写方法

        /// <summary>
        /// 投标信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_Project");
                map.EnDesc = "投标登记与查询";
                //map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //四位编号0001开始
                map.AddTBStringPK(ProjectAttr.No, null, "编号", false, true, 3, 3, 3);
                map.AddTBString(ProjectAttr.Xmbh,null,"项目编号",true,false,0,10,3);
                map.AddTBString(ProjectAttr.Name, null, "项目名称", true, false, 0, 10, 3);
                map.AddDDLSysEnum(ProjectAttr.XMLX, 1, "项目类型", true, true, ProjectAttr.XMLX, "@1=总包@2=设计@3=运营");
                map.AddDDLSysEnum(ProjectAttr.GCLB, 1, "工程类别", true, true, ProjectAttr.GCLB, "@1=矿井@2=选煤@3=矿井总体");

                this._enMap = map;
                return this._enMap;
            }
        }
       
    }
    /// <summary>
    /// 投标信息
    /// </summary>
    public class Projects : SimpleNoNames
    {
        /// <summary>
        /// 汽车信息s
        /// </summary>
        public Projects() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Project();
            }
        }
    }
}
