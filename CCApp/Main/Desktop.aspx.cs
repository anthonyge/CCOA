﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace CCOA.Main
{
    public partial class Desktop : System.Web.UI.Page
    {

        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;

        private void Alert(string msg)
        {
            BP.OA.Debug.Alert(this, msg);
        }
        private void SetUserConfigItem(string key, object value)
        {
            BP.OA.UserConfig.SetConfigItem(key, value);
        }
        private String GetUserConfigItem(string key)
        {
            return BP.OA.UserConfig.GetConfigItem(key);
        }
        //private int DisplayRowsCount = 8;
        public bool AllowUserConfig
        {
            get { return Convert.ToBoolean(BP.OA.Main.GetConfigItem("MyDeskTop_AllowUserConfig")); }
        }
        public int RepeatCount
        {
            get
            {
                int c = 0;
                if (this.AllowUserConfig)
                    c = Convert.ToInt32(this.GetUserConfigItem("MyDeskTop_RepeatCount"));
                if (c == 0)
                    return Convert.ToInt32(BP.OA.Main.GetConfigItem("MyDeskTop_RepeatCount"));
                return c;
            }
        }
        public string RepeatColWidth
        {
            get
            {
                string c = null;
                if (this.AllowUserConfig)
                    c = Convert.ToString(this.GetUserConfigItem("MyDeskTop_RepeatColWidth"));
                if (String.IsNullOrEmpty(c))
                    c = Convert.ToString(BP.OA.Main.GetConfigItem("MyDeskTop_RepeatColWidth"));
                return c;
            }
        }
        public string RepeatColHeight
        {
            get
            {
                string c = null;
                if (this.AllowUserConfig)
                    c = Convert.ToString(this.GetUserConfigItem("MyDeskTop_RepeatColHeight"));
                if (String.IsNullOrEmpty(c))
                    c = Convert.ToString(BP.OA.Main.GetConfigItem("MyDeskTop_RepeatColHeight"));
                return c;
            }
        }
        public int RepeatCellCount
        {
            get
            {
                int c = 0;
                if (this.AllowUserConfig)
                    c = Convert.ToInt32(this.GetUserConfigItem("MyDeskTop_RepeatCellCount"));
                if (c == 0)
                    c = Convert.ToInt32(BP.OA.Main.GetConfigItem("MyDeskTop_RepeatCellCount"));
                return c;
            }
        }
        private int Notice_RowsCount
        {
            get
            {
                int c = 0;
                if (this.AllowUserConfig)
                    c = Convert.ToInt32(this.GetUserConfigItem("MyDeskTop_Notice_RowsCount"));
                if (c == 0)
                    c = Convert.ToInt32(BP.OA.Main.GetConfigItem("MyDeskTop_Notice_RowsCount"));
                return c;
            }
        }
        private int Email_RowsCount
        {
            get
            {
                int c = 0;
                if (this.AllowUserConfig)
                    c = Convert.ToInt32(this.GetUserConfigItem("MyDeskTop_Email_RowsCount"));
                if (c == 0)
                    c = Convert.ToInt32(BP.OA.Main.GetConfigItem("MyDeskTop_Email_RowsCount"));
                return c;
            }
        }
        private int News_RowsCount
        {
            get
            {
                int c = 0;
                if (this.AllowUserConfig)
                    c = Convert.ToInt32(this.GetUserConfigItem("MyDeskTop_News_RowsCount"));
                if (c == 0)
                    c = Convert.ToInt32(BP.OA.Main.GetConfigItem("MyDeskTop_News_RowsCount"));
                return c;
            }
        }
        private int ToDo_NewsCount
        {
            get
            {
                int c = 0;
                if (this.AllowUserConfig)
                    c = Convert.ToInt32(this.GetUserConfigItem("MyDeskTop_ToDo_RowsCount"));
                if (c == 0)
                    c = Convert.ToInt32(BP.OA.Main.GetConfigItem("MyDeskTop_ToDo_RowsCount"));
                return c;
            }
        }
        //我的公告-标题状态格式方案
        private string NotReadStateTitleStyle = "";
        private string ReadStateTitleStyle = "font-weight:normal;";
        public string GetTitleFormatByReadState(object evalState)
        {
            if (evalState == null || evalState == Convert.DBNull) return String.Empty;
            int state = Convert.ToInt32(evalState);
            if (state == 0)
                return this.NotReadStateTitleStyle;
            else if (state == 1)
                return this.ReadStateTitleStyle;
            else
                return String.Empty;
        }
        private string GetUserNames(System.Data.DataTable dtEmps, string userNames)
        {
            List<String> l_users = new List<string>();
            foreach (DataRow dr in dtEmps.Rows)
            {
                if (String.Format(",{0},", userNames).Contains(String.Format(",{0},", Convert.ToString(dr["No"]))))
                    l_users.Add(String.Format("{0}", dr["Name"]));
            }
            return String.Join(",", l_users.ToArray());
        }
        private DataTable GetAllEmps()
        {
            return BP.OA.GPM.GetAllEmps();
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.BindData_News(); this.LoadNewsMsg();
                this.BindData_Notice(); this.LoadNoticeMsg();
                this.BindData_ToDo(); this.LoadToDoMsg();
                this.BindData_Email(0); this.LoadEmailLink();
            }
        }
        public string GetNameByUserNo(object eval_UserNo)
        {
            string userNo = Convert.ToString(eval_UserNo);
            return this.GetUserNames(this.GetAllEmps(), userNo);
        }
        private void BindData_Email(int iMode)
        {
            string con = null;
            if (iMode == 1)
            {
                this.lbEmail_NotRead.Font.Underline = true;
                this.lbEmail_All.Font.Underline = this.lbEmail_Read.Font.Underline = false;

                con = " and Received='0'";
            }
            else if (iMode == 2)
            {
                this.lbEmail_Read.Font.Underline = true;
                this.lbEmail_All.Font.Underline = this.lbEmail_NotRead.Font.Underline = false;
                con = " and Received='1'";


            }
            else
            {
                this.lbEmail_All.Font.Underline = true;
                this.lbEmail_Read.Font.Underline = this.lbEmail_NotRead.Font.Underline = false;
            }

            string sSql = String.Format("select top({2}) A.OID as No,B.Title as Name,A.AddTime as RDT,A.Sender as Sender,Received from OA_MessageInBox A  inner join OA_Message B on A.FK_MsgNo=B.OID where FK_ReceiveUserNo='{0}' {1} order by A.OID desc", BP.Web.WebUser.No, con, this.Email_RowsCount);
            if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            {
                sSql = String.Format("select A.OID as No,B.Title as Name,A.AddTime as RDT,A.Sender as Sender,Received from OA_MessageInBox A  inner join OA_Message B on A.FK_MsgNo=B.OID where FK_ReceiveUserNo='{0}' {1} order by A.OID desc LIMIT 0,{2}", BP.Web.WebUser.No, con, this.Email_RowsCount);
            }
            System.Data.DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sSql);
            this.rptEmail.DataSource = dt;
            this.rptEmail.DataBind();
        }
        private void LoadEmailLink()
        {
            string sSql = String.Format("select count(*) from OA_MessageInBox where FK_ReceiveUserNo='{0}' and Received='0'", BP.Web.WebUser.No);
            object oR1 = BP.DA.DBAccess.RunSQLReturnVal(sSql);
            int iR1 = Convert.ToInt32(oR1);
            sSql = String.Format("select count(*) from OA_MessageInBox where FK_ReceiveUserNo='{0}' and Received='1'", BP.Web.WebUser.No);
            object oR2 = BP.DA.DBAccess.RunSQLReturnVal(sSql);
            int iR2 = Convert.ToInt32(oR2);
            this.lbEmail_All.Text = String.Format("所有({0})", iR1 + iR2);
            this.lbEmail_NotRead.Text = String.Format("未读({0})", iR1);
            this.lbEmail_Read.Text = String.Format("已读({0})", iR2);
        }
        private void BindData_News()
        {
            string sql = String.Format("select top({0}) OID as No,Title as Name,AddTime as RDT,BrowseCount,SetTop from OA_Article order by Addtime desc", this.News_RowsCount);
            if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            {
                sql = String.Format("select OID as No,Title as Name,AddTime as RDT,BrowseCount,SetTop from OA_Article order by Addtime desc LIMIT 0,{0}", this.News_RowsCount);
            }
            this.rptNews.DataSource = BP.DA.DBAccess.RunSQLReturnTable(sql);
            this.rptNews.DataBind();
        }
        private void LoadNewsMsg()
        {
            string sql = String.Format("select Count(*) from OA_Article Where Convert(char(10),Cast(Addtime as DateTime),120)=Convert(char(10),GetDate(),120)");
            if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            {
                sql = String.Format("select Count(*) from OA_Article Where DATE_FORMAT(Addtime,'%Y-%m-%d')=CURDATE()");
            }
            object oR = BP.DA.DBAccess.RunSQLReturnString(sql);
            int iR = Convert.ToInt32(oR);
            if (iR == 0)
            {
                this.lblNews_Msg.Text = "今日还没有新闻！";
                this.lblNews_Msg.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lblNews_Msg.Text = String.Format("今日最新新闻{0}条", iR);
                this.lblNews_Msg.ForeColor = System.Drawing.Color.Red;
            }
        }
        private void BindData_Notice()
        {
            //string sql = "select top(10) A.OID as No,A.Title as Name,A.RDT as RDT"
            //              +",(select cast(count(*) as varchar) from OA_NoticeReader where FK_NoticeNo=A.OID and ReadTime<>'')"
            //              +" +'/'"
            //              +" +(select cast(count(*) as varchar) from OA_NoticeReader where FK_NoticeNo=A.OID)"
            //              +" as BrowseCount"
            //              +" from OA_Notice A order by A.RDT desc";
            string sSql = String.Format("select Top({1}) A.FK_UserNo as UserName,FK_Dept as Dept,A.OID as No,A.Title as Name,B.Name as NoticeCategory,RDT,C.Lab as Importance,NoticeSta,d.Lab as NoticeStaTitle,SetTop"
                                 + ",(select cast(count(*) as varchar) from OA_NoticeReader where FK_NoticeNo=A.OID and ReadTime<>'')"
                                 + "      +'/'+(select cast(count(*) as varchar) from OA_NoticeReader where FK_NoticeNo=A.OID)"
                                 + "         as ReadList"
                                 + " ,case when E.ReadTime='' then '0' else '1' end as ReadState"
                                 + ",case when NoticeSta=0 and GetDate() between StartTime and StopTime then cast(DateDiff(d,GetDate(),StopTime) as varchar)+'天后关闭'"
                                 + "        when NoticeSta=0 and GetDate() < StartTime then cast(DateDiff(d,GetDate(),StartTime) as varchar)+'天后打开'"
                                 + "        when NoticeSta=0 and GetDate() > StopTime then '已过期'+cast(DateDiff(d,StopTime,GetDate()) as varchar)+'天'"
                                 + "        when NoticeSta=1 then '手工打开'"
                                 + "        when NoticeSta=2 then '手工关闭'  End as PublishState"
                                 + " from OA_Notice A"
                                 + " inner join OA_NoticeCategory B on B.No=A.FK_NoticeCategory"
                                 + " inner join OA_NoticeReader E on E.FK_NoticeNo=A.OID"
                                 + " inner join Sys_Enum C on C.IntKey=A.Importance and C.EnumKey='Importance'"
                                 + " inner join Sys_Enum D on D.IntKey=A.NoticeSta and D.EnumKey='NoticeSta'"
                //+ " where E.FK_UserNo='{0}'", BP.Web.WebUser.No);
                                 + "   where E.FK_UserNo='{0}' and A.NoticeSta=1"
                                 + "      or E.FK_UserNo='{0}' and A.NoticeSta=0"
                                 + "         and GetDate() between A.StartTime and A.StopTime order by ReadState,SetTop desc,OID"
                                 , BP.Web.WebUser.No, this.Notice_RowsCount);
            if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            {
                sSql = String.Format("select A.FK_UserNo as UserName,FK_Dept as Dept,A.OID as No,A.Title as Name,B.Name as NoticeCategory,RDT,C.Lab as Importance,NoticeSta,d.Lab as NoticeStaTitle,SetTop,"
                                    + "CONVERT(CONCAT((select count(*) from OA_NoticeReader where FK_NoticeNo=A.OID and ReadTime<>''),'/',(select count(*) from OA_NoticeReader where FK_NoticeNo=A.OID))  USING gb2312) as ReadList,"
                                    + "case when E.ReadTime='' then '0' else '1' end as ReadState,case "
                                    + " when NoticeSta=0 and CURDATE() between StartTime and StopTime then CONCAT(DateDiff(CURDATE(),StopTime),'天后关闭')"
                                    + " when NoticeSta=0 and CURDATE() < StartTime then CONCAT(DateDiff(CURDATE(),StartTime),'天后打开')"
                                    + " when NoticeSta=0 and CURDATE() > StopTime then '已过期'+CONCAT(DateDiff(StopTime,CURDATE()),'天')"
                                    + " when NoticeSta=1 then '手工打开'"
                                    + " when NoticeSta=2 then '手工关闭'  End as PublishState"
                                    + " from OA_Notice A inner join OA_NoticeCategory B on B.No=A.FK_NoticeCategory inner join OA_NoticeReader E on E.FK_NoticeNo=A.OID "
                                    + " inner join Sys_Enum C on C.IntKey=A.Importance and C.EnumKey='Importance' inner join Sys_Enum D on D.IntKey=A.NoticeSta and D.EnumKey='NoticeSta'"
                                    + " where E.FK_UserNo='{0}' and A.NoticeSta=1 or E.FK_UserNo='{0}' and A.NoticeSta=0"
                                    + " and CURDATE() between A.StartTime and A.StopTime"
                                    + " order by ReadState,SetTop desc,OID LIMIT 0,{1}", BP.Web.WebUser.No, this.Notice_RowsCount);
            }
            this.rptNotice.DataSource = BP.DA.DBAccess.RunSQLReturnTable(sSql);
            this.rptNotice.DataBind();
        }
        private void LoadNoticeMsg()
        {
            string sSql = String.Format("select count(*) from OA_NoticeReader where FK_UserNo='{0}' and ReadTime=''", BP.Web.WebUser.No);
            object oR1 = BP.DA.DBAccess.RunSQLReturnVal(sSql);
            int iR1 = Convert.ToInt32(oR1);
            if (iR1 == 0)
            {
                this.lblNotice_Msg.Text = String.Format("目前公告你都已经阅读！", iR1);
                this.lblNotice_Msg.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lblNotice_Msg.Text = String.Format("你还有{0}条公告没有阅读！", iR1);
                this.lblNotice_Msg.ForeColor = System.Drawing.Color.Red;
            }
        }
        private void BindData_ToDo()
        {
            string sSql = String.Format("SELECT top({1}) RDT,WorkID as No,FlowName+'-'+Title as Name,FK_Flow,FK_Node,FID,AtPara FROM WF_EmpWorks WHERE FK_Emp='{0}' AND WFState=2 order by RDT desc", BP.Web.WebUser.No, this.ToDo_NewsCount);
            if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            {
                sSql = String.Format("SELECT RDT,WorkID as No,FlowName+'-'+Title as Name,FK_Flow,FK_Node,FID,AtPara FROM WF_EmpWorks WHERE FK_Emp='{0}' AND WFState=2 order by RDT desc LIMIT 0,{1}", BP.Web.WebUser.No, this.ToDo_NewsCount);
            }
            this.rptToDo.DataSource = CCPortal.DA.DBAccess.RunSQLReturnTable(sSql);
            this.rptToDo.DataBind();
        }
        /// <summary>
        /// 获取待办
        /// </summary>
        private void LoadToDoMsg()
        {
            string sql = String.Format("SELECT Count(*) FROM WF_EmpWorks WHERE FK_Emp='{0}' AND (WFState=2 OR WFState=5 OR WFState=6 OR WFState=8 OR WFState=9)", BP.Web.WebUser.No);
            object oR = BP.DA.DBAccess.RunSQLReturnString(sql);
            int iR = Convert.ToInt32(oR);
            if (iR == 0)
            {
                this.lblToDo_Msg.Text = "你的工作都已经完毕！";
                this.lblToDo_Msg.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lblToDo_Msg.Text = String.Format("你有待办{0}项", iR);
                this.lblToDo_Msg.ForeColor = System.Drawing.Color.Red;
            }
        }
        #endregion

        #region 3.页面事件(Page Event)
        protected void lbEmail_All_Click(object sender, EventArgs e)
        {
            this.BindData_Email(0);
        }

        protected void lbEmail_NotRead_Click(object sender, EventArgs e)
        {
            this.BindData_Email(1);
        }

        protected void lbEmail_Read_Click(object sender, EventArgs e)
        {
            this.BindData_Email(2);
        }
        #endregion 3.页面事件(Page Event)
    }
}