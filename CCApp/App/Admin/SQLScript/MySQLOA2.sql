﻿
DROP FUNCTION IF EXISTS GetDate;
--GO--
CREATE FUNCTION GetDate() 
  RETURNS varchar(1000)
BEGIN
 RETURN DATE_FORMAT(NOW(),'%Y-%m-%d-%H:%i:%s');
END;

--GO--

DROP PROCEDURE IF EXISTS SYST_pGetPageRows;
--GO--

CREATE PROCEDURE SYST_pGetPageRows(mSqls varchar(2000),Top int,mOrder varchar(500),nPageSize int,nPageID int)
  BEGIN 
    declare NewSql varchar(2000);
    declare nFrom int default 0;
    declare nTo varchar(2000);
    declare mExsql varchar(2000);
    set nFrom:=(nPageID-1)*nPageSize+1;
    set NewSql:=mSqls;
    set nTo:=nPageID*nPageSize;
    set NewSql:=CONCAT(NewSql,' ',mOrder,' LIMIT ',nFrom,',',nTo);
	EXECUTE NewSql;
END;