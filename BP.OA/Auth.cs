﻿using System;
using System.Security;
using System.Web;
using System.Data;
using System.Web.Security;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.DA;
using BP.Port;
namespace BP.OA
{
    /// <summary>
    ///本系统Auth层接口
    ///默认应用，必须配置Web.Config中的authentication知识树mode为Forms.
    ///用户还可以进行自己开发!
    /// </summary>
    public class Auth
    {
        #region 1.对外API 接口5个（登录验证、存在引用户、登录、验证状态、返回登录页、获取当前用户名）
        public static bool CheckPass(string user, string pwd)
        {
            //Emp emp = new Emp();
            //emp.No = user;
            //if (emp.RetrieveFromDBSources() == 0)
            //{
            //    return false;
            //}
            //if (!emp.Pass.Equals(pwd))
            //{
            //    return false;
            //}
            //else
            //{
            //    BP.Web.WebUser.SignInOfGener(emp);
            //    return true;
            //}

            string sql = String.Format("select No,Pass from Port_Emp Where No='{0}' and Pass='{1}'", user, pwd);
            DataTable dt = CCPortal.DA.DBAccess.RunSQLReturnTable(sql);
            if (dt == null) return false;
            if (dt.Rows.Count == 0) return false;
            DataRow dr = dt.Rows[0];
            if (Convert.ToString(dr["No"]) == user && Convert.ToString(dr["Pass"]) == pwd)
                return true;
            else
                return false;
        }
        public static bool ExistsUser(string user)
        {
            string sql = String.Format("select No from Port_Emp Where No='{0}'", user);
            string no = CCPortal.DA.DBAccess.RunSQLReturnString(sql);
            return !String.IsNullOrEmpty(no);
        }
        /// <summary>
        /// 注册
        /// 目前不支持，放在GPM中
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="email"></param>
        /// <param name="pwd"></param>
        public static int Register(string userName, string email, string pwd)
        {
            return 0;
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="user"></param>
        /// <param name="createPersistStore"></param>
        public static void LoginIn(string user, bool createPersistStore)
        {
            //login 登录WF系统
            Emp emp = new Emp();
            emp.No = user;
            emp.Retrieve();
            BP.Web.WebUser.SignInOfGener(emp, true);
            BP.Web.WebUser.SetSID(BP.Web.WebUser.SID);
            //removed by liuxc,重复登录,日志记录了2次
            //if (emp.RetrieveFromDBSources() == 1)
            //{
            //    BP.Web.WebUser.SignInOfGener(emp, true);
            //}

            ////Login本系统
            //FormsAuthentication.SetAuthCookie(user, true);
        }
        /// <summary>
        /// 验证状态
        /// </summary>
        /// <returns></returns>
        public static bool IsOnline()
        {
            if (string.IsNullOrEmpty(BP.Web.WebUser.No))
                return false;

            return true;

            //return HttpContext.Current.User.Identity.IsAuthenticated;
        }
        /// <summary>
        /// 注销当前用户
        /// </summary>
        /// <param name="user"></param>
        public static void LoginOut()
        {
            //注销WF系统
            BP.Web.WebUser.Exit();

            // FormsAuthentication.SignOut();
        }
        /// <summary>
        /// 登录失败，返回登录页
        /// </summary>
        public static void RedirectLoginUrl()
        {
            string r_url = HttpContext.Current.Request[Redirect_QueryStringKey];
            if (String.IsNullOrEmpty(r_url))
            {
                r_url = Login_Url;
            }
            HttpContext.Current.Response.Redirect(r_url);
        }
        #endregion

        #region 2.内部设置接口2个（登录页地址、返回QueryStringKey）
        /// <summary>
        /// 默认登录页地址
        /// </summary>
        public static string Login_Url
        {
            get
            {
                string url = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["Login_Url"]);
                if (!String.IsNullOrEmpty(url))
                    return url;
                else
                    return "/Main/Login/Login.aspx";
            }
        }
        /// <summary>
        /// 数据库安装页
        /// </summary>
        public static string DBInstall_Url
        {
            get
            {
                string url = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DBInstall_Url"]);
                if (!String.IsNullOrEmpty(url))
                    return url;
                else
                    return "../App/Admin/DBInstall.aspx";
            }
        }
        /// <summary>
        /// 默认QueryStringKey
        /// </summary>
        public static string Redirect_QueryStringKey
        {
            get
            {
                string key = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["Redirect_QueryStringKey"]);
                if (!String.IsNullOrEmpty(key))
                    return key;
                else
                    return "ReturnUrl";
            }
        }
        #endregion
    }
}
