﻿<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeBehind="SelectStation.aspx.cs" Inherits="CCOA.Ctrl.SelectStations.SelectStation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="/Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function getReturnText() {
            return $("#txt_Stations").val();
        }
        function getReturnValue() {
            return $("#txt_StationNames").val();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:CheckBoxList runat="server" ID="cblStations" RepeatLayout="Flow" RepeatDirection="Horizontal"
            RepeatColumns="1" AutoPostBack="true" 
            onselectedindexchanged="cblStations_SelectedIndexChanged">
        </asp:CheckBoxList>
        <asp:HiddenField ID="txt_StationNames" runat="server" />
        <asp:HiddenField ID="txt_Stations" runat="server" />
    </div>
    </form>
</body>
</html>
