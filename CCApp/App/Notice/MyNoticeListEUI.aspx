﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/AppMaster/AppSite.Master" AutoEventWireup="true" CodeBehind="MyNoticeListEUI.aspx.cs" Inherits="CCOA.App.Notice.MyNoteListEUI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        var spantext = $("#span1").text();
        //加载grid后回调函数
        function LoadDataGridCallBack(js, scorp) {
            $("#pageloading").hide();
            if (js == "")
            { js = "[]"; };
            var pushData = eval('(' + js + ')');
            

            $('#newsGrid').datagrid({

                columns: [[
          { field: "OID", title: "编号", width: 80,hidden:true },
           {
               field: 'Title', title: '标题', width: 300, formatter: function (value, rec) {
                   var title = "<a href='NoticeBrowser.aspx?OID=" + rec.OID + "'>" + rec.Title + "</a>";
                   //置顶
                   if (rec.SetTop) {
                       title = "<a href='NoticeBrowser.aspx?OID=" + rec.OID + "'>" + rec.Title + "</a>&nbsp;&nbsp;<img src='img/top.gif' alt='置顶'/>";
                   }
                   return title;
               }
           },
          {
              field: 'UserName', title: '发布者', width: 100, formatter: function (value, rec) {

                  var title = rec.UserName;
                  //置顶
                  if (rec.Dept) {
                      title = rec.Dept;
                  }
                  return title;
              }
          },
          { field: "NoticeCategory", title: "分类", width: 100 },
          { field: "Importance", title: "重要程度", width: 100 },
          {
              field: "ReadState", title: "阅读状态", width: 100,
              formatter: function (value, rec) {
                  var title;
                  if (rec.ReadState == 1) {
                      return title = "已读";
                  }
                  return title = "未读";
              }

              , styler: function (value, row, index) {
                  if (value == 0) {
                      return 'color:red;';
                  }
                  else return 'color:green;';
              }
          },
          {
              field: "RDT", title: "发布时间", width: 80,
              formatter: function (value, rec) {
                  var title;
                  $.ajax({
                      type: "post", //使用GET或POST方法访问后台
                      dataType: "text", //返回json格式的数据
                      contentType: "application/json; charset=utf-8",
                      url: "MyNoticeListEUI.aspx/GetRelativeTime", //要访问的后台地址
                      data: "{'evalTime':" + "'" + rec.RDT + "'}", //要发送的数据
                      async: false,
                      cache: false,
                      error: function (XMLHttpRequest, errorThrown) {
                          $("body").html("<b>访问页面出错，请联系管理员。<b>");
                          //callback(XMLHttpRequest);
                      },
                      success: function (msg) {//msg为返回的数据，在这里做数据绑定
                          return title = msg;
                      }
                  });
                  return title.substring(6, title.length - 2);
              }
          },
          {
              field: "PublishState", title: "发布状态", width: 100
          }

                ]],
                data: pushData,
                width: 'auto',
                height: 'auto',
                striped: true,
                rownumbers: true,
                singleSelect: true,
                pagination: true,
                remoteSort: false,
                fitColumns: true,
                pageNumber: scorp.pageNumber,
                pageSize: scorp.pageSize,
                pageList: [20, 30, 40, 50],
                onDblClickCell: function (index, field, value) {
                    var row = $('#newsGrid').datagrid('getSelected');
                    if (field=="Title") {
                                location.href="NoticeBrowser.aspx?OID=" + row.OID;
                            }
                },
                loadMsg: '数据加载中......'
            });
            //分页
            var pg = $("#newsGrid").datagrid("getPager");
            if (pg) {
                $(pg).pagination({
                    onRefresh: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    },
                    onSelectPage: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    }
                });
            }
        }
        //加载grid
        function LoadGridData(pageNumber, pageSize) {
            var pageNumber = pageNumber;
            var pageSize = pageSize;

            var params = {
                method: "getnewslist"
            };
            queryData(params, LoadDataGridCallBack, this);
        }

        //初始化
        $(function () {
            $("#pageloading").show();
            //InitNewsCatagory();
            LoadGridData(1, 20);
        });
        //公共方法
        function queryData(param, callback, scope, method, showErrMsg) {
            if (!method) method = 'GET';
            $.ajax({
                type: method, //使用GET或POST方法访问后台
                dataType: "text", //返回json格式的数据
                contentType: "application/json; charset=utf-8",
                url: "MyNoticeList.ashx", //要访问的后台地址
                data: param, //要发送的数据
                async: false,
                cache: false,
                complete: function () { }, //AJAX请求完成时隐藏loading提示
                error: function (XMLHttpRequest, errorThrown) {
                    $("body").html("<b>访问页面出错，请联系管理员。<b>");
                    //callback(XMLHttpRequest);
                },
                success: function (msg) {//msg为返回的数据，在这里做数据绑定
                    var data = msg;
                    callback(data, scope);
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="pageloading">
</div>
<div data-options="region:'center'" border="false" style="margin: 0; padding: 0;
        overflow: hidden;">
    <div id="tb" style="padding: 3px;">
    <div style="padding-left: 20px; padding-right: 20px; color: #444; margin-bottom: 2px;">
        <div style="padding: 3px 3px 3px 3px;">
            <asp:Literal runat="server" ID="liMyReadState"></asp:Literal>
        </div>
    </div>
    </div>
    <table id="newsGrid" fit="true" fitcolumns="true" toolbar="#tb" class="easyui-datagrid">
    </table>
</div>
</asp:Content>



