﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShortCols2.ascx.cs" Inherits="CCOA.Ctrl.ShortFunc.ShortCols2" %>
    <div>
        <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSource1" DataKeyNames="No"
        InsertItemPosition="FirstItem" >
        <AlternatingItemTemplate>
            <tr style="">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="删除" OnClientClick="return confirm('你确定要删除此客户分类吗！')" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="编辑" />
                </td>
                <td>
                    <asp:Label ID="NoLabel" runat="server" Text='<%# Eval("No") %>' />
                </td>
                <td>
                    <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </AlternatingItemTemplate>
        <EditItemTemplate>
            <tr style="">
                <td>
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="更新" ValidationGroup="Update" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="取消" />
                </td>
                <td>
                    <asp:Label ID="NoLabel1" runat="server" Text='<%# Eval("No") %>' />
                </td>
                <td>
                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' MaxLength="50" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ValidationGroup="Update"
                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="NameTextBox" Display="Dynamic"
                            ErrorMessage="名称不能为空"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </EditItemTemplate>
        <EmptyDataTemplate>
            <table id="Table1" runat="server" style="border-collapse:collapse;">
                <tr>
                    <td>
                        未返回数据。
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <InsertItemTemplate>
            <tr style="">
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="插入" ValidationGroup="Insert" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="清除" />
                </td>
                <td>
                    <asp:TextBox ID="NoTextBox" runat="server" Text='<%# Bind("No") %>' MaxLength="5" />
                    
                </td>
                <td>
                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' MaxLength="50" />
                </td>
                <td><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="NoTextBox" ValidationGroup="Insert"
                        ErrorMessage="编号不能为空" Display="none"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="NoTextBox"
                        ValidationExpression="\d{1,5}" ErrorMessage="编号必须为数字" Display="none" ValidationGroup="Insert">
                        </asp:RegularExpressionValidator><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="NameTextBox" Display="none" ValidationGroup="Insert"
                            ErrorMessage="名称不能为空"></asp:RequiredFieldValidator>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="true" ShowMessageBox="false" ValidationGroup="Insert" DisplayMode="List" />
                </td>
            </tr>
        </InsertItemTemplate>
        <ItemTemplate>
            <tr style="">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="删除" OnClientClick="return confirm('你真要删除这个客户目录吗！')" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="编辑" />
                </td>
                <td>
                    <asp:Label ID="NoLabel" runat="server" Text='<%# Eval("No") %>' />
                </td>
                <td>
                    <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </ItemTemplate>
        <LayoutTemplate>
            <table id="itemPlaceholderContainer" runat="server" border="1" class="table" cellpadding="3"
                style="border-collapse:collapse;">
                <tr id="Tr1" runat="server" style=" text-align:center;">
                    <td id="Td1" runat="server" style="width: 100px">
                        操作
                    </td>
                    <td id="Td2" runat="server" style="width: 50px;">
                        编号
                    </td>
                    <td id="Td3" runat="server" style="width: 120px;">
                        名称
                    </td>
                    <td id="Td4" runat="server" style="width: 120px;">
                        &nbsp;
                    </td>
                </tr>
                <tr id="itemPlaceholder" runat="server">
                </tr>
            </table>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <tr style="">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="删除" OnClientClick="return confirm('你确定要删除此客户分类吗！')" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="编辑" />
                </td>
                <td>
                    <asp:Label ID="NoLabel" runat="server" Text='<%# Eval("No") %>' />
                </td>
                <td>
                    <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </SelectedItemTemplate>
    </asp:ListView>
    
    <asp:SqlDataSource ID="SqlDataSource1" runat="server">
        <DeleteParameters>
            <asp:Parameter Name="No" Type="String" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="No" Type="String" />
            <asp:Parameter Name="Name" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="No" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
    </div>
