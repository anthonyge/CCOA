﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.Notice
{
    public partial class NoticeListMobile : System.Web.UI.Page
    {
        //权限控制
        private string FuncNo = null;

        private void Alert(string msg)
        {
            BP.OA.Debug.Alert(this, msg);
        }
        //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.BindData_Notice();
            }
        }
        private void BindData_Notice()
        {
            string sSql = String.Format("select Top({1}) A.FK_UserNo as UserName,FK_Dept as Dept,A.OID as No,A.Title as Name,B.Name as NoticeCategory,RDT,C.Lab as Importance,NoticeSta,d.Lab as NoticeStaTitle,SetTop"
                                 + ",(select cast(count(*) as varchar) from OA_NoticeReader where FK_NoticeNo=A.OID and ReadTime<>'')"
                                 + "      +'/'+(select cast(count(*) as varchar) from OA_NoticeReader where FK_NoticeNo=A.OID)"
                                 + "         as ReadList"
                                 + " ,case when E.ReadTime='' then '0' else '1' end as ReadState"
                                 + ",case when NoticeSta=0 and GetDate() between StartTime and StopTime then cast(DateDiff(d,GetDate(),StopTime) as varchar)+'天后关闭'"
                                 + "        when NoticeSta=0 and GetDate() < StartTime then cast(DateDiff(d,GetDate(),StartTime) as varchar)+'天后打开'"
                                 + "        when NoticeSta=0 and GetDate() > StopTime then '已过期'+cast(DateDiff(d,StopTime,GetDate()) as varchar)+'天'"
                                 + "        when NoticeSta=1 then '手工打开'"
                                 + "        when NoticeSta=2 then '手工关闭'  End as PublishState"
                                 + " from OA_Notice A"
                                 + " inner join OA_NoticeCategory B on B.No=A.FK_NoticeCategory"
                                 + " inner join OA_NoticeReader E on E.FK_NoticeNo=A.OID"
                                 + " inner join Sys_Enum C on C.IntKey=A.Importance and C.EnumKey='Importance'"
                                 + " inner join Sys_Enum D on D.IntKey=A.NoticeSta and D.EnumKey='NoticeSta'"
                                 + "   where E.FK_UserNo='{0}' and A.NoticeSta=1"
                                 + "      or E.FK_UserNo='{0}' and A.NoticeSta=0"
                                 + "         and GetDate() between A.StartTime and A.StopTime order by ReadState,SetTop desc,OID"
                                 , BP.Web.WebUser.No, 25);
            if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            {
                sSql = String.Format("select A.FK_UserNo as UserName,FK_Dept as Dept,A.OID as No,A.Title as Name,B.Name as NoticeCategory,RDT,C.Lab as Importance,NoticeSta,d.Lab as NoticeStaTitle,SetTop,"
                                    + "CONVERT(CONCAT((select count(*) from OA_NoticeReader where FK_NoticeNo=A.OID and ReadTime<>''),'/',(select count(*) from OA_NoticeReader where FK_NoticeNo=A.OID))  USING gb2312) as ReadList,"
                                    + "case when E.ReadTime='' then '0' else '1' end as ReadState,case "
                                    + " when NoticeSta=0 and CURDATE() between StartTime and StopTime then CONCAT(DateDiff(CURDATE(),StopTime),'天后关闭')"
                                    + " when NoticeSta=0 and CURDATE() < StartTime then CONCAT(DateDiff(CURDATE(),StartTime),'天后打开')"
                                    + " when NoticeSta=0 and CURDATE() > StopTime then '已过期'+CONCAT(DateDiff(StopTime,CURDATE()),'天')"
                                    + " when NoticeSta=1 then '手工打开'"
                                    + " when NoticeSta=2 then '手工关闭'  End as PublishState"
                                    + " from OA_Notice A inner join OA_NoticeCategory B on B.No=A.FK_NoticeCategory inner join OA_NoticeReader E on E.FK_NoticeNo=A.OID "
                                    + " inner join Sys_Enum C on C.IntKey=A.Importance and C.EnumKey='Importance' inner join Sys_Enum D on D.IntKey=A.NoticeSta and D.EnumKey='NoticeSta'"
                                    + " where E.FK_UserNo='{0}' and A.NoticeSta=1 or E.FK_UserNo='{0}' and A.NoticeSta=0"
                                    + " and CURDATE() between A.StartTime and A.StopTime"
                                    + " order by ReadState,SetTop desc,OID LIMIT 0,{1}", BP.Web.WebUser.No, 25);
            }
            this.rptNotice.DataSource = BP.DA.DBAccess.RunSQLReturnTable(sSql);
            this.rptNotice.DataBind();
        }
    }
}