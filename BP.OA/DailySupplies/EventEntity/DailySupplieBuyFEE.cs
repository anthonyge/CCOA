using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.DS
{
    /// <summary>
    /// 固定资产购买 流程事件实体
    /// </summary>
    public class DailySupplieBuyFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 固定资产购买 流程事件实体
        /// </summary>
        public DailySupplieBuyFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return "DailySupplieBuy"; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            //string sql = "select * from ND1005Dtl1 where  RefPK=" + this.WorkID;
            //DataTable dt = DBAccess.RunSQLReturnTable(sql);
            //foreach (DataRow dr in dt.Rows)
            //{
             //   BP.DS.API.DailySupplie_Buy(
             //       dr["OA_DSMain"].ToString(),
             //       int.Parse(dr["PiZhunShuLiang"].ToString()),
             //       int.Parse(dr["JianYiDanJia"].ToString()),
             //         int.Parse(dr["ShiJiJinE"].ToString()),
             //       this.GetValStr("ShenQingRen"),
             //this.GetValStr("ShuoMing"));
            //}
            return "写入成功....";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
