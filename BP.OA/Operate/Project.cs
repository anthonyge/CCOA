﻿using BP.En;
using BP.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.Operate
{
   /// <summary>
   /// 项目属性
   /// </summary>
    public class ProjectAttr : EntityNoNameAttr
    {
    }

    /// <summary>
    /// 项目实体
    /// </summary>
    public class Project : EntityNoName
    {
        #region 构造方法
        public Project() { }

        public Project(string _No) : base(_No) { }
        #endregion

        public override Map EnMap
        {
            get 
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_Projects");
                map.EnDesc = "项目";
                map.CodeStruct = "3";

                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBStringPK(ProjectAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBString(ProjectAttr.Name, null, "项目名称", true, false, 0, 100, 30);

                this._enMap = map;

                return this._enMap;
            }
        }
    }

    /// <summary>
    /// 项目
    /// </summary>
    public class Projects : EntitiesNoName
    {
        public Projects() { }

        public override Entity GetNewEntity
        {
            get
            {
                return new Project();
            }
        }
    }
    
}
