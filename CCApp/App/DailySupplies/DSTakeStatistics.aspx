﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/AppMaster/AppSite.Master" AutoEventWireup="true"
    CodeBehind="DSTakeStatistics.aspx.cs" Inherits="CCOA.App.DailySupplies.DSTakeStatistics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#newsGrid').datagrid({
                url: '/App/DailySupplies/datagrid_dataTest.json',
                columns: [[
                    { field: 'FK_Sort', title: '类别名', width: 100 },
                     { field: 'Unit', title: '单位', width: 30 },
                     { field: 'NumOfTake', title: '领取数量', width: 100 }
                ]],
                width: 'auto',
                height: 'auto'
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <div id="pageloading">
    </div>--%>
    <div data-options="region:'center'" border="false" style="margin: 0; padding: 0;
        overflow: hidden;">
        <div id="tb" style="padding: 3px;">
            <div style="padding-left: 10px; padding-right: 20px; color: #444; margin-bottom: 2px;">
                查询条件：<asp:DropDownList runat="server" class="ddlNewsCatagory" ID="ddlNewsCatagory"
                    Width="100px">
                    <asp:ListItem>请选择</asp:ListItem>
                    <asp:ListItem>按类别</asp:ListItem>
                    <asp:ListItem>按部门</asp:ListItem>
                </asp:DropDownList>
                <%--  关键字：<input id="TB_KeyWords" type="text" style="width: 250px; border-style: solid;
                    border-color: #aaaaaa;" />--%>
                <%--  <a id="DoQueryByKey" href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'"
                    onclick="LoadGridData(1, 20)">查询</a>--%>
                <%--  <a id="ShowAll" href="#" class="easyui-linkbutton"
                       data-options="plain:true,iconCls:'icon-search'" onclick="LoadGridData(1, 20)">显示所有</a>--%>
                <%--  <a id="A1" href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-delete'"
                    onclick="LoadGridData(1, 20)">批量删除</a>--%>
            </div>
            <div style="clear: both;">
            </div>
        </div>
        <table id="newsGrid" fit="true" fitcolumns="true" toolbar="#tb" class="easyui-datagrid">
        </table>
    </div>
</asp:Content>
