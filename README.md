﻿
### CCFlow基本信息.###
1. **产品名称:**驰骋OA.  **简称** CCOA   **版本:** CCOA6.0  **英文名称:** ChiCheng OA  
2. **官方网站:** [http://ccport.org](http://ccport.org "官方网站")  **开发历史:** 2012-2016年.         **性质:** 100%开源,无任何功能限制.
5. **许可协议:** GPL  URL: http://www.gnu.org/licenses/gpl.html 
6. **运行环境:** 设计器BS结构, IIS6.0+, .net4.0.
7. **开发语言:** VS2010, .net4.0 c#.net. **客户端:** FireFox 3.0以上. IE6+,或者使用IE内核的浏览器.
8. **组成部分:** 流程设计器、表单设计器、流程解析执行器、表单解析执行器、流程服务。
9. **基本功能:** 图形化流程设计/智能表单web定义免程序开发/级联下拉框/流程轨迹/单据自定义打印/邮件短信工作到达通知/自动任务分配/支持sdk模式开发/简洁集成/消息侦听/丰富事件接口/报表定义/工作量分析/绩效考核/手机访问/支持sqlserve,oracle,mysql,informix数据库

CCFlow功能: 懂管理就会开发基本流程,会SQL就能达到中级开发水平解决复杂的逻辑问题与其它系统耦合,有程基础就可以使用sdk做高级的开发.

10. **支持数据库:** sqlserver系列版本， oracle 系列版本
10. **Git 地址:** svn://git.oschina.net/opencc/CCOA
11. **下载地址** 请参考: http://git.oschina.net/opencc/ccflow/wikis/pages
12. **两个版本:**  ccflow是.net版本开发的. jflow使用Java开发是ccflow的影子版本。两个版本,代码&数据库解构,设计思想,功能,操作手册,完全相同. 流程表单模版完全通用。
13. [JFlow的官方网站 ](http://jflow.cn "jflow官方网站."), git网站地址:[http://git.oschina.net/opencc/jflow/tree/develop](http://git.oschina.net/opencc/jflow/tree/develop "jflow在git上.")
14. ccflow & JFlow 集成多众多的开发人员、流程设计人员、管理人员众多的智慧，成长过程中得到众多企业的帮助。
15. ccflow帮助了成百上千家企业成功，众多的爱好者的贡献，成功客户案例，请打开官方网站查找. http://ccflow.org/Case.aspx 

### 为什么选择CCOA? ###
1. CCOA集成了强大的开源工作流程引擎ccflow.
2. CCOA集成了CCGPM的权限管理系统，可以方便定义权限与组织结构.
3. CCFlow集成方便, 概念、名词通俗易懂.

### 基础功能 ###

1011	我的计划	252	010101
1012	公告通知	2002	0102
1013	公告管理	1012	010201
1014	添加公告	1012	10202
1015	我的公告	1012	10203
1016	邮件管理	2002	0103
1017	写邮件	1016	010301
1018	收件箱	1016	10302
1019	发件箱	1016	10303
1020	新闻管理	2002	0104
1021	添加新闻	1020	010401
1022	新闻管理	1020	10402
1024	新闻栏目	1020	10404
1025	工作流程	2002	0105
1026	发起	1025	010501
1027	待办	1025	10502
1028	抄送	1025	10503
1029	挂起	1025	10504
1030	在途	1025	10505
1031	查询	1025	10506
1032	关键字查询	1025	10507
1033	取消审批	1025	10508
1036	设置	1025	10511
1037	我的日程	252	010102
1038	创建流程	1025	10512
1039	草稿箱	1016	10304
1040	垃圾箱	1016	10305
1041	我的新闻	1020	11916
106	文档中心	2002	0106
107	知识树	106	010601
145	车辆管理	2002	0107
146	驾驶员信息	145	010701
147	我的文档	106	10602
160	知识管理	106	10603
185	指标管理	145	01070101
186	最新文档	106	10604
188	我的工作台	187	010801
189	所有资讯	187	10802
190	收件箱(0/1)	187	10803
191	我的日程	187	10804
192	我的文档	187	10805
193	信息块维护	187	10809
194	安全退出	187	10807
195	参数设置	272	11203
196	固定资产	2002	0113
197	资产登记	196	011301
198	办公用品	2002	0114
199	库存台账	198	011401
200	购买台账	198	11402
2000	业务系统	1000	0101
2001	办公系统	1000	0102
2002	驰骋OA	2000	0103
2003	Tester	2001	0104
2004	工作流程	2000	0105
2005	即时通讯	2000	0106
2006	公共信息网	2000	0107
2007	权限管理	2000	0108
2008	单点登陆	2000	0109
201	人事档案	2002	0115
2010	发送短消息	252	10103
2011	公告类型	1012	10204
202	员工档案	201	011501
203	合同签订信息	201	11502
204	领取台账	198	1140201
214	合同解除信息	201	11504
217	公积金台账	201	11507
219	社保与福利台账	201	11509
224	调动记录	223	011601
228	调动记录	227	01170101
230	发起人员调动流程	227	1170102
242	新建节点242	213	1150301
243	所属大类	196	11302
244	所属小类	196	11303
246	资产领用	196	11304
248	资产报修	196	11305
249	资产报废	196	11306
250	发起投票	252	010901
251	我的投票	252	10902
252	我的工具	2002	0110
253	科学计算器	252	011001
254	万年历	252	11002
255	记事便签	252	11003
256	同事共享	106	10605
257	资产归还	196	11307
258	我的通讯录	252	11004
259	合同变更台帐	201	11510
261	车辆信息	145	10702
262	知识共享	106	10606
263	用品类别	198	1140202
264	知识库	187	10808
265	会议管理	2002	0111
267	会议类型	265	11102
268	会议室资源维护	265	11103
269	会议室列表	265	11104
270	会议室预订	265	11105
271	我的会议	265	11106
272	我的论坛	2002	0112
273	论坛板块	272	011201
274	论坛首页	272	11202
297	快捷入口	2002	0119
299	待办	297	11911
300	在途	297	11912
301	发起	297	11913
302	我的邮件	297	11914
303	我的公告	297	11915
304	我的新闻	297	11916
305	我的会议	297	11917
 

### ccoa 程序文件清单:  ###
1. D:\CCOA\Components   -- 组件目录.
2. D:\CCOA\Components\BP.En30   -- 底层基类.
3. D:\CCOA\Components\BP.Web.Controls30   --BS 控件层.
4. D:\CCOA\Components\BP.WF  --工作流程引擎层
5. D:\CCOA\RefDLL  -- 第三方组件中需要调用dll.
6. D:\CCOA\Documents -  文档
7. D:\CCOA\CCApp  --OA系统前台主目录.  
8. D:\CCOA\DemoAndTesting  - 单元测试&Demo


### ccoa前台目录结构.前台程序.(不建议用户修改，如果修改请提交给我们，否则您就没有办法升级.) ###
1. D:\CCOA\CCApp\WF\ --前台程序.
2. D:\CCOA\CCApp\WF\Comm  --通用功能层.
3. D:\CCOA\CCApp\WF\Data  -- 应用程序数据目录. 包含一些xml,等等。
4. D:\CCOA\CCApp\WF\Data\Install 与安装有关系的文件
5. D:\CCOA\CCApp\WF\Data\JSLib  系统javascript 函数库。
6. D:\CCOA\CCApp\WF\Data\Language 语言包(完善中)
7. D:\CCOA\CCApp\WF\Data\Node  cs流程设计器节点类型（cs流程设计器不在发展）
8. D:\CCOA\CCApp\WF\Data\XML  xml配置文件不仅仅ccflow使用,bp框架也使用它。
9. D:\CCOA\CCApp\WF\UC  --用户控件.
10. D:\CCOA\CCApp\WF\DocFlow -- 公文流程(目前还不是很完善)
11. D:\CCOA\CCApp\WF\Admin - 对ccflow的管理比如设计方向条件.报表定义...
12. D:\CCOA\CCApp\WF\Admin\XAP CCFlowDesigner.xap流程设计器，CCForm.xap表单设计器。   
13. D:\CCOA\CCApp\WF\MapDef - 表单定义.
14. D:\CCOA\CCApp\WF\SDKComponents  --流程组件目录.
15. D:\CCOA\CCApp\WF\WorkOpt -- 工作处理器的附件功能.
16. D:\CCOA\CCApp\WF\Admin\CCBPMDesigner -- H5的流程设计器.
17. D:\CCOA\CCApp\WF\Admin\CCFormDesigner -- H5的表单设计器.
18. D:\CCOA\CCApp\WF\WebOffice -- 公文流程处理程序.
 

### 1.2 前台的用户数据文件，用户可以更改. ###
1. D:\CCOA\CCApp\DataUser --用户文件.
2. D:\CCOA\CCApp\DataUser\Seal -- 电子盖章图片.
3. D:\CCOA\CCApp\DataUser\UploadFile - 上传附件
4. D:\CCOA\CCApp\DataUser\Style -- 个性化风格文件.
5. D:\CCOA\CCApp\DataUser\CyclostyleFile -- 单据模版文件.
6. D:\CCOA\CCApp\DataUser\EmailTemplete -邮件模版文件.
7. D:\CCOA\CCApp\DataUser\ICON --ICON
8. D:\CCOA\CCApp\DataUser\TaoHong --公文套红.
9. D:\CCOA\CCApp\DataUser\Bill  单据打印生成数据.
10. D:\CCOA\CCApp\DataUser\CyclostyleFile 单据模板数据
11. D:\CCOA\CCApp\DataUser\DtlTemplete  导入明细表模板文件.
12. D:\CCOA\CCApp\DataUser\EmailTemplete  自定义邮件发送格式文件.
13. D:\CCOA\CCApp\DataUser\JSLib 用户自定义函数库
14. D:\CCOA\CCApp\DataUser\JSLibData 用户自定义函数生成文件。
15. D:\CCOA\CCApp\DataUser\Log 系统日志文件
16. D:\CCOA\CCApp\DataUser\ReturnLog 退回日志文件.
17. D:\CCOA\CCApp\DataUser\Siganture 签名文件.
18. D:\CCOA\CCApp\DataUser\Style 用户自定义风格文件。
19. D:\CCOA\CCApp\DataUser\UploadFile 表单附件上传文件，单附件，与多附件。
20. D:\CCOA\CCApp\DataUser\XML 用户系统配置文件。
21.  D:\CCOA\CCApp\GPM\**  -- 权限控制系统，如果不启用权限系统就可以省略。
 
###  如何学习好ccoa? ###
1.  多看视频与文档.
   1. ccflow提供的视频是4.0的视频，有一些功能对应不上您可以看文档，文档是最新的。
   2. ccflow提供了两个重要的文档<<驰骋工作流引擎-流程设计器操作手册-CCFlow6.doc>> <<驰骋工作流引擎-表单设计器操作手册-CCFlow6.doc>> 这是您掌握ccflow的基础.   
2. 加入群里与ccflow爱好者交流.请打开http://ccflow.org 网站,找到qq群加入里面, 把不明白的问题，提交到群论坛里，会有人回答你的问题.
3. 有问题反馈到bbs,  http://bbs.ccflow.org/  注意不要省掉注册邀请码.    
4. 通过OSC发布bug，反馈问题，协助我们开发。
5. 如果您们项目工期紧张并且有充足的费用，建议成为ccflow的vip用户，您会得到现场的技术支持与系统培训。

### 如何安装并设置ccflow开发环境?(请严格按如下步骤去安装,不要跳跃,否则会导致安装失败.) ###

1. 确认升级到IE7 以上，并且做如下设置.
   1. 菜单->工具->Internet 选项-> 隐私 -> 把打开弹出窗口阻止程序 关闭上，否则一些功能就不能使用.
   2. 菜单->工具->Internet 选项-> 常规 -> 浏览历史记录设置-> 选中 () 每次访问此网页时. 单选按钮. 点确定，以避免缓存对程序有影响。
   3. 确认你是以administrator 超级用户进行安装的。

2. 使用 svn下载源程序,请查看如下连接.
 
   1. 如果你不会使用svn请看这里: http://hi.baidu.com/ccflow/blog/item/427ceff4ff96d03bbc3109b9.html
   2. 建议把ccflow放在 D:\下面,  ccflow的说明书都以此目录说明, 以方便您定位程序文件.
   3. 特别提示: 如果不放置在D:\CCOA 下，您可能会遇到dll 文件引用找不到路径的问题。

3. 打开解决方案文件并编译它. CCOA 解决方案的位置: D:\CCOA\CCOA.sln, 设置 Default.aspx为起始页.

   ETC: 如果不能正常打开请按如下步骤检查.
   1. 您的vs是否是2010版本？
   2. 该版本上是否安装上了   Silverlight4_Tools.exe  ccflow 群共享里可以下载.
   3. 安装:Silverlight_4_Toolkit_April_2010.msi

4. 创建空白数据库.
   1. 建立数据库(不管你用的什么数据库,oracle,informix,sqlserver系列),先创建一个空白的数据库名称为: CCFlow6
   2. 请看下面第5步骤, 为数据库配置链接.

5. 打开文件 D:\CCOA\CCApp\web.config   修改 Appsetting 节点中的数据库连接。

   如下：【如果使用的是MSSQL数据库，修改数据库的链接的用户名和密码即可】
  
    <appSettings>   
    <!-- 数据库连接url. -->
    <add key="AppCenterDSN" value="Password=123;Persist Security Info=True;User ID=sa;Initial Catalog=ccflow;Data Source=.;Timeout=2" />
    <!--ccflow 支持的数据库类型, MSSQL, Oracle, DB2, MySQL,InforMix 
     注意区分大小写. 还要检查此数据库用户是否具有足够的权限可以创建表、
     视图、存储过程、函数。 -->
    <add key="AppCenterDBType" value="MSSQL" />

6. 在vs设计器里面，直接ctrl + F5 运行系统。

   1, 系统直接运行到安装界面.
   2，如果不需要安装demo，就不选安装demo。
   3，点击接受协议并安装，稍等十几分钟就会安装好。

7. 建立网站应用:

   1. 开始->> 运行->> 输入 inetmgr 打开IIS管理器.
   2. 确认您的机器已经安装上了.net4.0, 并且，启用了它:
        
        For Win7用户: 把鼠标点在Internet信息服务(IIS)管理器的根节点，找到 IIS 分组中的 [IASPI 和CGI的限制] 图标，确认ASP.NET v4.0 相关的dll是否被允许.
        For Win2003用户: Internet 信息服务树上找到 Web服务扩展. 确认 ASP.NET v4.0.*** 是存在或者被允许? 如果没有找到 ASP.NET v4.0 则是您没有安装.net4.0 Framework, 请下载安装解决.

   3. 建立网站, 网站名称为 CCOA  主目录为: D:\CCOA\CCApp , 应用程序池为 ASP.NET v4.0 Classic. (建议单独建立一个应用程序池.)


8. 启动流程设计器 
  
   1. 流程设计器进入地址:` http://localhost:5853/WF/Admin/CCBPMDesigner/Login.aspx`
   2. 如果使用Silverlight 版本流程设计器路径, 您需要安装Silverlight .
   3. ccflow6已经支持了 html5版本的流程设计器，你可以根据自己的爱好选择一个流程设计器，我们建议使用sl版本的流程设计器.
   4. 为什么我们没有放弃silverlight流程设计器？ 请参考wiki 界面。
   
   **说明:**

   1. 第一次进入后，会提示您安装与初始化CCOA数据.
   2. 注意选择是否是要安装demo .
   3. 安装成功会ccoa 会自动转入登录界面.
   4. 如果中途安装失败，您需要删除数据库重新建立，重新安装.
   5. 开始的是有使用admin 密码123登录，在右上角就可以进入流程设计器，权限管理系统，其他用户则看不到这两个连接.



### 安装过程中经常遇到的问题: ###

1. 在internet 信息服务(iis) 中， web 服务扩展 asp.net v4.xxxxxx 的dll 没有被允许.

   1. 如果找不到，就是没有安装iis的程序扩展功能。
   2. 控制面板-> 程序和功能-> 在左边有打开或者关闭windows功能. 把 Internet information services. 与 Internet 信息服务 全部选择上.

2. 没有注册 asp.net 到iis 上. 需要执行如下命令.

         cd C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319   (32位)
         cd C:\WINDOWS\Microsoft.NET\Framework64\v4.0.30319 (64位)
         aspnet_regiis.exe -ua
         aspnet_regiis.exe -i


3. 没有配置mime 类型, 导致流程轨迹图,流程设计器不能被使用. 处理方法.

        windows XP 或 win2003 server 用户:
        打开IIS->站点属性->HTTP头->MIME类型->新建： 
        扩展名： .xap     MIME类型：  xapapplication/x-silverlight  
        扩展名： .xaml   MIME类型：  application/xaml+xml

  Win7 用户:

    1， 启用iis7.
        控制面板-> 程序和功能-> 在左边有打开或者关闭windows功能.
        把 Internet information services. 与 Internet 信息服务 全部选择上。

    2,  设置mime类型.
    
       开始->运行->输入inetmgr . 把鼠标放在根节点上, 选择iis 分组中的 MIME类型.

       扩展名： .xap     MIME类型：  xapapplication/x-silverlight  
       扩展名： .xaml    MIME类型：  application/xaml+xml


4. 出现用户名及密码错误，请在web.config 文件中.

    <identity impersonate="true" userName="administrator" password="jiaozi"/> 
    中的 impersonate="true" 修改成 impersonate="false" 或者填写正确的密码, 
    也可以把  impersonate="false" 

5. silverlight 版本不是 4.0以及4.0以上，请先卸了，重新安装。

6. 造成流程设计器不能正常打开的原因:有可能是c:\windows\temp 目录访问权限有限制, 修改一下该目录的访问权限，比如everyone都可以读写试一下(不建议你这样)。

7, 在按下安装一步时，遇到创建表或者数据库的错误。
   1. 指定连接里面数据库用户没有权限，导致错误。
   2. web.config 中的 数据库类型设置错误.

        <add key="AppCenterDBType" value="MSSQL" />

8, 用户权限问题.

     如果出现:C:\Windows\Microsoft.NET\Framework64\v4.0.30319\Temporary ASP.NET Files”
     的写访问权限。的错误提示. 解决办法:  把“IIS_IUSERS”和你自己的系统用户赋予它操作
     C：\WINDOWS\TEMP文件夹的权利，

  重新启动IIS，并尝试再次运行Web应用程序。

9. 如果以上方式都不奏效.

        cd C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319
        aspnet_regiis.exe -ua
        aspnet_regiis.exe -i

   重新安装，就有可能解决，此时可以寻求你的同事解决。

10. 如果您在使用silverlight版本的ccflow时，不能正常工作，进入流程设计器的登录界面，但是admin登录不进去，提示错误。

   **原因:**有可能是: *.asmx 文件解析的方式不对，不是 Framework 4.0.处理方法: 在IIS的“处理程序映射”，添加脚本映射 *.asmx    

     %windir%\Microsoft.NET\Framework\v4.0.30319\aspnet_isapi.dll


11. VS2010+IE8 调试提示 “找不到元素”

    1.  Open RegEdit //打开注册表编辑器
    2.  Browse to HKEY_LOCALMACHINE -> SOFTWARE -> Microsoft -> Internet Explorer -> Main
    3.  Set TabProcGrowth to 0 //设置 TabProcGrowth 的值为 0

12. 当前ccflow的工作模式为集成模式，您没有安装或者成功配置CCGPM,ccflow的BPM工作模式，必须依赖CCGPM才能运行。

   1. 如果需要集成模式，请先安装CCGPM，然后安装ccflow;
   2. 或者改为简单模式：请在web.config 文件中把OSModel 改为0.

14. 在 mysql, oracle 的安装的时候会越到大小写敏感问题,我们需要被安装的mysql数据库不要区分大小写.


**更多商业问题请参考:**   [http://ccport.org/About.aspx](http://ccport.org/About.aspx "商业问题") 