﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BP.Web;
using System.Data;
using BP.DA;
using BP.En;
using BP.Sys;
using BP.OA.Meeting;
using System.Web.SessionState;
using BP.Tools;

namespace CCOA.App.Meeting
{
    /// <summary>
    /// MeetingEUI 的摘要说明
    /// </summary>
    public class MeetingEUI : IHttpHandler, IRequiresSessionState
    {
        /// <summary>
        /// 封装有关个别 HTTP 请求的所有 HTTP 特定的信息
        /// </summary>
        HttpContext _Context = null;

        public string getUTF8ToString(string param)
        {
            return HttpUtility.UrlDecode(_Context.Request[param], System.Text.Encoding.UTF8);
        }

        public void ProcessRequest(HttpContext context)
        {
            _Context = context;

            //admin获取权限
            //if (BP.Web.WebUser.No == null)
            //    return;

            string method = string.Empty;
            //返回值
            string s_responsetext = string.Empty;
            if (!string.IsNullOrEmpty(_Context.Request["method"]))
                method = _Context.Request["method"].ToString();

            switch (method)
            {
                case "MyMeetingMet"://我的会议                    
                    s_responsetext = MyMeetingMet();
                    break;
                case "RoomListMet"://会议室列表                    
                    s_responsetext = RoomListMet();
                    break;
            }
            if (string.IsNullOrEmpty(s_responsetext))
                s_responsetext = "";
            //组装ajax字符串格式,返回调用客户端
            _Context.Response.Charset = "UTF-8";
            _Context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            _Context.Response.ContentType = "text/html";
            _Context.Response.Expires = 0;
            _Context.Response.Write(s_responsetext);
            _Context.Response.End();
        }

        public string MyMeetingMet()
        {
            string d_name = WebUser.FK_DeptName;
            string d_no = WebUser.FK_Dept;

            string ddSelvalue = getUTF8ToString("ddSelvalue");
            string ccSelvalue = getUTF8ToString("ccSelvalue");
            //当前页
            string pageNumber = getUTF8ToString("pageNumber");
            int iPageNumber = string.IsNullOrEmpty(pageNumber) ? 1 : Convert.ToInt32(pageNumber);
            //每页多少行
            string pageSize = getUTF8ToString("pageSize");
            int iPageSize = string.IsNullOrEmpty(pageSize) ? 9999 : Convert.ToInt32(pageSize);
            //获取删除oid
            string impOID = getUTF8ToString("impOID");
            string NotAgainSql = String.Format("SELECT * FROM oa_roomording where OID in ({0});", impOID);
            if (!string.IsNullOrEmpty(impOID))
            {
                if (DBAccess.RunSQLReturnTable(NotAgainSql).Rows.Count != 0)
                {
                    string delSql = string.Format("delete from oa_sponsormeeting where FK_SponsorOID in ({0});" +
                                            "delete from oa_roomording where OID in ({0});" +
                                            "delete from OA_MeetingSummary where FK_OID in ({0});", impOID);
                    DBAccess.RunSQL(delSql);
                }
            }

            //再次做判断
            string ztQuery = null;
            string dtNow = BP.OA.Main.CurDateTimeStr;
            string setState = null;
            switch (ccSelvalue)
            {
                case "预定中":
                    setState = "0";
                    break;
                case "待召开":
                    setState = "1";
                    ztQuery = "and DateFrom>'" + dtNow + "'";
                    break;
                case "正在进行":
                    setState = "1";
                    ztQuery = "and DateFrom<'" + dtNow + "' and DateTo>'" + dtNow + "'";
                    break;
                case "已结束":
                    setState = "3";
                    ztQuery = "and DateTo<'" + dtNow + "'";
                    break;
                case "已取消":
                    setState = "2";
                    break;
                case "请选择...":
                    setState = "";
                    break;
            }

            string part = "and b.ParticipantsEmpNo like('%," + WebUser.No + ",%')  or ParticipantsEmpNo ='' ";//我参与的会议。
            string ording = "and a.FK_Emp='" + WebUser.No + "'";//我预定的。
            string sponsor = "and  b.FK_Emp='" + WebUser.No + "'";//我发起的。


            string impsql = null;
            string d_sql = null;
            switch (ddSelvalue)
            {
                case "0"://我预定的
                    impsql = ording;
                    d_sql = null;
                    break;
                case "1"://我发起的
                    d_sql = sponsor;
                    break;
                case "2"://我参与的
                    d_sql = part;
                    break;
                case "3":
                    break;
            }
            string strSql = null;
          
            if (ccSelvalue == "请选择..." || ddSelvalue == "3")
            {
                if (ccSelvalue == "请选择..." && ddSelvalue != "3")//前五种状态
                {
                    strSql = string.Format("SELECT d.Title,b.CompereEmpNo,Dept,DateFrom,DateTo,Room,State,d.OID FROM( " +
                                           "SELECT '{0}' as Dept,DateFrom,DateTo,c.Name as Room,State,a.OID,Title FROM OA_RoomOrding a," +
                                           " OA_Room c WHERE  a.Fk_Room =c.No  {3}) d LEFT JOIN oa_sponsormeeting b ON " +
                                           "(d.OID=b.FK_SponsorOID and b.SponsorDeptNo='{2}') where 1=1 {4} ", d_name, WebUser.No, d_no, impsql, d_sql);


                }
                else if (ccSelvalue != "请选择..." && ddSelvalue == "3")//后三种为请选择...
                {
                    strSql = string.Format("SELECT d.Title,b.CompereEmpNo,Dept,DateFrom,DateTo,Room,State,d.OID FROM( " +
                                              "SELECT '{0}' as Dept,DateFrom,DateTo,c.Name as Room,State,a.OID,Title FROM OA_RoomOrding a," +
                                              " OA_Room c WHERE  a.Fk_Room =c.No and a.State='{4}') d LEFT JOIN oa_sponsormeeting b ON " +
                                              "(d.OID=b.FK_SponsorOID and b.SponsorDeptNo='{2}') where 1=1 {3}", d_name, WebUser.No, d_no, ztQuery, setState);
                  
                }
                else
                {
                    strSql = string.Format("SELECT d.Title,b.CompereEmpNo,Dept,DateFrom,DateTo,Room,State,d.OID FROM( " +
                                             "SELECT '{0}' as Dept,DateFrom,DateTo,c.Name as Room,State,a.OID,Title FROM OA_RoomOrding a," +
                                             " OA_Room c WHERE  a.Fk_Room =c.No  {3}) d LEFT JOIN oa_sponsormeeting b ON " +
                                             "(d.OID=b.FK_SponsorOID and b.SponsorDeptNo='{2}') where 1=2 {4} ", d_name, WebUser.No, d_no, impsql, d_sql);

                }
            }
            else
            {
                //strSql = string.Format("SELECT d.Title,b.CompereEmpNo,Dept,DateFrom,DateTo,Room,'" + ccSelvalue + "' as State,d.OID FROM( " +
                //                             "SELECT '{0}' as Dept,DateFrom,DateTo,c.Name as Room,State,a.OID,Title FROM OA_RoomOrding a," +
                //                             " OA_Room c WHERE  a.Fk_Room =c.No and a.State='{6}') d LEFT JOIN oa_sponsormeeting b ON " +
                //                             "(d.OID=b.FK_SponsorOID and b.SponsorDeptNo='{2}') where 1=1 {5}", d_name, WebUser.No, d_no, impsql, d_sql, ztQuery, setState);

                strSql = string.Format("SELECT d.Title,b.CompereEmpNo,Dept,DateFrom,DateTo,Room,State,d.OID FROM( " +
                                            "SELECT '{0}' as Dept,DateFrom,DateTo,c.Name as Room,State,a.OID,Title FROM OA_RoomOrding a," +
                                            " OA_Room c WHERE  a.Fk_Room =c.No and a.State='{6}') d LEFT JOIN oa_sponsormeeting b ON " +
                                            "(d.OID=b.FK_SponsorOID and b.SponsorDeptNo='{2}') where 1=1 {5}", d_name, WebUser.No, d_no, impsql, d_sql, ztQuery, setState);
            }

            int totalCount = BP.OA.Main.GetPagedRowsCount(strSql);
            DataTable OldTable = BP.OA.Main.GetPagedRows(strSql, 0, "Order by Room desc", iPageSize, iPageNumber);
           
            return DataTableConvertJson.DataTable2Json(OldTable, totalCount);
        }


        public string RoomListMet()
        {
            string keyWords = getUTF8ToString("keyWords");
            //获取oid
            string GetNo = getUTF8ToString("GetNo");

            string NotAgainSql = String.Format("SELECT * FROM OA_Room where No in ({0});", GetNo);

            //避免执行重复操作   删除
            if (!string.IsNullOrEmpty(GetNo))
            {
                if (DBAccess.RunSQLReturnTable(NotAgainSql).Rows.Count != 0)
                {
                    string DelSome = String.Format("Delete from OA_Room where No in ({0})", GetNo);

                    DBAccess.RunSQL(DelSome);
                }
            }

            string pageNumber = getUTF8ToString("pageNumber");
            int iPageNumber = string.IsNullOrEmpty(pageNumber) ? 1 : Convert.ToInt32(pageNumber);
            //每页多少行
            string pageSize = getUTF8ToString("pageSize");
            int iPageSize = string.IsNullOrEmpty(pageSize) ? 9999 : Convert.ToInt32(pageSize);



            BP.OA.Meeting.Rooms rooms = new BP.OA.Meeting.Rooms();
            QueryObject obj = new QueryObject(rooms);
            obj.AddWhere("1=1");
            if (!String.IsNullOrEmpty(keyWords))
            {
                obj.AddWhere(String.Format(" and Name LIKE '%{0}%'", keyWords));
            }

            int totalCount = obj.GetCount();
            obj.addOrderBy(RoomAttr.RoomSta, RoomAttr.No);
            obj.DoQuery(RoomAttr.No, iPageSize, iPageNumber);

            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(rooms, totalCount);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}