﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
namespace CCOA.App.Message
{
    public partial class InDetail1 : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        private int rID
        {
            get
            {
                return Convert.ToInt32(Request.QueryString["OID"]);
            }
        }
        
        private void ReadIt(int oid,string userNo)
        {
            string sql = String.Format("Update OA_MessageInBox set ReceiveTime=GetDate(),Received=1 where OID={0} and FK_ReceiveUserNo='{1}'", oid, userNo);
            if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            {
                sql = String.Format("Update OA_MessageInBox set ReceiveTime=GetDate(),Received=1 where OID={0} and FK_ReceiveUserNo='{1}'", oid, userNo);
            }
            BP.DA.DBAccess.RunSQL(sql);
            //清除即时消息
            BP.OA.ShortMsg.ReceivedInfo(oid.ToString(), "邮件");
        }
        private System.Data.DataTable GetInEmail(int oid)
        {
            string sSql = String.Format("select A.OID,Sender,A.AddTime,B.Title,B.Doc,B.AttachFile,B.SendToUsers,B.CopyToUsers from OA_MessageInBox A"
                                                   +" inner join OA_Message B on A.FK_MsgNo=B.OID"
                                                   +" where A.OID={0}", this.rID);
            return BP.DA.DBAccess.RunSQLReturnTable(sSql);
        }
        /// <summary>
        /// 根据存储路径获取老文件名，必须是存储路径中包含老文件名才可。
        /// </summary>
        /// <param name="savePath"></param>
        /// <returns></returns>
        private string GetAttachOldFileName(string savePath)
        {
            String attachFile = System.IO.Path.GetFileName(savePath);
            String ext = System.IO.Path.GetExtension(savePath);
            int index = attachFile.LastIndexOf('_');
            string oldName = attachFile.Substring(0, index);
            return String.Format("{0}{1}", oldName, ext);
        }
        public string GetUserNames(object userNos)
        {
            String users = String.Format("{0}", userNos);
            return BP.OA.GPM.GetUserNames(users);
        }
        public string GetAllAttachStr(string savePath, string splitter, string linkFormat)
        {
            if (String.IsNullOrEmpty(splitter)) splitter = "&nbsp;&nbsp;";
            if (String.IsNullOrEmpty(linkFormat)) linkFormat = "<a href='{1}'>{0}</a>";
            StringBuilder sb = new StringBuilder();
            String[] attachFiles = savePath.Split(new String[] { ";", ",", "|" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (String attachFile in attachFiles)
            {
                if (sb.Length > 0) sb.Append(splitter);
                sb.AppendFormat(linkFormat
                    , this.GetAttachOldFileName(attachFile)
                    , attachFile);
            }
            return sb.ToString();
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.ReadIt(this.rID, BP.Web.WebUser.No);
                this.LoadData();
            }
        }

        private void LoadData()
        {
            //System.Data.DataTable dt = this.GetInEmail(this.rID);
            BP.OA.Message.InBox inbox = new BP.OA.Message.InBox(this.rID);
            BP.OA.Message.Message message = new BP.OA.Message.Message(inbox.FK_MsgNo);
            if (inbox == null) return;
            this.liTitle.Text = message.Title;
            this.liDoc.Text = message.Doc;
            this.liUser.Text = inbox.Sender;
            this.liCopyUsers.Text = message.CopyToUsers;
            this.liRDT.Text = String.Format("{0:yyyy年MM月dd日 HH:mm}", inbox.AddTime);
            this.liReceivers.Text = message.SendToUsers;
            string attachPath = message.AttachFile;
            if (!String.IsNullOrEmpty(attachPath))
            {
                this.pnAttachFile.Visible = true;
                //this.hlFile.Text = this.GetAttachOldFileName(attachPath);
                //this.hlFile.NavigateUrl = attachPath;
                this.liAttachFiles.Text = this.GetAllAttachStr(attachPath, null, null);
            }
        }
        #endregion

        #region //3.页面事件(Page Event)

        #endregion
    }
}