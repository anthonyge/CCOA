using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.OA.HR
{
    /// <summary>
    /// 人员调动 流程事件实体
    /// </summary>
    public class TransferFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 人员调动 流程事件实体
        /// </summary>
        public TransferFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return "Transfer"; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            //把领用信息写入api.
            API.Transfer(
                this.GetValStr("Port_Emp"),
                this.GetValStr("DiaoDongRiQi"),
                this.GetValStr("YuanDanWei"),
                this.GetValStr("YuanZhiWu"),
                this.GetValStr("Port_Dept"),
                this.GetValStr("Port_Duty"),
                this.GetValStr("DiaoDongYuanYin"),
                this.GetValStr("BeiZhu"));
                
               return "写入成功....";
              
            
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
