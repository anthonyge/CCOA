﻿using BP.En;
using BP.OA.Desktop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.Main.desktop
{
    public partial class updateDeskTop : System.Web.UI.Page
    {
        /// <summary>
        /// 获取传入参数
        /// </summary>
        /// <param name="param">参数名</param>
        /// <returns></returns>
        public string getUTF8ToString(string param)
        {
            return HttpUtility.UrlDecode(Request[param], System.Text.Encoding.UTF8);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string method = string.Empty;
            //返回值
            string s_responsetext = string.Empty;
            if (!string.IsNullOrEmpty(Request["method"]))
                method = Request["method"].ToString();

            switch (method)
            {
                case "saveDesktopIdx"://获取知识树
                s_responsetext = saveDesktopIdx();
                break;
                case "updateDesktopIsView":
                s_responsetext = updateDesktopIsView();
                break;  
                case "updateDeskImgSizeMethod":
                updateDeskImgSizeMethod();
                break;
            }
            if (string.IsNullOrEmpty(s_responsetext))
                s_responsetext = "保存成功!";
            //组装ajax字符串格式,返回调用客户端
            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.ContentType = "textml";
            Response.Expires = 0;
            Response.Write(s_responsetext);
            Response.End();
        }

        /// <summary>
        /// 设置首页显示的模块
        /// </summary>
        /// <returns></returns>
        public String updateDesktopIsView() {
            String str = string.Empty;
            String layoutData = getUTF8ToString("NoStr");   //选中显示模块列表01,02,03
            String[] noStrArray = layoutData.Split(',');
            Dictionary<string, DesktopPanel> dic = getDeskTopListToMap();
            foreach (String noStr in noStrArray) 
            {
                try
                {
                    updateDeskIdxMethod(noStr, true);
                }
                catch {
                    str = "操作失败!";
                }
                dic.Remove(noStr);  //移除以选中的列表
            }
            try
            {
                updateDesktopIsView2(dic);
            }
            catch (Exception)
            {
                str = "操作失败!";
            }            
            return str;
        }

        /// <summary>
        /// 将不需要显示的模块IsEnabled设置为false
        /// </summary>
        /// <param name="dic"></param>
        public void updateDesktopIsView2(Dictionary<string, DesktopPanel> dict)
        {
            foreach (KeyValuePair<string, DesktopPanel> kvp in dict)
            {
                try
                {
                    updateDeskIdxMethod(kvp.Key, false);
                }
                catch (Exception)
                {                 
                    
                }                
            }
        }

        /// <summary>
        /// 首页保存模块位置
        /// </summary>
        /// <returns></returns>
        public String saveDesktopIdx() {
            string layoutData = getUTF8ToString("baseConfig");
            String[] layoutArr = layoutData.Split('#');
            String resultStr = String.Empty;
            //03,05#02,01#05,06
            String[] l_Str = layoutArr[0].Split(',');
            String[] c_Str = layoutArr[1].Split(',');
            String[] r_Str = layoutArr[2].Split(',');
            //
            Dictionary<string, DesktopPanel> dic = getDeskTopListToMap();
            try
            {
                if (l_Str.Length > 0)
                {
                    for (int i = 0; i < l_Str.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(l_Str[i]))
                        {
                            updateDeskIdxMethod(l_Str[i], 1 + 3 * i);
                            dic.Remove(l_Str[i]);
                        }                        
                    }
                }

                if (c_Str.Length > 0)
                {
                    for (int i = 0; i < c_Str.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(c_Str[i]))
                        {
                            updateDeskIdxMethod(c_Str[i], 2 + 3 * i);
                            dic.Remove(c_Str[i]);
                        }
                    }
                }

                if (r_Str.Length > 0)
                {
                    for (int i = 0; i < r_Str.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(r_Str[i]))
                        {
                            updateDeskIdxMethod(r_Str[i], 3 + 3 * i);
                            dic.Remove(r_Str[i]);
                        }
                    }
                }
                updateDesktopIsView2(dic);
            }
            catch {
                resultStr = "操作失败!";
            }
            return null;
        }

        /// <summary>
        /// 修改排序字段
        /// </summary>
        /// <param name="key"></param>
        /// <param name="sort"></param>
        public void updateDeskIdxMethod(string key,int sort)         
        {
            BP.OA.Desktop.DesktopPanel model = new BP.OA.Desktop.DesktopPanel(key);
            model.Idx = sort;
            model.Update();
        }

        /// <summary>
        /// 修改显示字段
        /// </summary>
        /// <param name="key"></param>
        /// <param name="sort"></param>
        public void updateDeskIdxMethod(string key, bool IsEnabled)
        {
            BP.OA.Desktop.DesktopPanel model = new BP.OA.Desktop.DesktopPanel(key);
            model.IsEnabled = IsEnabled;
            model.Update();
        }

        /// <summary>
        /// 修改轮播图图片大小设置(outHref)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="sort"></param>
        public void updateDeskImgSizeMethod()
        {
            string imgSizeStr = getUTF8ToString("baseConfig");
            string[] imgSizeStrArr = imgSizeStr.Split('#');
            if (!String.IsNullOrEmpty(imgSizeStrArr[0]) && !String.IsNullOrEmpty(imgSizeStrArr[1]))
            {
                BP.OA.Desktop.DesktopPanel model = new BP.OA.Desktop.DesktopPanel(imgSizeStrArr[0]);
                if (model != null && model.PanelType == 0)
                {
                    model.OutHref = imgSizeStrArr[1];
                    model.Update();
                }                
            }            
        }

        /// <summary>
        /// 数据库所有模块map
        /// </summary>
        public Dictionary<string, DesktopPanel> getDeskTopListToMap() 
        {
            BP.OA.Desktop.DesktopPanels modelList = new BP.OA.Desktop.DesktopPanels();
            modelList.RetrieveAll();
            Dictionary<string, DesktopPanel> dict = new Dictionary<string, DesktopPanel>();
            foreach (BP.OA.Desktop.DesktopPanel model in modelList)
            {
                dict.Add(model.No,model);
            }
            return dict;
        }
    }
}