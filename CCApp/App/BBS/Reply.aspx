﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/BBS/BBSPage.Master" AutoEventWireup="true"
    CodeBehind="Reply.aspx.cs" ValidateRequest="false" ClientIDMode="Static" Inherits="CCOA.App.BBS.Reply" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function changeImg() {
            var img = document.getElementById("vcodeImg");
            img.src = img.src + '?';
        }
    </script>
    <link rel="stylesheet" href="/ctrl/kindeditor/themes/default/default.css" />
    <link rel="stylesheet" href="/ctrl/kindeditor/plugins/code/prettify.css" />
    <script type="text/javascript" charset="utf-8" src="/ctrl/kindeditor/kindeditor.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ctrl/kindeditor/lang/zh_CN.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ctrl/kindeditor/plugins/code/prettify.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="newpost clearfix">
        <h2>
            <span class="h2left"></span>回复</h2>
        <div class="newpost_cnt clearfix">
            <div class="preview_haozip" id="small_img" style="display: none">
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <th>
                            <span class="cred">*</span>标题：
                        </th>
                        <td>
                            <asp:TextBox ID="txt_title" CssClass="ipt_01" Width="625" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <span class="fred" id="fred"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span class="cred">*</span>内 容：
                        </th>
                        <td style="width: 650px">
                            <textarea id="editorContent" style="width: 650px; height: 300px; visibility: hidden;"
                                runat="server"></textarea>
                        </td>
                        <td class="fgray" id="conten_msg">
                           
                        </td>
                    </tr>
                    <%--<tr>
                        <th>
                            <span class="cred">*</span>验证码：
                        </th>
                        <td>
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <td style="width: 75px;">
                                            <asp:TextBox TextMode="SingleLine" Width="45" MaxLength="5" ID="txt_vali" runat="server"
                                                CssClass="ipt_02 ipt_03" />
                                        </td>
                                        <td style="width: 60px;">
                                            <img id="vcodeImg" src="/ctrl/validateCode_Green/GetCode.ashx?s='+Math.random()"
                                                alt="验证码" title="看不清楚?点击换一张" style="margin-left: 8px; cursor: pointer; width: 75px;
                                                height: 25px; border-width: 0px; border: solid 1px #999999; vertical-align: middle;"
                                                onclick="src='/ctrl/validateCode_Green/GetCode.ashx?s='+Math.random();" />
                                        </td>
                                        <td>
                                            <a href="javascript:changeImg();" class="decoration">换个验证码</a>
                                        </td>
                                        <td>
                                            <span id="c_msg" style="color: red">&nbsp;</span>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Button Text="提交" ID="btn_Save" CssClass=" btn_01" runat="server" OnClick="btn_Save_Click" />&nbsp;&nbsp;
                            <asp:Button Text="返回" ID="btn_Back" CssClass=" btn_02" runat="server" OnClick="btn_Back_Click" />
                            <span id="limit_hours"></span>
                        </td>
                        <td style="color: red">
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
            <script type="text/javascript">
                var editor1s = new Object();
                InitKind("#<%=editorContent.ClientID %>", "Upload/", editor1s);
        
            </script>
        </div>
    </div>
</asp:Content>
