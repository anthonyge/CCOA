﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyDoc.aspx.cs" Inherits="CCOA.App.KM.MyDoc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Js/Js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/tree.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/locale/easyui-lang-zh_CN.js" type="text/javascript" charset="UTF-8"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="js/AppData.js" type="text/javascript"></script>
    <script src="js/MyDoc.js" type="text/javascript"></script>
</head>
<body class="easyui-layout">
    <div data-options="region:'center'" style="overflow: hidden;">
        <div class="panel-header" style="height: 20px;">
            <div style="float: left; vertical-align: middle; margin-top: 6px;">
                <img alt="" align="middle" height="16" width="16" src="../../Images/Home.gif" />当前路径：
                <span id="folderPath"><a href='#' onclick="FolderPathClick('128','0')">我的文档</a></span>
            </div>
            <div style="float: right; vertical-align: middle;">
                <input type="text" id="TB_Search" />&nbsp;&nbsp;<a href="#" class="easyui-linkbutton"
                    data-options="iconCls:'icon-search'" onclick="MyDocSearch()">搜索</a></div>
        </div>
        <div style="overflow: auto; width:100%; height: 90%;">
            <table id="docGrid" class="easyui-datagrid">
            </table>
        </div>
    </div>
    <div id="newDocDialog">
        <table>
            <tr>
                <td>文件名：</td>
                <td><input type="text" id="TB_FileName" style="width: 500px;" /></td>
            </tr>
            <tr>
                <td>关键词：</td>
                <td><input type="text" id="TB_KeyWord" style="width: 500px;" /></td>
            </tr>
            <tr>
                <td>文件大小：</td>
                <td><input type="text" id="TB_FileSize" style="width: 500px;" /></td>
            </tr>
            <tr>
                <td>简介：</td>
                <td><textarea cols="100" rows="10" id="TB_Doc" style="width: 500px"></textarea></td>
            </tr>
        </table>
    </div>
    <div id="treeSortDialog" data-options="iconCls:'icon-save'">
        <div style="height: 30px; overflow: auto; margin: 10px;">
            目录名称:<input type="text" name="sortName" id="sortName" />
        </div>
    </div>
    <div id="shareDialog">
        <table style="width:460px; margin-top:10px;">
            <tr>
                <td style="width:80px; text-align:right;">是否共享：</td>
                <td><input type="checkbox" id="CB_IsShare" />共享</td>
            </tr>
            <tr>
                <td style="width:80px; text-align:right;">权限范围：</td>
                <td><input type="checkbox" id="CB_AllEmps" checked="checked" />全体人员</td>
            </tr>
            <tr>
                <td style="width:80px; text-align:right; vertical-align:top;">指定人员：</td>
                <td><textarea id="allowPersons" rows="10" cols="20" style="width:280px; display:inline;"></textarea>
                <input type="button" id="selectedPersons" value="选择人员" onclick="zDialog_open2('../../ctrl/SelectUsers/SelectUser_Jq.aspx', '选择人员', 650, 430,'#allowPersonVals','allowPersonVals','allowPersons');" style="display:inline;" />
                <input type="hidden" id="allowPersonVals" />
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
