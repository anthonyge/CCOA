using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.FixAss
{
    public class API
    {
        #region 固定资产领取 api.
        /// <summary>
        /// 领用申请流程是否启用?
        /// </summary>
        public static bool FixMan_GetUse_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("FixMan_GetUse_FlowIsEnable", false);
            }
        }
        //FixMan_GetUse
        //FixManGetUseFEE.cs
        /// <summary>
        /// 领用的流程标记
        /// </summary>
        public static string FixMan_GetUse_FlowMark
        {
            get
            {
                return "FixManGetUse";
            }
        }
        public static string FixManGetUse_OneInsert(long WorkID, string fk_fixMan,
            string WhoGetUse, string GetUseDept, string GetUseDate, string GetUseZT)
        {
            GetUse gu = new GetUse();
            gu.WorkID = WorkID.ToString();
            gu.FK_FixMan = fk_fixMan;
            gu.WhoGetUse = WhoGetUse;
            gu.GetUseDept = GetUseDept;
            gu.GetUseDate = GetUseDate;
            gu.GetUseZT = GetUseZT;
            gu.Insert();
            return "调用api成功...";
        }
        public static string FixManGetUse_TwoUpdate(int oid, string GetUseZT)
        {
            GetUse gu = new GetUse(oid);
            gu.GetUseZT = GetUseZT;
            gu.Update();
            return "调用api成功...";
        }
        /// <summary>
        /// 固定资产领取
        /// </summary>
        //public static string FixMan_GetUse(string fk_fixMan, string fk_fixAssBigCatagory, string fk_fixAssBigCatSon, string GuiGe, string ShenQingRen, string LiShuBuMen, string RiQi, string GetUseZT)
        //{
        //    GetUse ds = new GetUse();
        //    ds.FK_FixMan = fk_fixMan;
        //    ds.FK_FixAssBigCatagory = fk_fixAssBigCatagory;
        //    ds.FK_FixAssBigCatSon = fk_fixAssBigCatSon;
        //    ds.ZipSpec = GuiGe;
        //    ds.WhoGetUse = ShenQingRen;
        //    ds.GetUseDept = LiShuBuMen;
        //    ds.GetUseDate = RiQi;
        //    ds.GetUseZT = GetUseZT;
        //    ds.Insert();
        //    return "调用api成功...";
        //}
        public static string FixMan_GetUse(string fk_fixMan, string WhoUse, string WhereIt, string ZCZT)
        {
            FixMan fm = new FixMan(fk_fixMan);
            fm.No = fk_fixMan;
            fm.WhoUse = WhoUse;
            fm.WhereIt = WhereIt;
            fm.ZCZT = ZCZT;
            fm.Update();
            return "调用api成功...";
        }
        #endregion 固定资产领取 api.


        #region 固定资产报废 api.
        /// <summary>
        /// 报废申请流程是否启用?
        /// </summary>
        public static bool FixMan_GetBad_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("FixMan_GetBad_FlowIsEnable", false);
            }
        }
        //FixMan_GetUse
        //FixManGetUseFEE.cs
        /// <summary>
        /// 报废的流程标记
        /// </summary>
        public static string FixMan_GetBad_FlowMark
        {
            get
            {
                return "FixManGetBad";
            }
        }
        public static string FixManGetBad_OneInsert(long WorkID, string fk_fixMan,
          string WhoFixManGetBad, string FixManGetBadDept, string FixManGetBadDate,
            string WhyGetBad, string GetBadZT)
        {
            GetBad gb= new GetBad();
            gb.WorkID = WorkID.ToString();
            gb.FK_FixMan = fk_fixMan;
            gb.WhoFixManGetBad = WhoFixManGetBad;
            gb.FixManGetBadDept = FixManGetBadDept;
            gb.FixManGetBadDate = FixManGetBadDate;
            gb.WhyGetBad = WhyGetBad;
            gb.GetBadZT = GetBadZT;
            gb.Insert();
            return "调用api成功...";
        }
        public static string FixManGetBad_TwoUpdate(int oid, string GetBadZT)
        {
            GetBad gb = new GetBad(oid);
            gb.GetBadZT = GetBadZT;
            gb.Update();
            return "调用api成功...";
        }

        public static string FixMan_GetBad(string fk_fixMan, string WhoUse, string WhereIt, string ZCZT)
        {
            FixMan fm = new FixMan(fk_fixMan);
            fm.No = fk_fixMan;
            fm.WhoUse = WhoUse;
            fm.WhereIt = WhereIt;
            fm.ZCZT = ZCZT;
            fm.Update();
            return "调用api成功...";
        }
        /// <summary>
        /// 固定资产报废
        /// </summary>
        public static string FixMan_GetBad(string fk_fixMain, string fk_fixAssBigCatagory, string fk_fixAssBigCatSon, string GuiGe, string ShenQingRen, string LiShuBuMen, string RiQi, string WhyGetBad, string GetBadZT)
        {
            GetBad gb = new GetBad();
            gb.FK_FixMan = fk_fixMain;
            gb.FK_FixAssBigCatagory = fk_fixAssBigCatagory;
            gb.FK_FixAssBigCatSon = fk_fixAssBigCatSon;
            gb.ZipSpec = GuiGe;
            gb.WhoFixManGetBad = ShenQingRen;
            gb.FixManGetBadDept = LiShuBuMen;
            gb.FixManGetBadDate = RiQi;
            gb.WhyGetBad = WhyGetBad;
            gb.GetBadZT = GetBadZT;
            gb.Insert();
            return "调用api成功...";
        }
        #endregion 固定资产报废 api.


        #region 固定资产维修 api.
        /// <summary>
        /// 报废申请流程是否启用?
        /// </summary>
        public static bool FixMan_Repair_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("FixMan_Repair_FlowIsEnable", false);
            }
        }
        //FixMan_GetUse
        //FixManGetUseFEE.cs
        /// <summary>
        /// 报废的流程标记
        /// </summary>
        public static string FixMan_Repair_FlowMark
        {
            get
            {
                return "FixManRepair";
            }
        }

        public static string FixManRepair_OneInsert(long WorkID, string fk_fixMan,
          string WhoWantRep,string RepMney, string FixManRepairDept,
            string FixManRepairDate, string RepairZT, string WhyRepair)
        {
            Repair rp = new Repair();
            rp.WorkID = WorkID.ToString();
            rp.FK_FixMan = fk_fixMan;
            rp.WhoWantRep = WhoWantRep;
            rp.RepMney = RepMney;
            rp.FixManRepairDept = FixManRepairDept;
            rp.FixManRepairDate = FixManRepairDate;
            rp.RepairZT = RepairZT;
            rp.WhyRepair = WhyRepair;
            rp.Insert();
            return "调用api成功...";
        }
        public static string FixManRepair_TwoUpdate(int oid,string RepairZT)
        {
            Repair rp = new Repair(oid);
            rp.RepairZT = RepairZT;
            rp.Update();
            return "调用api成功...";
        }
        public static string FixManRepair_ThreeUpdate(int oid, string WhoRes)
        {
            Repair rp = new Repair(oid);
            rp.WhoRes = WhoRes;
            rp.Update();
            return "调用api成功...";
        }
        /// <summary>
        /// 固定资产维修
        /// </summary>
        public static string FixMan_Repair(string fk_fixMan,  string ZCZT)
        {
            FixMan fm = new FixMan(fk_fixMan);
            fm.No = fk_fixMan;
            fm.ZCZT = ZCZT;
            fm.Update();
            return "调用api成功...";
        }

        //public static string FixMan_Repair(string fk_fixMain, string fk_fixAssBigCatagory, string fk_fixAssBigCatSon, string GuiGe, string WhoRes, decimal RepMney, string WhoWantRep, string LiShuBuMen, string RiQi, string WhyRepair, string RepairZT)
        //{
        //    Repair rp = new Repair();
        //    rp.FK_FixMan = fk_fixMain;
        //    rp.FK_FixAssBigCatagory = fk_fixAssBigCatagory;
        //    rp.FK_FixAssBigCatSon = fk_fixAssBigCatSon;
        //    rp.ZipSpec = GuiGe;
        //    rp.WhoRes = WhoRes;
        //    rp.RepMney = RepMney.ToString();
        //    rp.WhoWantRep = WhoWantRep;
        //    rp.FixManRepairDept = LiShuBuMen;
        //    rp.FixManRepairDate = RiQi;
        //    rp.WhyRepair = WhyRepair;
        //    rp.RepairZT = RepairZT;
        //    rp.Insert();
        //    return "调用api成功...";
        //}
        #endregion 固定资产维修 api.

        #region 固定资产归还 api.
        /// <summary>
        /// 归还申请流程是否启用?
        /// </summary>
        public static bool FixMan_GiveBack_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("FixMan_GiveBack_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 归还的流程标记
        /// </summary>
        public static string FixMan_GiveBack_FlowMark
        {
            get
            {
                return "FixManGiveBack";
            }
        }
        public static string FixManGiveBack_OneInsert(long WorkID, string fk_fixMan,
        string WhoGiveBack, string GiveBackDept, string GiveBackDate,
          string GiveBackNote, string GiveBackZT)
        {
            GiveBack gb = new GiveBack();
            gb.WorkID = WorkID.ToString();
            gb.FK_FixMan = fk_fixMan;
            gb.WhoGiveBack = WhoGiveBack;
            gb.GiveBackDept = GiveBackDept;
            gb.GiveBackDate = GiveBackDate;
            gb.GiveBackNote = GiveBackNote;
            gb.GiveBackZT = GiveBackZT;
            gb.Insert();
            return "调用api成功...";
        }

        public static string FixManGiveBack_TwoUpdate(int oid, string GiveBackZT)
        {
            GiveBack gu = new GiveBack(oid);
            gu.GiveBackZT = GiveBackZT;
            gu.Update();
            return "调用api成功...";
        }
        /// <summary>
        /// 固定资产归还
        /// </summary>
        //public static string FixMan_GiveBack(string fk_fixMain, string fk_fixAssBigCatagory, string fk_fixAssBigCatSon, string GuiGe, string WhoGiveBack, string GiveBackDept, string GiveBackDate, string GiveBackZT, string GiveBackNote)
        //{
        //    GiveBack rp = new GiveBack();
        //    rp.FK_FixMan = fk_fixMain;
        //    rp.FK_FixAssBigCatagory = fk_fixAssBigCatagory;
        //    rp.FK_FixAssBigCatSon = fk_fixAssBigCatSon;
        //    rp.ZipSpec = GuiGe;
        //    rp.WhoGiveBack = WhoGiveBack;
        //    rp.GiveBackDept = GiveBackDept;
        //    rp.GiveBackDate = GiveBackDate;
        //    rp.GiveBackZT = GiveBackZT;
        //    rp.GiveBackNote = GiveBackNote;
        //    rp.Insert();
        //    return "调用api成功...";
        //}
        public static string FixMan_GiveBack(string fk_fixMan, string WhoUse, string WhereIt, string ZCZT)
        {
            FixMan fm = new FixMan(fk_fixMan);
            fm.No = fk_fixMan;
            fm.WhoUse = WhoUse;
            fm.WhereIt = WhereIt;
            fm.ZCZT = ZCZT;
            fm.Update();
            return "调用api成功...";
        }
        #endregion 固定资产归还 api.

    }
}
