function b_RoundCurve(classname,b_c,bg_c,state,tagname,titlebg,titleimg){	
	var divs=getElementsByClassName(classname);
	for(var i=0;i<divs.length;i++){
		var obj=divs[i];
		var path=window.location.href;
		path=path.substring(0,path.lastIndexOf('/')+1);
		var comstyle="height:1px; font-size:1px;overflow:hidden; display:block;";

		var b1="margin:0 5px;";
		var b2="margin:0 3px;border-right:2px solid; border-left:2px solid;";
		var b3="margin:0 2px;border-right:1px solid; border-left:1px solid;";
		var b4="margin:0 1px;border-right:1px solid; border-left:1px solid;height:2px;";
		var content="border-right:1px solid;border-left:1px solid;overflow:hidden;position:relative;";
		var bgColor="background:"+bg_c+";";
		
		var imgPos2="background-position:-4px top;";
		var imgPos3="background-position:-2px -1px;";
		var imgPos4="background-position:-1px -2px;";
		var conPos="background-position:left -4px;";
		var imgPos5="background-position:-1px bottom;";
		var imgPos6="background-position:-2px bottom;";
		var imgPos7="background-position:-4px bottom;";
		
		
		var imgBgStr,imgPos3,imgPos4,conPos,imgPos5,imgPos6,imgPos7;
		var b_img2,b_img3,b_img4,b_img5,b_img6,b_img7,c_img,imgurl;
		
		
		if(state==1){
			b_img2=bgColor;
			b_img3=bgColor;
			b_img4=bgColor;
			c_img=bgColor;
			b_img5=bgColor;
			b_img6=bgColor;
			b_img7=bgColor;
	    }
		if(state==2){
			var imgObj=divs[i].getElementsByTagName('img')[0];
			var imgheight=imgObj.height;
			var contentheight=imgheight-10;
			var imgweight=imgObj.width;
			obj.style.width=(imgweight+2)+"px";
			var imgsrc=imgObj.src.replace(path,'');
			var imgBgStr="background:url("+imgsrc+") no-repeat;";

			conPos="height:"+contentheight+"px;width:"+imgweight+"px;overflow:hidden;";	
			
			b_img2=imgBgStr+imgPos2;
			b_img3=imgBgStr+imgPos3;
			b_img4=imgBgStr+imgPos4;
			c_img=conPos;
			b_img5=imgBgStr+"background-position:-1px -"+ (imgheight-4)+"px;";
			b_img6=imgBgStr+"background-position:-2px -"+ (imgheight-2)+"px;";
			b_img7=imgBgStr+"background-position:-4px -"+ (imgheight-1)+"px;";
			imgurl=imgsrc;
		}
		if(state==3){
		    var objh3=obj.getElementsByTagName(tagname)[0];
			if(titleimg!=null){
				var bgimg="background:url("+titleimg+") repeat-x;";
				b_img2=bgimg+imgPos2;
				b_img3=bgimg+imgPos3;
				b_img4=bgimg+imgPos4;

				objh3.style.background="url("+titleimg+") repeat-x left -4px";
				objh3.style.borderBottomColor=b_c;
			}
			else{
				var bg_c="background:"+titlebg+";";
				b_img2=bg_c;
				b_img3=bg_c;
				b_img4=bg_c;

				objh3.style.background=titlebg;
				objh3.style.borderBottomColor=b_c;
			}

			c_img=bgColor;
			b_img5=bgColor;
			b_img6=bgColor;
			b_img7=bgColor;

		}
		
		
		
		if(state==4){
			var bgimg=getStyle(obj,"backgroundImage");
			var bgWidth=getStyle(obj,"width");
			bgimg=bgimg.replace(path,"");
			bgimg=bgimg.substring(4,bgimg.length);
			bgimg=bgimg.substring(0,bgimg.length-1);
			var bgimgheight=getStyle(obj,"height");
			
			bgimgheight=bgimgheight.replace("px","");
			var contentheight=bgimgheight-10;
			bgWidth=bgWidth-2;
			
			bgimg=bgimg.replace("url(\"","");
			bgimg=bgimg.replace("\")","");
			path=path.substring(0,(path.lastIndexOf('/')+1));
			bgimg=bgimg.replace(path,"");
			imgBgStr="background:url("+bgimg+") no-repeat;";
			obj.style.background="none";

			b_img2=imgBgStr+imgPos2;
			b_img3=imgBgStr+imgPos3;
			b_img4=imgBgStr+imgPos4;
			c_img=imgBgStr+conPos+"height:"+contentheight+"px;width:"+bgWidth+"px;";
			
			b_img5=imgBgStr+"background-position:-1px -"+ (bgimgheight-4)+"px;";
			b_img6=imgBgStr+"background-position:-2px -"+ (bgimgheight-2)+"px;";
			b_img7=imgBgStr+"background-position:-4px -"+ (bgimgheight-1)+"px;";
		}
		if(state==2 || state==4){
			conDivHeight="";
	    }
		else{
			var H=getStyle(obj,"height");
			H=H.replace("px","");
			conDivHeight="height:"+(H-8)+"px";
		}
		
		
		var rDivStr="<b style='"+ comstyle+b1+"background:"+b_c+"'></b>";
		rDivStr+="<b style='"+ comstyle+b2+"border-color:"+b_c+";"+b_img2+"'></b>";
		rDivStr+="<b style='"+ comstyle+b3+"border-color:"+b_c+";"+b_img3+"'></b>";
		rDivStr+="<b style='"+ comstyle+b4+"border-color:"+b_c+";"+b_img4+"'></b>";
		rDivStr+="<div style='"+content+"border-color:"+b_c+";"+c_img+conDivHeight+"'>";
		rDivStr+="@d_P";
		rDivStr+="</div>";
		rDivStr+="<b style='"+ comstyle+b4+"border-color:"+b_c+";"+b_img5+"'></b>";
		rDivStr+="<b style='"+ comstyle+b3+"border-color:"+b_c+";"+b_img6+"'></b>";
		rDivStr+="<b style='"+ comstyle+b2+"border-color:"+b_c+";"+b_img7+"'></b>";
		rDivStr+="<b style='"+ comstyle+"margin:0 5px;background:"+b_c+"'></b>";
		
		var htmlText=divs[i].innerHTML;
		if(state==2){
			var str1=htmlText.replace("src=\"","src=\"*");
			var strsplit1=str1.split('*')[0];
			var strsplit2=str1.split('*')[1];
			var url=strsplit2.substring(0,strsplit2.indexOf('\"'));
			htmlText=strsplit1+imgurl+"\" style='border:0px;position:absolute;top:-4px;left:0px;'/>";
		}
		rDivStr=rDivStr.replace('@d_P',htmlText);
		divs[i].innerHTML=rDivStr;
	}
}

function getStyle( elem, name )
{
	if (elem.style[name])
	{
		return elem.style[name];
	}

	else if (elem.currentStyle)
	{
		return elem.currentStyle[name];
	}
	else if (document.defaultView && document.defaultView.getComputedStyle)
	{
		name = name.replace(/([A-Z])/g,"-$1");
		name = name.toLowerCase();
		var s = document.defaultView.getComputedStyle(elem,"");
		return s && s.getPropertyValue(name);
	}
	else
	{
		return null;
	}
}


function getElementsByClassName(searchClass, node,tag){  
	if(document.getElementsByClassName){return  document.getElementsByClassName(searchClass)}
	else{        
		node = node || document;        
		tag = tag || "*";        
		var classes = searchClass.split(" "),        
		elements = (tag === "*" && node.all)? node.all : node.getElementsByTagName(tag),        
		patterns = [],         
		returnElements = [],        
		current,         
		match;        
		var i = classes.length;       
		while(--i >= 0){patterns.push(new RegExp("(^|\\s)" + classes[i] + "(\\s|$)"));}        
		var j = elements.length;       
		while(--j >= 0){            
			current = elements[j];           
			match = false;            
			for(var k=0, kl=patterns.length; k<kl; k++){                
				match = patterns[k].test(current.className);                
				if (!match)  break;           
			} 
			if (match)  returnElements.push(current);        
		}        
		return returnElements;   
	} 
}