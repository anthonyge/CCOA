﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.OA;

namespace BP.OA.RCSWGL
{
    /// <summary>
    /// 通讯公司
    /// </summary>
    public enum TXGS { 
        /// <summary>
        /// 移动公司
        /// </summary>
        YDGS,
        /// <summary>
        /// 联通公司
        /// </summary>
        LTGS,
        /// <summary>
        /// 电信公司
        /// </summary>
        DXGS
    }
    /// <summary>
    /// 电话管理属性
    /// </summary>
    public class DHGLAttr : EntityNoNameAttr {
        /// <summary>
        /// 部门
        /// </summary>
        //public const string FK_Dept = "FK_Dept";

        public const string Dept = "Dept";
        /// <summary>
        /// 职位
        /// </summary>
        //public const string FK_Station = "FK_Station";

        public const string Station = "Station";
        /// <summary>
        ///姓名
        /// </summary>
       // public const string FK_Emp = "FK_Emp";

        public const string Emp = "Emp";
        /// <summary>
        /// 手机号
        /// </summary>
        public const string Tel = "Tel";
        /// <summary>
        /// 合同号
        /// </summary>
        public const string HTHao = "HTHao";
        /// <summary>
        /// 通讯公司
        /// </summary>
        public const string TXGS = "TXGS";
        /// <summary>
        /// 托收单位
        /// </summary>
        public const string TSDV = "TSDV";
        /// <summary>
        /// 所属单位
        /// </summary>
        public const string SDV = "SDV";
        /// <summary>
        /// 经办人
        /// </summary>
        public const string JBR = "JBR";
        /// <summary>
        /// 补贴标准
        /// </summary>
        public const string BTBZ = "BTBZ";
        /// <summary>
        /// 花费
        /// </summary>
        public const string HF = "HF";
        /// <summary>
        /// 差额
        /// </summary>
        public const string CE = "CE";
        /// <summary>
        /// 电话使用状态
        /// </summary>
        public const string DHSYZT = "DHSYZT";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Note = "Note";

        public const string WorkId = "WorkId";
    }
    /// <summary>
    /// 电话管理
    /// </summary>
    public class DHGL : EntityNoName
    {
        //#region 外键属性
        ///// <summary>
        ///// 部门
        ///// </summary>
        //public string FK_Dept
        //{
        //    get
        //    {
        //        return this.GetValStrByKey(DHGLAttr.FK_Dept);
        //    }
        //    set
        //    {
        //        this.SetValByKey(DHGLAttr.FK_Dept, value);
        //    }
        //}
        ///// <summary>
        ///// 岗位
        ///// </summary>
        //public string FK_Station
        //{
        //    get
        //    {
        //        return this.GetValStrByKey(DHGLAttr.FK_Station);
        //    }
        //    set
        //    {
        //        this.SetValByKey(DHGLAttr.FK_Station, value);
        //    }
        //}
        ///// <summary>
        ///// 人员
        ///// </summary>
        //public string FK_Emp
        //{
        //    get
        //    {
        //        return this.GetValStrByKey(DHGLAttr.FK_Emp);
        //    }
        //    set
        //    {
        //        this.SetValByKey(DHGLAttr.FK_Emp, value);
        //    }
        //}
        //#endregion
        #region 属性
        /// <summary>
        /// 通讯公司
        /// </summary>
        public int TXGS {
            get {
                return this.GetValIntByKey(DHGLAttr.TXGS);
            }
            set {
                this.SetValByKey(DHGLAttr.TXGS,value);
            }
        }
        /// <summary>
        /// 通讯公司名称
        /// </summary>
        public string TXGSText {
            get {
                return this.GetValRefTextByKey(DHGLAttr.TXGS);
            }
        }
     
        /// <summary>
        /// 手机号
        /// </summary>
        public string Tel
        {
            get
            {
                return this.GetValStrByKey(DHGLAttr.Tel);
            }
            set { this.SetValByKey(DHGLAttr.Tel, value); }
        }
        /// <summary>
        /// 合同号
        /// </summary>
        public string HTHao
        {
            get
            {
                return this.GetValStrByKey(DHGLAttr.HTHao);
            }
            set { this.SetValByKey(DHGLAttr.HTHao, value); }
        }
        /// <summary>
        /// 托收单位
        /// </summary>
        public string TSDV
        {
            get
            {
                return this.GetValStrByKey(DHGLAttr.TSDV);
            }
            set { this.SetValByKey(DHGLAttr.TSDV, value); }
        }
        /// <summary>
        /// 所属单位
        /// </summary>
        public string SDV
        {
            get
            {
                return this.GetValStrByKey(DHGLAttr.SDV);
            }
            set { this.SetValByKey(DHGLAttr.SDV, value); }
        }
        /// <summary>
        /// 经办人
        /// </summary>
        public string JBR
        {
            get
            {
                return this.GetValStrByKey(DHGLAttr.JBR);
            }
            set { this.SetValByKey(DHGLAttr.JBR, value); }
        }
        /// <summary>
        /// 补贴标准
        /// </summary>
        public string BTBZ
        {
            get
            {
                return this.GetValStrByKey(DHGLAttr.BTBZ);
            }
            set { this.SetValByKey(DHGLAttr.BTBZ, value); }
        }
        /// <summary>
        /// 部门
        /// </summary>
        public string Dept
        {
            get
            {
                return this.GetValStrByKey(DHGLAttr.Dept);
            }
            set { this.SetValByKey(DHGLAttr.Dept, value); }
        }
        /// <summary>
        /// 岗位
        /// </summary>
        public string Station
        {
            get
            {
                return this.GetValStrByKey(DHGLAttr.Station);
            }
            set { this.SetValByKey(DHGLAttr.Station, value); }
        }
        /// <summary>
        /// 人员
        /// </summary>
        public string Emp
        {
            get
            {
                return this.GetValStrByKey(DHGLAttr.Emp);
            }
            set { this.SetValByKey(DHGLAttr.Emp, value); }
        
        }

        /// <summary>
        /// 话费
        /// </summary>
        public decimal HF
        {
            get
            {
                return this.GetValDecimalByKey(DHGLAttr.HF);
            }
            set { this.SetValByKey(DHGLAttr.HF, value); }
        }
        /// <summary>
        /// 差额
        /// </summary>
        public decimal CE
        {
            get
            {
                return this.GetValDecimalByKey(DHGLAttr.CE);
            }
            set { this.SetValByKey(DHGLAttr.CE, value); }
        }
        /// <summary>
        /// 电话使用状态
        /// </summary>
        public string DHSYZT
        {
            get
            {
                return this.GetValStrByKey(DHGLAttr.DHSYZT);
            }
            set { this.SetValByKey(DHGLAttr.DHSYZT, value); }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note
        {
            get
            {
                return this.GetValStrByKey(DHGLAttr.Note);
            }
            set { this.SetValByKey(DHGLAttr.Note, value); }
        }

        public Int64 WorkId {
            get { 
                return this.GetValInt64ByKey(DHGLAttr.WorkId); 
            }
            set { this.SetValByKey(DHGLAttr.WorkId,value); }
        }
        #endregion
        #region 构造方法
        /// <summary>
        /// 无参构造方法
        /// </summary>
        public DHGL(){ }
        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="_No"></param>
        public DHGL(string _No) : base(_No) { }
        #endregion

        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_DHGL");
                map.EnDesc = "电话管理";//描述
                map.CodeStruct = "3";
                //存储位置
                map.DepositaryOfEntity = BP.DA.Depositary.None;
                map.DepositaryOfMap = BP.DA.Depositary.None;

                map.AddTBStringPK(DHGLAttr.No, null, "编号", true, true, 0, 100, 20);

                //map.AddDDLEntities(DHGLAttr.FK_Dept, null, "部门", new BP_Port_DeptEmpStation(), true);
                //map.AddDDLEntities(DHGLAttr.FK_Station, null, "岗位", new BP_Port_DeptEmpStation(), true);
                //map.AddDDLEntities(DHGLAttr.FK_Emp, null, "人员", new BP_Port_DeptEmpStation(), true);

                map.AddTBString(DHGLAttr.Dept, null, "部门", true, false, 0, 100, 20);
                map.AddTBString(DHGLAttr.Station, null, "职位", true, false, 0, 100, 20);
                map.AddTBString(DHGLAttr.Name, null, "名字", true, false, 0, 100, 20);

                map.AddTBString(DHGLAttr.Tel, null, "手机号", true, false, 0, 100, 20);
                map.AddTBString(DHGLAttr.HTHao, null, "合同号", true, false, 0, 100, 20);

                map.AddDDLSysEnum(DHGLAttr.TXGS, 0, "通讯公司", true,true,DHGLAttr.TXGS,"@0=移动公司@1=联通公司@2=电信公司");

                map.AddTBString(DHGLAttr.TSDV, null, "托收单位", true, false, 0, 100, 20);
                map.AddTBString(DHGLAttr.SDV, null, "所属单位", true, false, 0, 100, 20);
                map.AddTBString(DHGLAttr.JBR, null, "经办人", true, false, 0, 100, 20);
                map.AddTBString(DHGLAttr.BTBZ, null, "补贴标准", true, false, 0, 100, 20);
                map.AddTBInt(DHGLAttr.HF, 0, "花费", true, false);
                map.AddTBInt(DHGLAttr.CE, 0, "差额", true, false);

                map.AddTBString(DHGLAttr.DHSYZT, null, "电话使用状态", true, false, 0, 100, 20);
                map.AddTBStringDoc(DHGLAttr.Note, null, "备注", true, false, true);

                this._enMap = map;
                return this._enMap;
            }
        }

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }
        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        #endregion


    }


    public class DHGLs : EntitiesNoName {
        /// <summary>
        /// 电话管理
        /// </summary>
        public DHGLs() { }
        /// <summary>
        /// 得到它的ENTITY
        /// </summary>
        public override Entity GetNewEntity
        {
            get {
                return new DHGL();
            }
        }
    }
}
