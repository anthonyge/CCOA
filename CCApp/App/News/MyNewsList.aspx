﻿<%@ Page Title="" Language="C#" MasterPageFile="../../Main/master/Site1.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="MyNewsList.aspx.cs" Inherits="CCOA.App.News.MyNewsList" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">    
    <link href="../../CSS/GridviewPager.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="padding-left: 10px; padding-right: 20px; color: #444; margin-bottom:2px;">
        <div style="text-align: left; float: left; width: 660px;">
           新闻目录：<asp:DropDownList runat="server" ID="ddlNewsCatagory" Width="100px"></asp:DropDownList>
            关键字：<asp:TextBox ID="tbKeyWords" runat="server" Width="250" BorderStyle="Solid" BorderColor="#aaaaaa"
                BorderWidth="1"></asp:TextBox>&nbsp;<asp:Button ID="LinkButton1" Font-Bold="true" ForeColor="#234323"
                    runat="server" OnClick="Query" Text="查询" />&nbsp;&nbsp;<asp:Button ID="LinkButton3" Font-Bold="true" ForeColor="#234323"
                    runat="server" OnClick="QueryAll" Text="显示所有" />
        </div>
        <div style="clear: both;">
        </div>
    </div>
    <asp:GridView ID="GridView1" DataKeyNames="id" CssClass="grid" runat="server" AutoGenerateColumns="False"
        OnDataBound="GridView1_DataBound">
        <HeaderStyle HorizontalAlign="Left" />
        <Columns>
            <asp:BoundField DataField="id" HeaderText="编号" ItemStyle-Width="40" ReadOnly="true">
                <ItemStyle Width="40px"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="ArticleCatagory" HeaderText="新闻目录" ItemStyle-Width="70px">
                <ItemStyle Width="70px"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderText="标题">
                <ItemTemplate>
                    <asp:HyperLink ID="Label1" runat="server" Font-Bold="false" Text='<%# Eval("Title") %>' NavigateUrl='<%# Eval("id","NewsArticle.aspx?page=mynl&ID={0}") %>'></asp:HyperLink>
                    <%# BP.OA.Main.GetTimeImg(Eval("AddTime"),2,"新邮件","img/news_New.gif",0,0)%>
                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("SetTop")))?"":"<img src='img/top.gif' alt='置顶'"%>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="关键字">
                <ItemTemplate>
                    <span title='<%#Eval("KeyWords") %>'>
                        <%# GetLimitedLength(Eval("KeyWords"))%></span>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
                <ItemStyle Width="80px"></ItemStyle>
            </asp:TemplateField>
            <asp:BoundField DataField="AddTime" HeaderText="发布时间" ItemStyle-Width="120px">
                <ItemStyle Width="120px"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="BrowseCount" HeaderText="浏览" ItemStyle-Width="40px" DataFormatString="{0} 次">            
            </asp:BoundField>            
        </Columns>
        <RowStyle HorizontalAlign="Center" />
        <HeaderStyle HorizontalAlign="Center" />
    </asp:GridView>
    <div style="text-align: center;">
    <table cellpadding="0" cellspacing="0" align="center" width="99%" class="border">
        <tr>
            <td align="left" colspan="2">
                <webdiyer:AspNetPager ID="AspNetPager1" CssClass="paginator" CurrentPageButtonClass="cpb"
                    runat="server" AlwaysShow="false" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                     PrevPageText="上一页" ShowCustomInfoSection="Left" ShowInputBox="Never"
                    OnPageChanged="AspNetPager1_PageChanged" CustomInfoTextAlign="Left" LayoutType="Table">
                </webdiyer:AspNetPager>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>
