﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/AddressList/Site.Master" AutoEventWireup="true" CodeBehind="PublicList.aspx.cs" Inherits="CCOA.App.AddressList.PublicList" %>
<%@ Register src="../Pub.ascx" tagname="Pub" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <link href="../../Comm/Style/Table0.css" rel="stylesheet" type="text/css" />
    <asp:Repeater ID="rep_List" runat="server">
    <HeaderTemplate>
    <table width="35%" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>部门</th>      
        <th>电话</th>
        <th>Email</th>
        <th>部门负责人</th>
        <th>备注</th>
    </tr>
    </HeaderTemplate>
    <ItemTemplate>
    <tr>
   <td><%#Eval("No")%></td>
   <td><%#Eval("Name")%></td>
   <td><%#GetDeptNames(Eval("FK_Dept"))%></td>
    <td><%#Eval("Tel")%></td>
     <td><%#Eval("Email")%></td>
   <td><%#Eval("Dept")%></td>
   <td><%#Eval("Legend")%></td>
  
   
    </tr>
    </ItemTemplate>  
    <FooterTemplate></table></FooterTemplate>
    </asp:Repeater>
    <uc1:Pub ID="Pub1" runat="server" />
</asp:Content>
