﻿using System;
using System.Collections.Generic;
using System.Text;

using BP.DA;
using BP.En;

namespace BP.OA.Desktop
{
    /// <summary>
    /// 桌面配置属性
    /// </summary>
    public class DesktopPersonAttr : EntityOIDAttr
    {
        /// <summary>
        /// 所属人员
        /// </summary>
        public const string FK_Emp = "FK_Emp";
        /// <summary>
        /// 模块编号
        /// </summary>
        public const string FK_PID = "FK_PID";
        /// <summary>
        /// 加载顺序
        /// </summary>
        public const string Idx = "Idx";
    }
    /// <summary>
    ///  桌面配置
    /// </summary>
    public class DesktopPerson : EntityOID
    {
        #region 属性
        /// <summary>
        /// 所属人员
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(DesktopPersonAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(DesktopPersonAttr.FK_Emp, value);
            }
        }
        /// <summary>
        /// 模块编号
        /// </summary>
        public string FK_PID
        {
            get
            {
                return this.GetValStrByKey(DesktopPersonAttr.FK_PID);
            }
            set
            {
                this.SetValByKey(DesktopPersonAttr.FK_PID, value);
            }
        }
        /// <summary>
        /// 加载顺序
        /// </summary>
        public int Idx
        {
            get
            {
                return this.GetValIntByKey(DesktopPersonAttr.Idx);
            }
            set
            {
                this.SetValByKey(DesktopPersonAttr.Idx, value);
            }
        }
        #endregion 属性

        #region 构造方法
        /// <summary>
        /// 桌面配置
        /// </summary>
        public DesktopPerson()
        {
        }

        #endregion

        /// <summary>
        /// 桌面配置Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map();
                map.PhysicsTable = "OA_DesktopPerson"; //物理表。
                map.EnDesc = "桌面配置";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.CodeStruct = "2";

                map.AddTBStringPK(DesktopPersonAttr.OID, null, "编号", true, true, 10, 10, 10);
                map.AddDDLEntities(DesktopPersonAttr.FK_Emp, null, "FK_Emp", new BP.Port.Emps(), true);
                map.AddTBString(DesktopPersonAttr.FK_PID, null, "模块编号", true, false, 0, 200, 300, false);
                map.AddTBInt(DesktopPersonAttr.Idx, 0, "加载顺序", true, false);
                this._enMap = map;
                return this._enMap;
            }
        }
    }
    /// <summary>
    /// 桌面配置
    /// </summary>
    public class DesktopPersons : EntitiesOID
    {
        /// <summary>
        /// 桌面配置s
        /// </summary>
        public DesktopPersons() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DesktopPerson();
            }
        }
    }
}
