﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA.Calendar
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class CalendarTaskAttr : EntityOIDAttr
    {
        public const String FK_UserNo = "FK_UserNo";
        public const String CalendarCatagory = "CalendarCatagory";
        public const String Title = "Title";
        public const String StartDate = "StartDate";
        public const String EndDate = "EndDate";
        public const String Location = "Location";
        public const String Notes = "Notes";
        public const String Url = "Url";
        public const String IsAllDay = "IsAllDay";
        public const String Reminder = "Reminder";
        public const String IsNew = "IsNew";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class CalendarTask : EntityOID
    {
        #region 属性
        public string FK_UserNo
        {
            get { return this.GetValStrByKey(CalendarTaskAttr.FK_UserNo); }
            set { this.SetValByKey(CalendarTaskAttr.FK_UserNo, value); }
        }
        public int CalendarCatagory
        {
            get { return this.GetValIntByKey(CalendarTaskAttr.CalendarCatagory); }
            set { this.SetValByKey(CalendarTaskAttr.CalendarCatagory, value); }
        }
        public String Title
        {
            get { return this.GetValStrByKey(CalendarTaskAttr.Title); }
            set { this.SetValByKey(CalendarTaskAttr.Title, value); }
        }
        public DateTime StartDate
        {
            get { return this.GetValDateTime(CalendarTaskAttr.StartDate); }
            set { this.SetValByKey(CalendarTaskAttr.StartDate, value); }
        }
        public DateTime EndDate
        {
            get { return this.GetValDateTime(CalendarTaskAttr.EndDate); }
            set { this.SetValByKey(CalendarTaskAttr.EndDate, value); }
        }
        public String Location
        {
            get { return this.GetValStrByKey(CalendarTaskAttr.Location); }
            set { this.SetValByKey(CalendarTaskAttr.Location, value); }
        }
        public String Notes
        {
            get { return this.GetValStrByKey(CalendarTaskAttr.Notes); }
            set { this.SetValByKey(CalendarTaskAttr.Notes, value); }
        }
        public string Url
        {
            get { return this.GetValStrByKey(CalendarTaskAttr.Url); }
            set { this.SetValByKey(CalendarTaskAttr.Url, value); }
        }
        public bool IsAllDay
        {
            get { return this.GetValBooleanByKey(CalendarTaskAttr.IsAllDay); }
            set { this.SetValByKey(CalendarTaskAttr.IsAllDay, value); }
        }
        public string Reminder
        {
            get { return this.GetValStrByKey(CalendarTaskAttr.Reminder); }
            set { this.SetValByKey(CalendarTaskAttr.Reminder, value); }
        }
        public bool IsNew
        {
            get { return this.GetValBooleanByKey(CalendarTaskAttr.IsNew); }
            set { this.SetValByKey(CalendarTaskAttr.IsNew, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public CalendarTask() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public CalendarTask(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_CalendarTask";
                map.EnDesc = "日程事件";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBString(CalendarTaskAttr.FK_UserNo, null, "所属用户", true, false, 0, 50, 20);
                map.AddTBInt(CalendarTaskAttr.CalendarCatagory, 0, "日历类型", true, false);
                map.AddTBString(CalendarTaskAttr.Title, null, "新闻标题", true, false, 0, 100, 50);
                map.AddTBDateTime(CalendarTaskAttr.StartDate, null, "开始日期", true, false);
                map.AddTBDateTime(CalendarTaskAttr.EndDate, null, "结束日期", true, false);
                map.AddTBString(CalendarTaskAttr.Location, null, "地点", true, false, 0, 10, 20);
                map.AddTBString(CalendarTaskAttr.Notes, null, "文本", true, false, 0, 100, 50);
                map.AddTBString(CalendarTaskAttr.Url, null, "网址", true, false, 0, 100, 50);
                map.AddBoolean(CalendarTaskAttr.IsAllDay, true, "全天行为", true, false);
                map.AddTBString(CalendarTaskAttr.Reminder, null, "新闻内容", true, false, 0, 100, 50);
                map.AddBoolean(CalendarTaskAttr.IsNew, true, "是否为新", true, false);
                //map.AddTBString(CalendarTaskAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(CalendarTaskAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        #region 自主开发
        public System.Data.DataTable GetNewsDetail(int id)
        {
            string sSql = String.Format("select Title,KeyWords,B.Name as CalendarTaskCatagory,AddTime,FK_UserNo,Doc,CalendarTaskSource from OA_CalendarTask A"
                             + " inner join OA_CalendarTaskCatagory B on A.FK_CalendarTaskCatagory=B.No Where A.OID={0}", this.OID);
            return BP.DA.DBAccess.RunSQLReturnTable(sSql);
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class CalendarTasks : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new CalendarTask();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public CalendarTasks()
        {
        }
    }
}
