using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.DS
{
    /// <summary>
    /// 类别 属性
    /// </summary>
    public class DSSortAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 是否可以下载
        /// </summary>
        public const string IsDownload = "IsDownload";
    }
    /// <summary>
    ///  类别
    /// </summary>
    public class DSSort : EntityNoName
    {
        #region 属性
        #endregion 属性

        #region 权限控制属性.
        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 类别
        /// </summary>
        public DSSort()
        {
        }
        /// <summary>
        /// 类别
        /// </summary>
        /// <param name="_No"></param>
        public DSSort(string _No) : base(_No) { }
        #endregion

        /// <summary>
        /// 类别Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_DSSort");
                map.EnDesc = "类别";
              

                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.CodeStruct = "3";
                map.IsAutoGenerNo = true;
                map.AddTBStringPK(DSSortAttr.No, null, "编号", true, true, 3, 3, 3);

                map.AddTBString(DSSortAttr.Name, null, "类别", true, false, 0, 100, 30, false);


                this._enMap = map;
                return this._enMap;
            }
        }

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }
        #endregion 重写方法
    }
    /// <summary>
    /// 类别
    /// </summary>
    public class DSSorts : SimpleNoNames
    {
        /// <summary>
        /// 类别s
        /// </summary>
        public DSSorts() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DSSort();
            }
        }
    }
}
