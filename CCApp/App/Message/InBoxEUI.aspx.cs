﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.Message
{
    public partial class InBoxEUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadMsg();

                //清除即时消息
                BP.OA.ShortMsg.ReceivedInfo(null, "邮件");
            }
        }
        private void LoadMsg()
        {
            string sSql = String.Format("SELECT count(*) from OA_MessageInBox where FK_ReceiveUserNo='{0}' and Received='0'", BP.Web.WebUser.No);
            object oR = BP.DA.DBAccess.RunSQLReturnVal(sSql);
            int iR = Convert.ToInt32(oR);
            if (iR == 0)
            {
                this.lblMsg.ForeColor = System.Drawing.Color.Red;
                this.lblMsg.Text = String.Format("你没有新邮件！", iR);
            }
            else
            {
                this.lblMsg.ForeColor = System.Drawing.Color.Red;
                this.lblMsg.Text = String.Format("你还有{0}封邮件未读！", iR);
            }
        }
    }
}