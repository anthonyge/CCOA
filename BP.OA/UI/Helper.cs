﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BP.Port;
using BP.OA.Meeting;

namespace CCOA
{
    /// <summary>
    /// 
    /// </summary>
    public class MyHelper
    {
        /// <summary>
        /// 60秒不用数据自动到期
        /// </summary>
        private static Emps Emps
        {
            get
            {
                if (HttpRuntime.Cache["emps"] == null)
                {
                    BP.Port.Emps emps = new BP.Port.Emps();
                    emps.RetrieveAll();
                    HttpRuntime.Cache.Insert("emps", emps, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(60));
                }
                return HttpRuntime.Cache["emps"] as Emps;
            }
        }
        /// <summary>
        /// 60秒不用数据自动到期
        /// </summary>
        private static RoomResources Resources
        {
            get
            {
                if (HttpRuntime.Cache["resources"] == null)
                {
                    RoomResources resources = new RoomResources();
                    resources.RetrieveAll();
                    HttpRuntime.Cache.Insert("resources", resources, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(60));
                }
                return HttpRuntime.Cache["resources"] as RoomResources; ;
            }
        }

        public static string GetEmpName(object empNo)
        {
            string result = string.Empty;
            if (empNo == null || empNo.ToString() == string.Empty)
            {
                return "全部";
            }
            string[] nos = empNo.ToString().Trim(',').Split(',');
            foreach (Emp emp in Emps)
            {
                if (nos.Contains(emp.No, new MyStringComparer()))
                {
                    result += emp.Name + ",";
                }
            }
            return result.Trim(',');
        }
        public static string GetResName(object resNo)
        {
            string result = string.Empty;
            if (resNo == null || resNo.ToString() == string.Empty)
            {
                return result;
            }
            string[] res = resNo.ToString().Trim(',').Split(',');
            foreach (RoomResource re in Resources)
            {
                if (res.Contains(re.No, new MyStringComparer()))
                {
                    result += re.Name + ",";
                }
            }
            return result.Trim(',');
        }
        public static string GetRoomSta(object sta)
        {
            string result = "正常";
            if (sta != null && sta.ToString() == "1")
            {
                result = "维护中";
            }
            return result;
        }

        #region 会议室状态
        public static string GetMeetingState(object dateFrom, object dateTo, object state)
        {
            if (Convert.ToString(state) == "0")
            {
                return "预定中";
            }
            else if (Convert.ToString(state) == "1")//已发起
            {
                return CheckMeetingState(dateFrom, dateTo);
            }
            else if (Convert.ToString(state) == "2")
            {
                return "已取消";
            }
            else if (Convert.ToString(state) == "3")
            {
                return "已结束";
            }
            else
            {
                return string.Empty;
            }
        }
        public static string CheckMeetingState(object dateFrom, object dateTo)
        {
            if (dateFrom != null && dateTo != null)
            {
                DateTime df = Convert.ToDateTime(dateFrom);
                DateTime dt = Convert.ToDateTime(dateTo);
                if (df > DateTime.Now)
                {
                    return "待召开";
                }
                else
                {
                    if (dt > DateTime.Now)
                    {
                        return "正在进行";
                    }
                    else
                    {
                        return "正在进行";// "已结束";
                    }
                }
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion
    }
}