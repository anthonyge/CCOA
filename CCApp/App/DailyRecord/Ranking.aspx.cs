﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA;
using BP.En;

namespace CCOA.App.DailyRecord
{
    public partial class Ranking : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {


                this.Rep_List0.DataSource = GetName();
                this.Rep_List0.DataBind();
                this.Rep_List1.DataSource = GetRanking();
                this.Rep_List1.DataBind();
              
            }

        }

        //日志总数排序
        public     DataTable   GetName()
        {
            string sSql = String.Format(" select Rec,  COUNT(*) total  from OA_DRSheet where  RDT>=DATEADD(wk,DATEDIFF(wk,0,getdate()),0)  group by Rec order by COUNT(*) desc  ");
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sSql);
            return dt;
        }
        //编号名字转换
        public string GetCHName( string id)
        {
            string sSql = String.Format(" select Name  from Port_Emp where No='{0}'",id);
            string name = BP.DA.DBAccess.RunSQLReturnString(sSql);
            return name;
 
        }
        //日志篇数排行
        public DataTable GetRanking()
        {
            string sSql = String.Format("select rec, COUNT(*) total from OA_DRSheet   group by Rec order by COUNT(*)  desc");

           
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sSql);
            return dt;
        }
        //私有日志
        public string GetPrivate(string  id)
        {
            string sSql = String.Format("select  COUNT(*)  from OA_DRSheet where DRSta=0 and Rec='{0}'",id);
            string count = BP.DA.DBAccess.RunSQLReturnString(sSql);
            return count;
        }
        //共享日志
        public string GetShare(string id)
        {
            string sSql = String.Format("select  COUNT(*)  from OA_DRSheet where DRSta=1 and Rec='{0}'", id);
            string count = BP.DA.DBAccess.RunSQLReturnString(sSql);
            return count;
        }
        //公开日志
        public string GetPublic(string id)
        {
            string sSql = String.Format("select  COUNT(*)  from OA_DRSheet where DRSta=2 and Rec='{0}'", id);
            string count = BP.DA.DBAccess.RunSQLReturnString(sSql);
            return count;
        }
        //周总结
        public string GetWeek(string id)
        {
            string sSql = String.Format("select  COUNT(*)  from OA_DRSheet where DRShe=3 and Rec='{0}'", id);
            string count = BP.DA.DBAccess.RunSQLReturnString(sSql);
            return count;
        }
        //月总结
        public string GetMonth(string id)
        {
            string sSql = String.Format("select  COUNT(*)  from OA_DRSheet where DRShe=4 and Rec='{0}'", id);
            string count = BP.DA.DBAccess.RunSQLReturnString(sSql);
            return count;
        }
    }
}