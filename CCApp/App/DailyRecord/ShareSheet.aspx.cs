﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA;
using BP.En;

namespace CCOA.App.DailyRecord
{
    public partial class SheareSheet : BP.Web.WebPage
    {
        #region 属性.
        public int PageIdx
        {
            get
            {
                try
                {
                    return int.Parse(this.Request.QueryString["PageIdx"]);
                }
                catch
                {
                    return 1;
                }
            }
        }
        #endregion 属性.
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //变量定义: 第几页.
                int currPageIdx = this.PageIdx;
                int pageSize = 12; //页面记录条数.

                //实体查询.
                BP.OA.DRSheets ens = new BP.OA.DRSheets();

                //创建查询对象
                BP.En.QueryObject qo = new QueryObject(ens);

                string  rec=Request.QueryString["Rec"];
                if (rec!=null)
                {
                    string sSql = String.Format(" select Doc from OA_DRSheet where DRSta=2  and Rec='{0}'", rec);
                    qo.AddWhereInSQL(DRSheetAttr.Doc, sSql);
                }
                else
                {
                    string newsql = string.Format("select  Doc from OA_DRSheet,OA_DRShare where DRSta=1  and  Rec=Sharer and ShareTo= '{0}' and  Rec!= '{1}' ", BP.Web.WebUser.No, BP.Web.WebUser.No);
                    qo.AddWhereInSQL(DRSheetAttr.Doc, newsql);

                }
               
                qo.addOrderByDesc(DRSheetAttr.RDT);

                //把代码放到表格尾部, 形成 第1,2,3,4,5 页 ....... 
                this.Pub1.Clear();
                this.Pub1.BindPageIdx(qo.GetCount(),
                    pageSize, currPageIdx, "ShareSheet.aspx?1=2&3=xx");

                //每页有15条数据，取第2页的数据.
                qo.DoQuery("OID", pageSize, currPageIdx);

                DataTable dt = ens.ToDataTableField();
                foreach (DataRow dr in dt.Rows)
                {
                    dr["Doc"] = BP.DA.DataType.ParseText2Html(dr["Doc"].ToString());
                }
                this.rep_List.DataSource = dt;
                this.rep_List.DataBind();
            }
              
        }

        //获取评论数据源 
        public DataTable getdt(string id)
        {
            string Ssql = String.Format(" select * from  OA_DRComment   where  FK_COM='{0}'", id);
            DataTable dtcom = BP.DA.DBAccess.RunSQLReturnTable(Ssql);
            return dtcom;

        }
        //用户编号转换成用户名称
        public string GetName(string number)
        {
            string Ssql = String.Format("select Name from Port_Emp where No='{0}'", number);
            string name = BP.DA.DBAccess.RunSQLReturnString(Ssql);

            return name;

        }

        //姓名转换成编号
        public string GetNo(string name)
        {
            string Ssql = String.Format("select No from Port_Emp where Name ='{0}'", name);
            string no = BP.DA.DBAccess.RunSQLReturnString(Ssql);

            return no;

        }
        //日期格式转换
        public static string GetWeekNameOfDay(DateTime idt)
        {
            string dt, week = "";

            dt = idt.DayOfWeek.ToString();
            switch (dt)
            {
                case "Monday":
                    week = "星期一";
                    break;
                case "Tuesday":
                    week = "星期二";
                    break;
                case "Wednesday":
                    week = "星期三";
                    break;
                case "Thursday":
                    week = "星期四";
                    break;
                case "Friday":
                    week = "星期五";
                    break;
                case "Saturday":
                    week = "星期六";
                    break;
                case "Sunday":
                    week = "星期日";
                    break;

            }
            return week;
        }


        public void btnSub_Click(object sender, EventArgs e)
        {



            //获取文本框的值
            string str = "";
            foreach (RepeaterItem item in this.rep_List.Items)
            {
                TextBox txt = (TextBox)item.FindControl("TB_Com");
                if (txt.Visible)
                    str += " " + txt.Text;
            }

            //获取OID的值
            string id = "";
            Button button = (Button)sender;
            //判断HiddenField是否存在
            if (button.NamingContainer.FindControl("HiddenField1") != null)
            {
                //存在，把对象转换为HiddenField控件
                HiddenField hf = (HiddenField)button.NamingContainer.FindControl("HiddenField1");
                //取出HiddenField的Value值。
                id = hf.Value;

                BP.OA.DRComment en = new DRComment();
                en.Comm = str;
                en.FK_COM = id;
                en.Insert();         
            }

            string mesg = "您有新的评论，请查看！";
            string sql = String.Format("select Rec from OA_DRSheet where OID='{0}'", id);
            string userName = BP.DA.DBAccess.RunSQLReturnString(sql);

            BP.OA.ShortMsg.Send_ShortInfo(userName, mesg, String.Empty, false);
            //变量定义: 第几页.
            int currPageIdx = this.PageIdx;
            int pageSize = 12; //页面记录条数.

            //实体查询.
            BP.OA.DRSheets ens = new BP.OA.DRSheets();

            //创建查询对象
            BP.En.QueryObject qo = new QueryObject(ens);
            //qo.AddWhere(DRSheetAttr.Rec,"select *from OA_DRSheet where DRSta=2");

            string sSql = String.Format("select  Doc from OA_DRSheet,OA_DRShare where DRSta=1  and  Rec=Sharer and ShareTo= '{0}' and  Rec!= '{1}' ", BP.Web.WebUser.No, BP.Web.WebUser.No);


            qo.AddWhereInSQL(DRSheetAttr.Doc, sSql);
            qo.addOrderByDesc(DRSheetAttr.RDT);

            //把代码放到表格尾部, 形成 第1,2,3,4,5 页 ....... 
            this.Pub1.Clear();
            this.Pub1.BindPageIdx(qo.GetCount(),
                pageSize, currPageIdx, "PublicSheet.aspx?1=2&3=xx");

            //每页有15条数据，取第2页的数据.
            qo.DoQuery("OID", pageSize, currPageIdx);


            DataTable dt = ens.ToDataTableField();
            foreach (DataRow dr in dt.Rows)
            {
                dr["Doc"] = BP.DA.DataType.ParseText2Html(dr["Doc"].ToString());
            }

            this.rep_List.DataSource = dt;
            this.rep_List.DataBind();

        }

        protected void submit_Click(object sender, EventArgs e)
        {

            //string reque = tb_find.Text;
            string reque = GetNo(tb_find.Text);



            //变量定义: 第几页.
            int currPageIdx = PageIdx;
            int pageSize = 12; //页面记录条数.
            BP.OA.DRSheets ens = new BP.OA.DRSheets();

            //创建查询对象
            BP.En.QueryObject qo = new QueryObject(ens);
            string sSql = String.Format("select Doc from OA_DRSheet where DRSta=1  and Rec ='{0}'", reque);

            qo.AddWhereInSQL(DRSheetAttr.Doc, sSql);
            qo.addOrderByDesc(DRSheetAttr.RDT);

            //把代码放到表格尾部, 形成 第1,2,3,4,5 页 ....... 
            this.Pub1.Clear();
            this.Pub1.BindPageIdx(qo.GetCount(),
                pageSize, currPageIdx, "ShareSheet.aspx?Rec=" + reque);

            //每页有15条数据，取第2页的数据.
            //qo.DoQuery("OID", pageSize, currPageIdx);
            qo.DoQuery();



            DataTable dt = ens.ToDataTableField();
            foreach (DataRow dr in dt.Rows)
            {
                dr["Doc"] = BP.DA.DataType.ParseText2Html(dr["Doc"].ToString());
            }

            this.rep_List.DataSource = dt;
            this.rep_List.DataBind();
        }
    }
}