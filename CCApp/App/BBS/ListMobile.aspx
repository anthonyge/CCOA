﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Mobile.Master" AutoEventWireup="true"
    CodeBehind="ListMobile.aspx.cs" Inherits="CCOA.App.BBS.ListMobile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../CCMobile/css/themes/default/jquery.mobile-1.4.5.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../../CCMobile/css/themes/classic/theme-classic.css" rel="stylesheet"
        type="text/css" />
    <script src="../../CCMobile/js/jquery.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/comment/action.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div data-role="page" data-theme="c">
        <div data-role="header" data-position="fixed" data-theme="b">
            <a href="/CCMobile/Home.aspx" data-icon="black" data-role="button ">返回</a>
            <h2>
                论坛</h2>
        </div>
        <%
            BP.OA.BBS.BBSClasss bbsClasss = new BP.OA.BBS.BBSClasss();
            bbsClasss.RetrieveAll();
        %>
        <div data-role="main" style="padding: 0px;">
            <ul data-role="listview" data-inset="true">
                <%
                
                    foreach (BP.OA.BBS.BBSClass item in bbsClasss)
                    {
                   
                    
                %>
                <li><a href="ListMobileForum.aspx?FK_Class=<%=item.No%>">
                    <img src="images/Draft.png" />
                    板块：<%=item.Name%>
                    <br />
                    <br />
                    <font color="green" size="-3">简介：<%=item.ReMark%>
                    </font></a></li>
                <%
                    } %>
            </ul>
        </div>
    </div>
</asp:Content>
