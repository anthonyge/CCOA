using System;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.KM
{
	/// <summary>
	/// 知识树到人员属性
	/// </summary>
	public class TreeEmpAttr
	{
		/// <summary>
		/// 知识树
		/// </summary>
		public const string RefTreeNo="RefTreeNo";
		/// <summary>
		/// 到人员
		/// </summary>
		public const string FK_Emp="FK_Emp";
	}
	/// <summary>
	/// 知识树到人员
	/// 知识树的到人员有两部分组成.	 
	/// 记录了从一个知识树到其他的多个知识树.
	/// 也记录了到这个知识树的其他的知识树.
	/// </summary>
	public class TreeEmp :EntityMM
	{
		#region 基本属性
		/// <summary>
		///知识树
		/// </summary>
		public string  RefTreeNo
		{
			get
			{
				return this.GetValStrByKey(TreeEmpAttr.RefTreeNo);
			}
			set
			{
				this.SetValByKey(TreeEmpAttr.RefTreeNo,value);
			}
		}
		/// <summary>
		/// 到人员
		/// </summary>
		public string FK_Emp
		{
			get
			{
				return this.GetValStringByKey(TreeEmpAttr.FK_Emp);
			}
			set
			{
				this.SetValByKey(TreeEmpAttr.FK_Emp,value);
			}
		}
        public string FK_EmpT
        {
            get
            {
                return this.GetValRefTextByKey(TreeEmpAttr.FK_Emp);
            }
        }
		#endregion 

		#region 构造方法
		/// <summary>
		/// 知识树到人员
		/// </summary>
		public TreeEmp(){}
		/// <summary>
		/// 重写基类方法
		/// </summary>
		public override Map EnMap
		{
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("KM_TreeEmp");
                map.EnDesc = "人员";

                map.DepositaryOfEntity = Depositary.None;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBStringPK(TreeEmpAttr.RefTreeNo, null, "树", true, true, 1, 100, 100);
                map.AddDDLEntitiesPK(TreeEmpAttr.FK_Emp, null, "人员", new Emps(), true);

                this._enMap = map;
                return this._enMap;
            }
		}
		#endregion
	}
	/// <summary>
	/// 知识树到人员
	/// </summary>
    public class TreeEmps : EntitiesMM
    {
        /// <summary>
        /// 他的到人员
        /// </summary>
        public Emps HisEmps
        {
            get
            {
                Emps ens = new Emps();
                foreach (TreeEmp ns in this)
                {
                    ens.AddEntity(new Emp(ns.FK_Emp));
                }
                return ens;
            }
        }
        /// <summary>
        /// 知识树到人员
        /// </summary>
        public TreeEmps() { }
        /// <summary>
        /// 知识树到人员
        /// </summary>
        /// <param name="NodeID">知识树ID</param>
        public TreeEmps(int NodeID)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(TreeEmpAttr.RefTreeNo, NodeID);
            qo.DoQuery();
        }
        /// <summary>
        /// 知识树到人员
        /// </summary>
        /// <param name="EmpNo">EmpNo </param>
        public TreeEmps(string EmpNo)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(TreeEmpAttr.FK_Emp, EmpNo);
            qo.DoQuery();
        }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new TreeEmp();
            }
        }
        /// <summary>
        /// 转向此知识树的集合的 Nodes
        /// </summary>
        /// <param name="nodeID">此知识树的ID</param>
        /// <returns>转向此知识树的集合的Nodes (FromNodes)</returns> 
        public Emps GetHisEmps(int nodeID)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(TreeEmpAttr.RefTreeNo, nodeID);
            qo.DoQuery();

            Emps ens = new Emps();
            foreach (TreeEmp en in this)
            {
                ens.AddEntity(new Emp(en.FK_Emp));
            }
            return ens;
        }
    }
}
