﻿var title = "签到";

//初始化
$(function () {
    LoadGrid();
});


//加载列表
function LoadGrid() {
    $("#pageloading").show();
    Application.data.getRegister(callBack, this);
}
//签到 回调函数
function callBack(jsonData, scope) {
    if (jsonData) {
        var pushData = eval('(' + jsonData + ')');
        var grid = $("#maingrid").ligerGrid({
            columns: [
                   { display: '登记类型', name: 'No', width: 380, align: 'left' },
                   { display: '规定时间', name: 'RegularTime' },
                   { display: '登记时间', name: 'RegisterTime' },
                   { display: '状态', name: 'FK_RWState' },
                   { display: '备注', name: 'Demo' },
                   { display: '签到', name: 'No', render: function (rowdata, rowindex) {
                       if (rowdata.RegisterTime != "") {
                           return "已签到";
                       } else {
                           var title = "<a href=\"javascript:RWClick('" + rowdata.No + "')\">签到</a>";
                           return title;
                       }

                   }
                   }
                   ],
            pageSize: 20,
            data: pushData,
            rownumbers: true,
            height: "99%",
            width: "99%",
            columnWidth: 100,
            onReload: LoadGrid
        });
    }
    else {
        $.ligerDialog.warn('加载数据出错，请关闭后重试！');
    }
    $("#pageloading").hide();
}

//清空 控件
function SetEmpty() {
    $("#txtrwtype").val('');
    $("#TB_Demo").val('');
}

//签到 操作
function RWClick(obj) {
    var op = obj;
    $.ligerDialog.open({
        target: $("#stateInfo"),
        title: title,
        width: 600,
        height: 420,
        isResize: true,
        modal: true,
        buttons: [
        { text: '保存', onclick: function (i, d) {
            var demo = $("#TB_Demo").val();
            demo = encodeURI(demo);
            Application.data.registerWork(op, demo, function (js) {
                if (js == "1") {
                    SetEmpty();
                    d.hide();
                    LoadGrid();
                }
                else {
                    $.ligerDialog.warn('请重新操作！');
                }
            }, this);
        }
        }, { text: '关闭', onclick: function (i, d) {
            SetEmpty();
            d.hide();
        }
        }]
    });
}
//**********************************请假*****************************************

function SetEmptyQJ() {
    $("#txtQJtime1").val('');
    $("#txtQJtime2").val('');
    $("#txtQJYY").val('');
}

function LoadQJ() {
    Application.data.getQJ(callBackQJ, this);
}
//请假 操作
function QJ_Click() {
    var time1 = $("#txtQJtime1").val();
    var time2 = $("#txtQJtime2").val();
    var demo = $("#txtQJYY").val();
    Application.data.registerQJ(time1, time2, demo, function (js) {
        if (js == "1") {
            // alert("保存成功");
            put_css(3);
            LoadQJ();
        }
    }, this);
}
//请假 回调函数
function callBackQJ(jsonData, scope) {
    if (jsonData) {
        var pushData = eval('(' + jsonData + ')');
        var grid = $("#maingridQJ").ligerGrid({
            columns: [
                   { display: '开始时间', name: 'StartTime' },
                   { display: '结束时间', name: 'EndTime' },
                   { display: '请假天数', name: 'DCount' },
                      { display: '原因', name: 'Demo' },
                   { display: '登记时间', name: 'RecordTime' }
                   ],
            pageSize: 20,
            data: pushData,
            rownumbers: true,
            height: "99%",
            width: "99%",
            columnWidth: 100,
            onReload: LoadQJ
        });
    }
}
//**********************外出*********************************************
function SetEmptyWC() {
    $("#txtWCtime1").val('');
    $("#txtWCtime2").val('');
    $("#txtWCYY").val('');
    $("#txtWCD").val('');
}

function LoadWC() {
    Application.data.getWC(callBackWC, this);
}
//外出 操作
function WC_Click() {
    var time1 = $("#txtWCtime1").val();
    var time2 = $("#txtWCtime2").val();
    var demo = $("#txtWCYY").val();
    var addr = $("#txtWCD").val();
    Application.data.registerWC(time1, time2, demo, addr, function (js) {
        if (js == "1") {
            // alert("保存成功");
            put_css(2);
            LoadWC();
        }
    }, this);
}
//外出 回调函数
function callBackWC(jsonData, scope) {
    if (jsonData) {
        var pushData = eval('(' + jsonData + ')');
        var grid = $("#maingridWC").ligerGrid({
            columns: [
                   { display: '开始时间', name: 'StartTime' },
                   { display: '结束时间', name: 'EndTime' },
                     { display: '目的地', name: 'Addr' },
                   { display: '原因', name: 'Demo' },
                   { display: '登记时间', name: 'RecordTime' }
                   ],
            pageSize: 20,
            data: pushData,
            rownumbers: true,
            height: "99%",
            width: "99%",
            columnWidth: 100,
            onReload: LoadWC
        });
    }
}
//**********************出差*********************************************
function SetEmptyCC() {
    $("#txtCCtime1").val('');
    $("#txtCCtime2").val('');
    $("#txtCCYY").val('');
    $("#txtAddr").val('');
}

function LoadCC() {
    Application.data.getCC(callBackCC, this);
}
//出差 操作
function CC_Click() {
    var time1 = $("#txtCCtime1").val();
    var time2 = $("#txtCCtime2").val();
    var demo = $("#txtCCYY").val();
    var addr = $("#txtAddr").val();
    Application.data.registerCC(time1, time2, demo, addr, function (js) {
        if (js == "1") {
            // alert("保存成功");
            put_css(4);
            LoadCC();
        }
    }, this);
}
//出差 回调函数
function callBackCC(jsonData, scope) {
    if (jsonData) {
        var pushData = eval('(' + jsonData + ')');
        var grid = $("#maingridCC").ligerGrid({
            columns: [
                   { display: '开始时间', name: 'StartTime' },
                   { display: '结束时间', name: 'EndTime' },
                     { display: '目的地', name: 'Addr' },
                   { display: '原因', name: 'Demo' },
                   { display: '登记时间', name: 'RecordTime' }
                   ],
            pageSize: 20,
            data: pushData,
            rownumbers: true,
            height: "99%",
            width: "99%",
            columnWidth: 100,
            onReload: LoadCC
        });
    }
}