using System;
using System.Collections;
using BP.DA;
using BP.En;

namespace BP.OA
{
	/// <summary>
	/// 共享人共享给属性
	/// </summary>
    public class DRShareAttr
    {
        /// <summary>
        /// 共享人
        /// </summary>
        public const string Sharer = "Sharer";
        /// <summary>
        /// 共享给
        /// </summary>
        public const string ShareTo = "ShareTo";
        
    }
    /// <summary>
    /// 共享人共享给  
    /// </summary>
    public class DRShare : EntityMM
    {
        #region 基本属性
        /// <summary>
        ///共享人
        /// </summary>
        public string Sharer
        {
            get
            {
                return this.GetValStringByKey(DRShareAttr.Sharer);
            }
            set
            {
                this.SetValByKey(DRShareAttr.Sharer, value);
            }
        }
        /// <summary>
        /// 共享给
        /// </summary>
        public string ShareTo
        {
            get
            {
                return this.GetValStringByKey(DRShareAttr.ShareTo);
            }
            set
            {
                this.SetValByKey(DRShareAttr.ShareTo, value);
            }
        }
        #endregion

        #region 构造方法
        /// <summary>
        /// 共享人共享给
        /// </summary>
        public DRShare() { }
        /// <summary>
        /// 重写基类方法
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_DRShare");
                map.EnDesc = "共享人共享给";

                map.DepositaryOfEntity = Depositary.None;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBStringPK(DRShareAttr.Sharer, null, "共享人", true, true,1,100,100);
                map.AddTBStringPK(DRShareAttr.ShareTo, null, "共享给", true, true, 1, 100, 100);
                

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
	/// <summary>
	/// 共享人共享给
	/// </summary>
    public class DRShares : EntitiesMM
    {
        /// <summary>
        /// 共享人共享给
        /// </summary>
        public DRShares() { }
        /// <summary>
        /// 共享人共享给
        /// </summary>
        /// <param name="NodeID">共享人ID</param>
        public DRShares(int NodeID)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(DRShareAttr.Sharer, NodeID);
            qo.DoQuery();
        }
        /// <summary>
        /// 共享人共享给
        /// </summary>
        /// <param name="EmpNo">EmpNo </param>
        public DRShares(string EmpNo)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(DRShareAttr.ShareTo, EmpNo);
            qo.DoQuery();
        }
        /// <summary>
        /// 得调用它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DRShare();
            }
        }
    }
}
