﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/AppMaster/AppSite.Master" AutoEventWireup="true"   CodeBehind="RoomListEUI.aspx.cs" Inherits="CCOA.App.Meeting.RoomListEUI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <script src="../../Js/Validform_v5.3.2_min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var GetNo;
        //加载grid后回调函数
        function LoadDataGridCallBack(js, scorp) {
            $("#pageloading").hide();
            if (js == "") js = "[]";

            //系统错误
            if (js.status && js.status == 500) {
                $("body").html("<b>访问页面出错，请联系管理员。<b>");
                return;
            }

            var pushData = eval('(' + js + ')');
            $('#newsGrid').datagrid({
                columns: [[
                     { checkbox: true },
                      { field: 'Name', title: '会议室名称', width: 50, align: 'left' },
                       { field: 'Capacity', title: '容纳人数', width: 50, align: 'center' },
                     { field: 'Fk_EmpName', title: '会议室管理员', width: 70, align: 'center' },
                    { field: 'ResourceName', title: '会议室资源描述', width: 70, align: 'center' },
                    { field: 'RoomSta', title: '状态', width: 70, align: 'center', formatter: function (value, row, index) {
                        var result = "正常";
                        if (row.RoomSta != null && row.RoomSta == "1") {
                            result = "维护中";
                        }
                        return result;
                    }
                    },
                    { field: 'Note', title: '会议室说明', width: 70, align: 'center' }
                    ]],
                idField: 'No',
                selectOnCheck: false,
                checkOnSelect: true,
                singleSelect: true,
                data: pushData,
                width: 'auto',
                height: 'auto',
                striped: true,
                rownumbers: true,
                pagination: true,
                remoteSort: false,
                fitColumns: true,
                pageNumber: scorp.pageNumber,
                pageSize: scorp.pageSize,
                pageList: [20, 30, 40, 50],
                onDblClickCell: function (index, field, value) {
                },
                loadMsg: '数据加载中......'
            });
            //分页
            var pg = $("#newsGrid").datagrid("getPager");
            if (pg) {
                $(pg).pagination({
                    onRefresh: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    },
                    onSelectPage: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    }
                });
            }
        }
        //加载grid
        function LoadGridData(pageNumber, pageSize) {
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            var keyWords = document.getElementById("TB_KeyWords").value;
            var params = {
                method: "RoomListMet",
                keyWords: encodeURI(keyWords),
                GetNo: GetNo,
                pageNumber: pageNumber,
                pageSize: pageSize
            };
            queryData(params, LoadDataGridCallBack, this);
        }
        function RefreshGrid() {
            var grid = $('#newsGrid');
            var options = grid.datagrid('getPager').data("pagination").options;
            var curPage = options.pageNumber;
            var pageSize = options.pageSize;
            LoadGridData(curPage, pageSize);
        }
        //初始化
        $(function () {
            $("#newsGrid").Validform({
                tiptype: 2,
                showAllError: true
            });
            LoadGridData(1, 20);
        });

        //公共方法
        function queryData(param, callback, scope, method, showErrMsg) {
            if (!method) method = 'GET';
            $.ajax({
                type: method, //使用GET或POST方法访问后台
                dataType: "text", //返回json格式的数据
                contentType: "application/json; charset=utf-8",
                url: "MeetingEUI.ashx", //要访问的后台地址
                data: param, //要发送的数据
                async: false,
                cache: false,
                complete: function () { }, //AJAX请求完成时隐藏loading提示
                error: function (XMLHttpRequest, errorThrown) {
                    callback(XMLHttpRequest);
                },
                success: function (msg) {//msg为返回的数据，在这里做数据绑定
                    var data = msg;
                    callback(data, scope);
                }
            });
        }
        function DelSelections() {
            var rows = $('#newsGrid').datagrid('getChecked');
            if (rows.length == 0) {
                alert("您没有选中项!");
            }
            else {
                if (confirm("确定删除所选项?")) {
                    var ids = [];
                    for (var i = 0; i < rows.length; i++) {
                        ids.push(rows[i].No);
                    }
                    GetNo = ids.join(',');
                    RefreshGrid();
                }
            }
        }
        function DelAll() {
            $.messager.confirm('确认', '您确认想要删除记录吗？', function (r) {
                if (r) {
                    var ids = [];
                    var getAll = $("#newsGrid").datagrid("getRows");
                    for (var i = 0; i < getAll.length; i++) {
                        ids.push(getAll[i]['No']);
                    }
                    GetNo = ids.join(',');
                    //调用
                    RefreshGrid();
                }
            });

        }
        //添加会议室
        var diag = new Dialog();
        function openDialog(url, title, height, width) {
            $("<div id='dialogEnPanel'></div>").append($("<iframe width='100%' height='100%' frameborder=0 src='" + url + "'/>")).dialog({
                title: title,
                width: width,
                height: height,
                autoOpen: true,
                modal: true,
                resizable: true,
                onClose: function () {
                    $("#dialogEnPanel").remove();
                    var pg = $('#newsGrid').datagrid('getPager');
                    var curPage = $(pg).pagination.pageNumber;
                    LoadGridData(curPage, 20);
                }
            });
        }
        
        function DoRefresh() {
            window.location.reload()
        }
        //修改行数据注意getRows,getChecked与getSelections的区别
        function RepairMeetingRoom() {
            var ids = [];
            var rows = $('#newsGrid').datagrid('getChecked');
            if (rows.length == 0) {
                $.messager.alert("提示", "请选择单条数据!");
            }
            else {
                openDialog('AddRoom.aspx?No=' + rows[0].No, '修改会议室', 420, 600);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<div id="pageloading">
    </div>--%>
    <div data-options="region:'center'" border="false" style="margin: 0; padding: 0;
        overflow: hidden;">
        <div id="tb" style="padding: 6px; height: 28px;">
            <div style="float: left; margin-left: 5px;">
                关键字：<input id="TB_KeyWords" type="text" style="width: 150px; border-style: solid;
                    border-color: #aaaaaa;" />
                <a id="DoQueryByKey" href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'"
                    onclick="LoadGridData(1, 20)">查询</a>
            </div>
            <a href="javascript:void(0)" style="float: left;" onclick="openDialog('AddRoom.aspx','添加会议室',420,600)"
                    class="easyui-linkbutton" data-options="toggle:true,group:'g2',plain:true,iconCls:'icon-add'">
                    添加会议室</a>
            <a id="A2" href="#" style="float: left;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-edit'"
                onclick="RepairMeetingRoom()">修改会议室</a> 
        <div class="datagrid-btn-separator">
                </div>
            <a id="ShowAll" href="#" style="float: left;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-delete'"
                onclick="DelSelections()">删除所选</a> <a id="A1" href="#" style="float: left;" class="easyui-linkbutton"
                    data-options="plain:true,iconCls:'icon-delete'" onclick="DelAll()">清空所有</a>
            
            
        </div>
        <table id="newsGrid" fit="true" fitcolumns="true" toolbar="#tb" class="easyui-datagrid">
        </table>
    </div>
</asp:Content>
