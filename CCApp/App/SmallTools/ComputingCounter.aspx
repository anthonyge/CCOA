﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComputingCounter.aspx.cs"
    Inherits="CCOA.App.SmallTools.ComputingCounter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>科学计算器</title>
</head>
<body scroll="no" onload="document.form.input.focus();">
    <form name="form" method="post" action="javascript:doit();" id="from1">
    <div align="center">
        <table width="260" border="0" height="260" align="center" bordercolor="#5c8dd9" bgcolor="#5c8dd9"
            id="Table1">
            <tr bgcolor="#5c8dd9">
                <td colspan="7" height="2">
                    <div align="center">
                        <b><font face="Arial, Helvetica, sans-serif" color="#FFFFFF">科学计算器</font></b></div>
                </td>
            </tr>
            <tr bgcolor="#5c8dd9">
                <td colspan="7" height="2">
                    <div align="center">
                        <input type="text" name="input" size="40" id="Text1" />
                    </div>
                </td>
            </tr>
            <tr bgcolor="#5c8dd9">
                <td width="50" height="4">
                    <input type="button" name="one" value="1" onclick="form.input.value += '1'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button1">
                </td>
                <td width="50" height="4">
                    <input type="button" name="two" value="2" onclick="form.input.value += '2'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button2">
                </td>
                <td width="50" height="4">
                    <input type="button" name="three" value="3" onclick="form.input.value += '3'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button3">
                </td>
                <td width="20" height="4">
                </td>
                <td width="50" height="4">
                    <input type="button" name="clear" value="C" onclick="form.input.value = ''" style="color: #FFFFFF;
                        background-color: #9F0004; height: 25 px; width: 40px" id="Button4">
                </td>
                <td width="50" height="4">
                    <input type="button" name="percent" value=" % " onclick="form.input.value = eval(form.input.value) / 100"
                        style="color: #FFFFFF; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button5">
                </td>
                <td width="50" height="4">
                    <input type="button" name="(" value=" ( " onclick="form.input.value += '('" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button6">
                </td>
            </tr>
            <tr bgcolor="#5c8dd9">
                <td width="50" height="2">
                    <input type="button" name="four" value="4" onclick="form.input.value += '4'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button7">
                </td>
                <td width="50" height="2">
                    <input type="button" name="five" value="5" onclick="form.input.value += '5'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button8">
                </td>
                <td width="50" height="2">
                    <input type="button" name="six" value="6" onclick="form.input.value += '6'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button9">
                </td>
                <td width="20" height="2">
                </td>
                <td width="50" height="2">
                    <input type="button" name="times" value="  x  " onclick="form.input.value += ' * '"
                        style="color: #FFFFFF; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button10">
                </td>
                <td width="50" height="2">
                    <input type="button" name="div" value="  /  " onclick="form.input.value += ' / '"
                        style="color: #FFFFFF; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button11">
                </td>
                <td width="50" height="2">
                    <input type="button" name=")" value=" ) " onclick="form.input.value += ')'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button12">
                </td>
            </tr>
            <tr bgcolor="#5c8dd9">
                <td width="50" height="2">
                    <input type="button" name="seven" value="7" onclick="form.input.value += '7'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button13">
                </td>
                <td width="50" height="2">
                    <input type="button" name="eight" value="8" onclick="form.input.value += '8'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button14">
                </td>
                <td width="50" height="2">
                    <input type="button" name="nine" value="9" onclick="form.input.value += '9'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button15">
                </td>
                <td width="20" height="2">
                </td>
                <td width="50" height="2">
                    <input type="button" name="plus" value="  +  " onclick="form.input.value += ' + '"
                        style="color: #FFFFFF; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button16">
                </td>
                <td width="50" height="2">
                    <input type="button" name="minus" value="  -  " onclick="form.input.value += ' - '"
                        style="color: #FFFFFF; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button17">
                </td>
                <td width="50" height="2">
                    <input type="button" name="round" value="Rnd" onclick="Round()" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button18">
                </td>
            </tr>
            <tr bgcolor="#5c8dd9">
                <td width="50" height="2">
                    <input type="button" name="zero" value="0" onclick="form.input.value += '0'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button19">
                </td>
                <td width="50" height="2">
                    <input type="button" name="point" value="." onclick="form.input.value += '.'" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button20">
                </td>
                <td width="50" height="2">
                    <input type="button" name="pi" value="PI" onclick="form.input.value += '3.1415926535897932384626433832795'"
                        style="color: #FFFFFF; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button21">
                </td>
                <td width="20" height="2">
                </td>
                <td width="50" height="2">
                    <input type="button" name="pi2" value="+/-" onclick="Neg()" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button22">
                </td>
                <td width="50" height="2">
                    <input type="button" name="DoIt" value=" = " onclick="doit()" style="color: #FFFFFF;
                        background-color: #9f0004; height: 25 px; width: 40px" id="Button23">
                </td>
                <td width="50" height="2">
                    <input type="button" name="round2" value="Ran#" onclick="Ran()" style="color: #FFFFFF;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button24">
                </td>
            </tr>
            <tr bgcolor="#5c8dd9">
                <td width="50" height="24">
                </td>
                <td width="50" height="24">
                </td>
                <td width="50" height="24">
                </td>
                <td width="20" height="24">
                </td>
                <td width="50" height="24">
                </td>
                <td width="50" height="24">
                </td>
                <td width="50" height="24">
                </td>
            </tr>
            <tr bgcolor="#5c8dd9">
                <td width="50">
                    <input type="button" name="quad" value="^2" onclick="form.input.value = form.input.value * form.input.value"
                        style="color: #FFFFFF; background-color: #333333; height: 25 px; width: 40px"
                        id="Button25">
                </td>
                <td width="50">
                    <input type="button" name="root" value="root" onclick="Root()" style="color: #FFFFFF;
                        background-color: #333333; height: 25 px; width: 40px" id="Button26">
                </td>
                <td width="50">
                    <input type="button" name="ln" value="ln" onclick="Ln()" style="color: #FFFFFF; background-color: #333333;
                        height: 25 px; width: 40px" id="Button27">
                </td>
                <td width="20">
                </td>
                <td width="50">
                    <input type="button" name="1/2" value="1/2" onclick="form.input.value += '0.5'" style="color: #ffffff;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button28">
                </td>
                <td width="50">
                    <input type="button" name="1/3" value="1/3" onclick="form.input.value += '0.3333333333'"
                        style="color: #ffffff; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button29">
                </td>
                <td width="50">
                    <input type="button" name="1/4" value="1/4" onclick="form.input.value += '0.25'"
                        style="color: #ffffff; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button30">
                </td>
            </tr>
            <tr bgcolor="#5c8dd9">
                <td width="50">
                    <input type="button" name="sin" value="sin" onclick="Sin()" style="color: #FFFFFF;
                        background-color: #333333; height: 25 px; width: 40px" id="Button31">
                </td>
                <td width="50">
                    <input type="button" name="cos" value="cos" onclick="Cos()" style="color: #FFFFFF;
                        background-color: #333333; height: 25 px; width: 40px" id="Button32">
                </td>
                <td width="50">
                    <input type="button" name="tan" value="tan" onclick="Tan()" style="color: #FFFFFF;
                        background-color: #333333; height: 25 px; width: 40px" id="Button33">
                </td>
                <td width="20">
                </td>
                <td width="50">
                    <input type="button" name="1/5" value="1/5" onclick="form.input.value += '0.2'" style="color: #ffffff;
                        background-color: #5c8dd9; height: 25 px; width: 40px" id="Button34">
                </td>
                <td width="50">
                    <input type="button" name="1/6" value="1/6" onclick="form.input.value += '0.1666666667'"
                        style="color: #ffffff; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button35">
                </td>
                <td width="50">
                    <input type="button" name="1/7" value="1/7" onclick="form.input.value += '0.1428571429'"
                        style="color: #ffffff; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button36">
                </td>
            </tr>
            <tr bgcolor="#5c8dd9">
                <td width="50">
                    <input type="button" name="sin2" value="asin" onclick="Isin()" style="color: #FFFFFF;
                        background-color: #333333; height: 25 px; width: 40px" id="Button37">
                </td>
                <td width="50">
                    <input type="button" name="cos2" value="acos" onclick="Icos()" style="color: #FFFFFF;
                        background-color: #333333; height: 25 px; width: 40px" id="Button38">
                </td>
                <td width="50">
                    <input type="button" name="tan2" value="atan" onclick="Itan()" style="color: #FFFFFF;
                        background-color: #333333; height: 25 px; width: 40px" id="Button39">
                </td>
                <td width="20">
                </td>
                <td width="50">
                    <input type="button" name="1/8" value="1/8" onclick="form.input.value += '0.125'"
                        style="color: #ffffff; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button40">
                </td>
                <td width="50">
                    <input type="button" name="1/9" value="1/9" onclick="form.input.value += '0.1111111111'"
                        style="color: #ffffff; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button41">
                </td>
                <td width="50">
                    <input type="button" name="1/10" value="1/10" onclick="form.input.value += '0.1'"
                        style="color: #ffffff; background-color: #5c8dd9; height: 25 px; width: 40px"
                        id="Button42">
                </td>
            </tr>
        </table>
    </div>
    </form>
    <script type="text/javascript">
        function doit() {
            form.input.value = eval(form.input.value)
        }
        function Cos() {
            x = form.input.value
            if (x == '') alert('Error: Input Required');
            else form.input.value = Math.cos(x);
        }
        function Sin() {
            x = form.input.value
            if (x == '') alert('Error: Input Required');
            else form.input.value = Math.sin(x);
        }
        function Ln() {
            x = form.input.value
            if (x == '') alert('Error: Input Required');
            else form.input.value = Math.log(x);
        }
        function Root() {
            x = form.input.value
            if (x == '') alert('Error: Input Required');
            else form.input.value = Math.sqrt(x);
        }
        function Tan() {
            x = form.input.value
            if (x == '') alert('Error: Input Required');
            else form.input.value = Math.tan(x);
        }
        function Icos() {
            x = form.input.value
            if (x == '') alert('Error: Input Required');
            else form.input.value = Math.acos(x);
        }
        function Isin() {
            x = form.input.value
            if (x == '') alert('Error: Input Required');
            else form.input.value = Math.asin(x);
        }
        function Itan() {
            x = form.input.value
            if (x == '') alert('Error: Input Required');
            else form.input.value = Math.atan(x);
        }
        function Round() {
            x = form.input.value
            if (x == '') alert('Error: Input Required');
            else form.input.value = Math.round(x);
        }
        function Ran() {
            x = form.input.value
            form.input.value = Math.random(x);
        }
        function Neg() {
            x = form.input.value
            if (x == '') alert('Error: Input Required');
            else x = parseFloat(x) * -1;
        }
        function del() {
            x = form.input.value
            x = (x.substring) - 1
        }
    </script>
</body>
</html>
