﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Mobile.Master" AutoEventWireup="true"
    CodeBehind="ListMobileForum.aspx.cs" Inherits="CCOA.App.BBS.ListMobileForum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../CCMobile/css/themes/default/jquery.mobile-1.4.5.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../../CCMobile/css/themes/classic/theme-classic.css" rel="stylesheet"
        type="text/css" />
    <script src="../../CCMobile/js/jquery.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/comment/action.js" type="text/javascript"></script>
    <style type="text/css">
        th
        {
            border-bottom: 1px solid #d6d6d6;
        }
        
        tr:nth-child(even)
        {
            background: #e9e9e9;
        }
        .style1
        {
            width: 30%;
        }
        .style2
        {
            width: 5%;
        }
        .style3
        {
            width: 10%;
        }
        .style4
        {
            width: 10%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div data-role="page" data-theme="c">
        <div data-role="header" data-position="fixed" data-theme="b">
            <a href="ListMobile.aspx" data-icon="black" data-role="button ">返回</a>
            <h2>
                主题列表</h2>
        </div>
        <%
            string FK_Class = this.Request.QueryString["FK_Class"];
            BP.OA.BBS.BBSMotifs motifs = new BP.OA.BBS.BBSMotifs();

            BP.En.QueryObject qo = new BP.En.QueryObject(motifs);
            qo.AddWhere(BP.OA.BBS.BBSMotifAttr.FK_Class, FK_Class);
            qo.addOrderByDesc(BP.OA.BBS.BBSMotifAttr.LastReTime);
            qo.DoQuery();
        %>
        <div class="ui-content" data-role="main" style="padding: 0px;">
            <ul data-role="listview" data-inset="true">
                <%
                    foreach (BP.OA.BBS.BBSMotif itemCat in motifs)
                    {
                %>
                <li><a href="ThreadMobile.aspx?FK_Class=<%=itemCat.FK_Class%>&FK_Motif=<%=itemCat.No%>">
                    标题：<%=itemCat.Name %>
                    <br />
                    <font color="green" size="-3">
                    发布人：<%=itemCat.FK_Emp%> | 浏览/回复:<%=itemCat.Hits%>/<%=itemCat.Replays%>
                    <br />最后回复时间：<%=BP.DA.DataType.ParseSysDate2DateTimeFriendly( itemCat.LastReTime.ToString()) %>
                    </font>
                    </a> </li>
                <%} %>
            </ul>
        </div>
        
    </div>
</asp:Content>
