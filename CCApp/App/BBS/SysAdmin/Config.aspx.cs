﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA;
using BP.OA.BBS;
using System.Data;
using System.Text;



namespace CCOA.App.BBS.SysAdmin
{
    public partial class Config : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
           
            if (!IsPostBack)
            {
                BBSConfig bbscomfig = new BBSConfig(0); 
                this.txt_webName.Text = bbscomfig.WebName;
                this.txt_webSite.Text = bbscomfig.WebSite;
                this.txt_keywords.Text = bbscomfig.KeyWords;
                this.txt_description.Text = bbscomfig.Description;
                this.txt_filter.Text = bbscomfig.Filter;
                this.txt_F_exp.Text = bbscomfig.F_exp;
                this.txt_F_integral.Text = bbscomfig.F_integral;
                this.txt_F_gold.Text = bbscomfig.F_gold;
                this.txt_R_exp.Text = bbscomfig.R_exp;
                this.txt_R_integral.Text = bbscomfig.R_integral;
                this.txt_R_gold.Text = bbscomfig.R_gold;
                //this.WaterSet.SelectedValue =Convert.ToString(bbscomfig.WaterSet);
                //this.txt_Water.Text = Convert.ToString(bbscomfig.WaterTxt);
            }
            

        }
        private void Alert(String msg)
        {
            BP.OA.Debug.Alert(this, msg);
        }

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            BBSConfig bbscomfig = new BBSConfig(0);
            bbscomfig.WebName =txt_webName.Text;
            bbscomfig.WebSite = txt_webSite.Text;
            bbscomfig.KeyWords = txt_keywords.Text;
            bbscomfig.Description = txt_description.Text;
            bbscomfig.Filter = txt_filter.Text;
            bbscomfig.F_exp = txt_F_exp.Text;
            bbscomfig.F_integral = txt_F_integral.Text;
            bbscomfig.F_gold = txt_F_gold.Text;
            bbscomfig.R_exp=txt_R_exp.Text;
            bbscomfig.R_integral=txt_R_integral.Text;
            bbscomfig.R_gold=txt_R_gold.Text;
            bbscomfig.WaterSet = BP.OA.BBS.WaterSet.none;//WaterSet.SelectedValue;
            //bbscomfig.WaterTxt=txt_Water.Text;
            
            //验证
            StringBuilder sbErrs = new StringBuilder();
            if (String.IsNullOrEmpty(txt_webSite.Text))
            {
                sbErrs.Append("域名不能为空！");
            }
            if (String.IsNullOrEmpty(txt_webName.Text))
            {
                sbErrs.Append("网站名称不能为空！");
            }
            if (String.IsNullOrEmpty(txt_keywords.Text))
            {
                sbErrs.Append("关键字不能为空！");
            }
            if (String.IsNullOrEmpty(txt_F_exp.Text))
            {
                sbErrs.Append("发帖加经验不能为空！");
            }
            if (String.IsNullOrEmpty(txt_F_integral.Text))
            {
                sbErrs.Append("发帖加积分不能为空！");
            }
            if (String.IsNullOrEmpty(txt_F_gold.Text))
            {
                sbErrs.Append("发帖加金币不能为空！");
            }
            if (String.IsNullOrEmpty(txt_R_exp.Text))
            {
                sbErrs.Append("回帖加经验不能为空！");
            }
            if (String.IsNullOrEmpty(txt_R_integral.Text))
            {
                sbErrs.Append("回帖加积分不能为空！");
            }
            if (String.IsNullOrEmpty(txt_R_gold.Text))
            {
                sbErrs.Append("回帖加金币不能为空！");
            }
            if (sbErrs.Length != 0)
            {
                this.Alert(sbErrs.ToString());
                return;
            }
            bbscomfig.Update();
          
        }
    }
}