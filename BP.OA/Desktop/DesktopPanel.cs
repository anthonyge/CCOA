﻿using System;
using System.Collections.Generic;
using System.Text;

using BP.DA;
using BP.En;

namespace BP.OA.Desktop
{
    public enum PanelType
    {
        ImgModel,//图片，轮换图
        OutHref,//外部链接
        ListModel,//列表
        DataTableModel,//表格式
        Chart//图表
    }

    /// <summary>
    /// 信息块属性
    /// </summary>
    public class DesktopPanelAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 图标
        /// </summary>
        public const string PanelIcon = "PanelIcon";
        /// <summary>
        /// 更多地址
        /// </summary>
        public const string ExtUrl = "ExtUrl";
        /// <summary>
        /// 信息块类型
        /// </summary>
        public const string PanelType = "PanelType";
        /// <summary>
        /// 表名
        /// </summary>
        public const string PTable = "PTable";
        /// <summary>
        /// 标题列名
        /// </summary>
        public const string TitleColumn = "TitleColumn";
        /// <summary>
        /// 更新时间
        /// </summary>
        public const string EDT = "EDT";
        /// <summary>
        /// 图片内容URL
        /// </summary>
        public const string ImgUrl = "ImgUrl";
        /// <summary>
        /// 排序字段
        /// </summary>
        public const string OrderColumn = "OrderColumn";
        /// <summary>
        /// 显示行数
        /// </summary>
        public const string AllowRows = "AllowRows";
        /// <summary>
        /// 外部主键
        /// </summary>
        public const string FK_MyPK = "FK_MyPK";
        /// <summary>
        /// 查询字段
        /// </summary>
        public const string ColumnName = "ColumnName";
        /// <summary>
        /// 查询字段中文名
        /// </summary>
        public const string ColumnDesc = "ColumnDesc";
        /// <summary>
        /// 外部链接
        /// </summary>
        public const string OutHref = "OutHref";
        /// <summary>
        /// 是否启用
        /// </summary>
        public const string IsEnabled = "IsEnabled";
        /// <summary>
        /// 排序
        /// </summary>
        public const string Idx = "Idx";
    }
    /// <summary>
    ///  桌面配置
    /// </summary>
    public class DesktopPanel : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 图标
        /// </summary>
        public string PanelIcon
        {
            get
            {
                return this.GetValStrByKey(DesktopPanelAttr.PanelIcon);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.PanelIcon, value);
            }
        }
        /// <summary>
        /// 更多地址
        /// </summary>
        public string ExtUrl
        {
            get
            {
                return this.GetValStrByKey(DesktopPanelAttr.ExtUrl);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.ExtUrl, value);
            }
        }
        /// <summary>
        /// 信息块类型
        /// </summary>
        public PanelType PanelType
        {
            get
            {
                return (PanelType)this.GetValIntByKey(DesktopPanelAttr.PanelType);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.PanelType, (int)value);
            }
        }
        /// <summary>
        /// 表名
        /// </summary>
        public string PTable
        {
            get
            {
                return this.GetValStrByKey(DesktopPanelAttr.PTable);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.PTable, value);
            }
        }
        /// <summary>
        /// 标题列名
        /// </summary>
        public string TitleColumn
        {
            get
            {
                return this.GetValStrByKey(DesktopPanelAttr.TitleColumn);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.TitleColumn, value);
            }
        }
        /// <summary>
        /// 更新时间
        /// </summary>
        public string EDT
        {
            get
            {
                return this.GetValStrByKey(DesktopPanelAttr.EDT);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.EDT, value);
            }
        }
        /// <summary>
        /// 图片内容URL
        /// </summary>
        public string ImgUrl
        {
            get
            {
                return this.GetValStrByKey(DesktopPanelAttr.ImgUrl);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.ImgUrl, value);
            }
        }
        /// <summary>
        /// 排序字段
        /// </summary>
        public string OrderColumn
        {
            get
            {
                return this.GetValStrByKey(DesktopPanelAttr.OrderColumn);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.OrderColumn, value);
            }
        }
        /// <summary>
        /// 显示行数
        /// </summary>
        public int AllowRows
        {
            get
            {
                return this.GetValIntByKey(DesktopPanelAttr.AllowRows);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.AllowRows, value);
            }
        }
        /// <summary>
        /// 外部主键
        /// </summary>
        public string FK_MyPK
        {
            get
            {
                return this.GetValStrByKey(DesktopPanelAttr.FK_MyPK);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.FK_MyPK, value);
            }
        }
        /// <summary>
        /// 查询字段
        /// </summary>
        public string ColumnName
        {
            get
            {
                return this.GetValStrByKey(DesktopPanelAttr.ColumnName);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.ColumnName, value);
            }
        }
        /// <summary>
        /// 查询字段中文名
        /// </summary>
        public string ColumnDesc
        {
            get
            {
                return this.GetValStrByKey(DesktopPanelAttr.ColumnDesc);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.ColumnDesc, value);
            }
        }
        /// <summary>
        /// 外部链接字段
        /// </summary>
        public string OutHref
        {
            get
            {
                return this.GetValStrByKey(DesktopPanelAttr.OutHref);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.OutHref, value);
            }
        }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return this.GetValBooleanByKey(DesktopPanelAttr.IsEnabled);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.IsEnabled, value);
            }
        }
        /// <summary>
        /// 加载顺序
        /// </summary>
        public int Idx
        {
            get
            {
                return this.GetValIntByKey(DesktopPanelAttr.Idx);
            }
            set
            {
                this.SetValByKey(DesktopPanelAttr.Idx, value);
            }
        }
        #endregion 属性

        #region 构造方法
        /// <summary>
        /// 桌面配置
        /// </summary>
        public DesktopPanel()
        {
        }

        public DesktopPanel(string no)
        {
            this.No = no;
            this.Retrieve();
        }

        #endregion

        /// <summary>
        /// 桌面配置Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map();
                map.PhysicsTable = "OA_DesktopPanel"; //物理表。
                map.EnDesc = "信息块属性";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.CodeStruct = "2";

                map.AddTBStringPK(DesktopPanelAttr.No, null, "编号", true, true, 10, 10, 10);
                map.AddTBString(DesktopPanelAttr.Name, null, "信息块标题", true, false, 0, 200, 300, false);
                map.AddDDLSysEnum(DesktopPanelAttr.PanelType, 0, "信息块类型", true, true, DesktopPanelAttr.PanelType, "@0=图片，轮换图@1=外部链接@2=列表@3=表格式@4=图表");
                map.AddTBString(DesktopPanelAttr.PanelIcon, null, "图标路径", true, false, 0, 200, 300, true);
                map.AddTBString(DesktopPanelAttr.PTable, null, "表名", true, false, 0, 500, 300, false);
                map.AddTBString(DesktopPanelAttr.FK_MyPK, null, "主键", true, false, 0, 500, 300, false);
                map.AddTBString(DesktopPanelAttr.TitleColumn, null, "标题列名", true, false, 0, 500, 300, false);
                map.AddTBString(DesktopPanelAttr.OrderColumn, null, "排序字段", true, false, 0, 500, 300, false);
                map.AddTBString(DesktopPanelAttr.AllowRows, "0", "显示行数", true, false, 0, 500, 300, false);
                //列表
                map.AddTBString(DesktopPanelAttr.ColumnName, null, "查询字段", true, false, 0, 1000, 400, true);
                map.AddTBString(DesktopPanelAttr.ColumnDesc, null, "查询字段中文名", true, false, 0, 1000, 400, true);

                map.AddTBString(DesktopPanelAttr.ExtUrl, null, "更多地址", true, false, 0, 500, 300, true);
                
                //图片，轮换图
                map.AddTBString(DesktopPanelAttr.EDT, null, "更新时间字段", true, false, 0, 500, 300, false);
                map.AddTBString(DesktopPanelAttr.ImgUrl, null, "图片内容URL字段", true, false, 0, 500, 300, true);
                //外部链接
                map.AddTBString(DesktopPanelAttr.OutHref, null, "外部链接字段", true, false, 0, 1000, 400, true);

                map.AddBoolean(DesktopPanelAttr.IsEnabled, true, "是否启用", true, true);
                map.AddTBInt(DesktopPanelAttr.Idx, 0, "加载顺序", true, false);
                this._enMap = map;
                return this._enMap;
            }
        }
    }
    /// <summary>
    /// 桌面配置
    /// </summary>
    public class DesktopPanels : EntitiesOID
    {        
        /// <summary>
        /// 桌面配置s
        /// </summary>
        public DesktopPanels() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DesktopPanel();
            }
        }
    }
}
