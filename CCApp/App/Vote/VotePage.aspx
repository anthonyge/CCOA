﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VotePage.aspx.cs" Inherits="CCOA.App.Vote.VotePage"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Ctrl/kindeditor/kindeditor-min.js" type="text/javascript"></script>
    <script src="../../Ctrl/kindeditor/lang/zh_CN.js" type="text/javascript"></script>
    <style type="text/css">
        .div1
        {
            border-style: solid;
            border-color: Gray;
            border-width: 1px;
            width: 100%;
        }
        .div2
        {
            background-color: Red;
            width: 30%;
        }
        .article_intro
        {
            padding: 5px 5px 5px 5px;
            background-color: #eeeeee;
            color: #444444;
        }
        .ari
        {
            font-weight: bold;
        }
        .arl
        {
        }
        div.article
        {
            padding-left: 10px;
            min-height: 300px;
        }
        p.article p
        {
            min-height: 0px;
        }
        img
        {
            border: 0px;
        }
    </style>
    <script type="text/javascript">
        var editor;
        KindEditor.ready(function (K) {
            editor = K.create('#Comment', {
                resizeType: 1,
                allowPreviewEmoticons: false,
                allowImageUpload: false,
                items: [
						'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
						'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
						'insertunorderedlist', '|', 'emoticons', 'image', 'link']
            });
        });
        function check() {
            if ($("input:[name='VoteItem']:checked").length == 0) {
                alert("请选择投票项目！");
            }
            else {
                editor.sync();
                $("#form1").submit();
                var arryName = document.getElementsByName("FK_VoteOID");
                this.location = "VoteResult.aspx?id=" + arryName[0].value;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" action="VotePage.aspx" target="iframe1">
    <div style="font-size: 11px; position: absolute; width: 50px; height: 50px; top: 2px;
        left: 2px;">
        <a href="VoteList.aspx">
            <img style="width: 40px; height: 40px;" src="../../Images/back.png" alt="返回投票列表" /></a>
    </div>
    <p style="font-size: 25px; font-weight: bolder; padding-left: 10px; text-align: center;">
        <asp:Label runat="server" ID="lblTitle"></asp:Label></p>
    <hr />
    <p class="article_intro">
        <span class="ari">投票起止日期：从<asp:Label runat="server" ID="lblFromTime" CssClass="arl"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <span class="ari">到&nbsp;<asp:Label runat="server" ID="lblToTime" CssClass="arl"></asp:Label></span></p>
    <table class="table">
        <tr>
            <th style="width: 40px;">
                序号
            </th>
            <th style="text-align: left;">
                选项
            </th>
            <th style="text-align: left;">
                选择
            </th>
        </tr>
        <%
            if (en != null)
            {
                int i = 0;
                string[] strs = en.Item.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var str in strs)
                {
                    i++;
                    string strType = "checkbox";
                    if (en.VoteType == "单投")
                    {
                        strType = "radio";
                    }
                    string td1 = "<td>" + i.ToString() + "</td>\r\n";
                    string td2 = "<td  style='text-align:left;'>" + str + "</td>\r\n";
                    string td3 = "<td  style='text-align:left;'><input type='" + strType + "' name='VoteItem' value='" + i.ToString() + "'/></td>\r\n";
                    Response.Write("<tr>" + td1 + td2 + td3 + "</tr>\r\n");
                }
            }
        %>
    </table>
    <div id="divcom" style="width: 100%; margin: 10px 0px 10px 0px;">
        <p class="article_intro">
        <span class="ari">添加评论</span>
        </p>
        <textarea name="Comment" id="Comment" style="width: 100%"></textarea>
    </div>
    <div style="margin: 10px 0px 10px 0px;">
        <%if (en == null) return; %>
        <% if (en.IsAnonymous == "1")
           {
               Response.Write("<input type='checkbox' value='1' name='IsAnony' />匿名投票&nbsp&nbsp&nbsp;");
           }
           if (en.IsEnable == "1")
           {
               if (!string.IsNullOrEmpty(en.DateFrom) && DateTime.Now < Convert.ToDateTime(en.DateFrom))
               {
                   Response.Write("&nbsp&nbsp&nbsp投票尚未开始！");
               }
               else if (!string.IsNullOrEmpty(en.DateTo) && DateTime.Now > Convert.ToDateTime(en.DateTo))
               {
                   Response.Write("&nbsp&nbsp&nbsp投票已经结束！");
               }
               else
               {
                   //权限判断
                   if (!string.IsNullOrEmpty(en.VoteEmpNo) && !en.VoteEmpNo.ToLower().Contains(BP.Web.WebUser.No.ToLower()))
                   {
                       Response.Write("抱歉，你没有权限投票！");
                   }
                   else if (this.IsHasVoted() == true)
                   {
                       Response.Write("对不起，您已经投过票了！");
                   }
                   else
                   {
                       Response.Write("<input type='button' onclick='check()' value='投票'/>");
                   }
               }
           }
           else
           {
               Response.Write("投票已经停止！~");
           }
        %>
    </div>
    <input type="hidden" value='<%=Request.QueryString["id"]%>' name="FK_VoteOID" />
    <input type="hidden" name="FK_Emp" value="<%=UserNo%>" />
    </form>
    <iframe id="iframe1" name="iframe1" style="display: none" />
</body>
</html>
