﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;

namespace BP.OA.Meeting
{

    public class MeetingTypeAttr : EntityNoNameAttr
    {
        public const string Note = "Note";
    }
    public class MeetingType : EntityNoName
    {
        public string Note
        {
            get
            {
                return this.GetValStrByKey(MeetingTypeAttr.Note);
            }
            set
            {
                this.SetValByKey(MeetingTypeAttr.Note, value);
            }
        }
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_MeetingType");
                map.EnDesc = "会议类型";
                map.IsAllowRepeatName = false;
                map.CodeStruct = "3";
                map.AddTBStringPK(MeetingTypeAttr.No, string.Empty, "编号", true, true, 3, 3, 3);
                map.AddTBString(MeetingTypeAttr.Name, string.Empty, "名称", true, false, 1, 100, 30, false);
                map.AddTBString(MeetingTypeAttr.Note, string.Empty, "说明", true, false, 0, 200, 300);
                this._enMap = map;
                return this._enMap;
            }
        }
    }
    public class MeetingTypes : EntitiesNoName
    {
        /// </summary>
        public MeetingTypes() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new MeetingType();
            }
        }
    }
}
