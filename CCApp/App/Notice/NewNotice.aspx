﻿<%@ Page Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true" CodeBehind="NewNotice.aspx.cs"
    ValidateRequest="false" ClientIDMode="Static" Inherits="CCOA.App.NOTICE.NewNotice" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cph_head">
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        .doc-table
        {
            border-collapse: collapse;
            border-spacing: 0;
            margin-left: 9px;
            display: table;
            border-color: gray;
            width:99%;
        }
        .doc-table th
        {
            background: #EEE;
        }
        .doc-table th, .doc-table td
        {
            border: 1px solid #BDDBEF;
            padding: 3px 3px;
        }
        td.title
        {
            width: 100px;
            text-align: right;
        }
        td.text
        {
            text-align:left;
            padding-left:10px;
        }
    </style>
    <script src="../../js/js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <script src="../../js/js_EasyUI/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../js/js_EasyUI/plugins/jquery.datebox.js" type="text/javascript"></script>
    <script src="../../js/zDialog/zDialog.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function ($) {
            //更多链接
            $("#href_More").click(
            function () {
                if ($("#table_More").css("display") == "none") {
                    $("#table_More").fadeIn(10);
                    $(this).html("更多.↑.");
                }
                else {
                    $("#table_More").fadeOut(10);
                    $(this).html("更多.↓.");
                }
            });
            //自动状态功能
            var rbl_NoticeSta = "span#ui_NoticeSta input";
            $(rbl_NoticeSta).click(
            function () {
                if ($(this).val() == "0")
                    $("#div_AutoNoticeSta").fadeIn(10);
                else
                    $("#div_AutoNoticeSta").fadeOut(10);
            });
            //反馈状态功能
            $("#ui_GetAdvices").click(
            function () {
                if ($("#div_Advice").css("display") == "none")
                    $("#div_Advice").fadeIn(10);
                else
                    $("#div_Advice").fadeOut(10);
            });
            //个人授权功能
            $("#cb_Private").click(
            function () {
                if ($("#div_Private").css("display") == "none") {
                    $("#div_Private").fadeIn(10);
                    $("#span_Private_dept").fadeIn(10);
                    $("#span_Private_priv").fadeOut(10);
                }
                else {
                    $("#div_Private").fadeOut(10);
                    $("#span_Private_dept").fadeOut(10);
                    $("#span_Private_priv").fadeIn(10);
                }
            });
            //发布对象
            $("#cb_Users").click(
            function () {
                if ($("#cb_Users").attr("checked") != "checked") {
                    $("#div_Users").fadeIn(10);
                    $("#span_Users_part").fadeIn(10);
                    $("#span_Users_all").fadeOut(10);
                }
                else {
                    $("#div_Users").fadeOut(10);
                    $("#span_Users_part").fadeOut(10);
                    $("#span_Users_all").fadeIn(10);
                }
            });
        }); 
    </script>

    <link rel="stylesheet" href="../../ctrl/kindeditor/themes/default/default.css" />
    <link rel="stylesheet" href="../../ctrl/kindeditor/plugins/code/prettify.css" />
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/kindeditor.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/lang/zh_CN.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/plugins/code/prettify.js"></script>
    <script type="text/javascript">
        KindEditor.ready(function (K) {
            var editor1 = K.create('#ui_Doc', {
                cssPath: '../../ctrl/kindeditor/plugins/code/prettify.css',
                uploadJson: '../../ctrl/kindeditor/asp.net/upload_json.ashx',
                fileManagerJson: '../../ctrl/kindeditor/asp.net/file_manager_json.ashx',
                allowFileManager: true
            });
            editor1.sync();
            prettyPrint();
        });
    </script>

    <script src="../../Js/jquery.MultiFile.js" charset="gb2312" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cph_title">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cph_body">
    <div>
    <div style="margin:30px 100px; width:820px;">
      <%--  <div style=" color: Red; padding: 3px 3px 3px 3px; margin-bottom:10px;">
            (注：)公告是组织内部非常正式的行政管理方式，请没有权限的人或没有授权的人勿使用此功能，否则一切后果自负。
        </div>--%>
        <table class="doc-table">
            <caption style="text-align: left; font-size: 14px; font-weight: bold;">
                <img src="img/Notice.png" style="width:13px;height:13px;" />公告基本信息</caption>
            <tr>
                <td class="title">
                    <span style="color:Red;">*</span>&nbsp;公告类型:
                </td>
                <td class="text">
                    &nbsp;<asp:RadioButtonList 
                        ID="ui_FK_NoticeCategory" runat="server" RepeatLayout="Flow"
                        RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <span style="color:Red;">*</span>&nbsp;标题:
                </td>
                <td class="text">
                    &nbsp;<asp:TextBox ID="ui_Title" runat="server" 
                        Width="90%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <span style="color:Red;">*</span>&nbsp;内容:
                </td>
                <td class="text">
                    &nbsp;<asp:TextBox ID="ui_Doc" runat="server" 
                        TextMode="MultiLine" Style="width: 97%;
                        height: 200px;"></asp:TextBox>
                </td>
            </tr>
        </table>
        <div style="text-align: right; padding-right: 30px; margin-top:5px;">
            <a href="#" id="href_More" style="color:#333; text-decoration:none;">更多.↓.</a></div>
        <table class="doc-table" id="table_More" style="display:none;">
            <caption style="text-align: left; font-size: 14px; font-weight: bold;">
                更多设置</caption>
            <tr>
                <td class="title">
                    &nbsp;重要程度:
                </td>
                <td class="text">
                    &nbsp;<asp:RadioButtonList ID="ui_Importance" runat="server" RepeatLayout="Flow"
                        RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;发布授权:
                </td>
                <td class="text">
                    <asp:CheckBox ID="cb_Private" runat="server" Checked="true" />
                    <span id="span_Private_priv">以<b>个人名义</b>/部门名义发布</span>
                    <span id="span_Private_dept" style=" display:none;">以个人名义/<b>部门名义</b>发布</span>
                    <div id="div_Private" style="display: none; margin-top: 5px; padding-top: 5px; border-top: dashed 1px #aaa;">
                    请选择部门：<asp:DropDownList runat="server" ID="ui_FK_Dept" Width="200"></asp:DropDownList>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;接收对象:
                </td>
                <td class="text">
                    <asp:CheckBox ID="cb_Users" runat="server" Checked="true" />
                    <span id="span_Users_all">发给<b>全体人员</b>/部分人员</span>
                    <span id="span_Users_part" style=" display:none;">发给全体人员/<b>部分人员<br/>（填写为以下三个条件的交集）</b></span>
                    <div id="div_Users" style="display: none; margin-top: 5px; padding-top: 5px; border-top: dashed 1px #aaa;">
                        <input type="button" value="添加人员" onclick="zDialog_open2('../../ctrl/SelectUsers/SelectUser_Jq.aspx', '选择人员', 650, 430,'#txt_UserNames','txt_UserNames','txt_Users');" />（不选择表示全部人员）<br/>
                        <asp:TextBox ID="txt_Users" runat="server" TextMode="MultiLine" Width="85%" Rows="2" ReadOnly="true"></asp:TextBox>
                        <asp:HiddenField ID="txt_UserNames" runat="server"></asp:HiddenField>
                        <br/><input type="button" value="添加职位" onclick="zDialog_open2('../../ctrl/SelectStations/SelectStation.aspx', '选择职位', 500, 400,'#txt_StationNames','txt_StationNames','txt_Stations');" />（不选择表示所有职位）<br/>
                        <asp:TextBox ID="txt_Stations" runat="server" TextMode="MultiLine" Width="85%" Rows="2" ReadOnly="true"></asp:TextBox>
                        <asp:HiddenField ID="txt_StationNames" runat="server"></asp:HiddenField>
                        <br/><input type="button" value="添加部门" onclick="zDialog_open2('../../ctrl/SelectDepts/SelectDept_zTree1.aspx', '选择部门', 500, 400,'#txt_DeptNames','txt_DeptNames','txt_Depts');" />（不选择表示所有部门）<br/>
                        <asp:TextBox ID="txt_Depts" runat="server" TextMode="MultiLine" Width="85%" Rows="2" ReadOnly="true"></asp:TextBox>
                        <asp:HiddenField ID="txt_DeptNames" runat="server"></asp:HiddenField>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;目前状态:
                </td>
                <td class="text">
                    &nbsp;<asp:RadioButtonList ID="ui_NoticeSta" runat="server" RepeatLayout="Flow"
                        RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                    <div id="div_AutoNoticeSta" style="display: none; margin-top:5px; padding-top:5px; border-top:dashed 1px #aaa;">
                        开始日期：<asp:TextBox ID="ui_StartTime" runat="server" CssClass="easyui-datebox"></asp:TextBox>
                        结束日期：<asp:TextBox ID="ui_StopTime" runat="server" CssClass="easyui-datebox"></asp:TextBox>
                    </div>
                </td>
            </tr>
            <tr style="display:none;">
                <td class="title">
                    &nbsp;反馈设置:
                </td>
                <td class="text">
                    <asp:CheckBox ID="ui_GetAdvices" runat="server" Text="是否反馈" />
                    <div id="div_Advice" style="display: none; margin-top:5px; padding-top:5px; border-top:dashed 1px #aaa;">
                    反馈信息说明：
                    <br/><asp:TextBox ID="ui_AdviceDesc" runat="server" TextMode="MultiLine" Width="95%" Rows="5"></asp:TextBox>
                    <br/>反馈选项列表：(每行一个)
                    <br/><asp:TextBox ID="ui_AdviceItems" runat="server" TextMode="MultiLine" Width="95%" Rows="5"></asp:TextBox>
                    </div>
                </td>
            </tr>
            <tr style="">
                <td class="title">
                    &nbsp;附件:
                </td>
                <td class="text">
                    <asp:FileUpload ID="FileUpload1" runat="server" Width="300" CssClass="multi" />
                </td>
            </tr>
        </table>
        <div style="text-align:center; margin-top:5px;">
             <asp:Button runat="server" Text="添加公告" ID="btnSubmit" 
                 onclick="btnSubmit_Click" />
        </div>
    </div>
    </div>
</asp:Content>