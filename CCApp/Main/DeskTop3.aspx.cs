﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using BP.OA.Desktop;

namespace CCOA.Main
{    
    public partial class DeskTop3 : System.Web.UI.Page
    {
        public String jsonStringData = String.Empty;

        public String treejsonStr = string.Empty;
        
        protected void Page_Load(object sender, EventArgs e)
        {            
            String sqlStr = "select * from OA_DesktopPanel where IsEnabled='1' order by Idx asc";
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sqlStr);
            
            IList<JsonModelLoad> appL = new List<JsonModelLoad>();
            IList<JsonModelLoad> appM = new List<JsonModelLoad>();
            IList<JsonModelLoad> appR = new List<JsonModelLoad>();
            for(int i = 0 ; i< dt.Rows.Count; i++)
            {
                JsonModelLoad model = new JsonModelLoad();
                model.id = dt.Rows[i]["No"].ToString();
                model.cnName = dt.Rows[i]["Name"].ToString();
                model.IsEnabled = dt.Rows[i]["IsEnabled"].ToString();
                model.Idx = Convert.ToInt32(dt.Rows[i]["Idx"].ToString());
                model.PanelType = dt.Rows[i]["PanelType"].ToString();
                if (model.PanelType == "0")
                {  //轮播图单独模板
                    model.url = "DeskTopLoadData2.aspx?No=" + dt.Rows[i]["No"].ToString();
                }
                else {
                    model.url = "DeskTopLoadData.aspx?No=" + dt.Rows[i]["No"].ToString();
                }
                
                if(Convert.ToInt32(dt.Rows[i]["Idx"].ToString())%3==1)                
                {
                    appL.Add(model);
                }else if(Convert.ToInt32(dt.Rows[i]["Idx"].ToString())%3==2)                
                {
                    appM.Add(model);
                }else{
                    appR.Add(model);                    
                }                
            }
            JsonModelDivLoad modelLoad = new JsonModelDivLoad();
            modelLoad.appL = appL;
            modelLoad.appM = appM;
            modelLoad.appR = appR;
            jsonStringData = JsonConvert.GetJsonString(modelLoad,false);
            treejsonStr = getAllDeskTopModular();
        }

        /// <summary>
        /// 获取
        /// </summary>
        protected string getAllDeskTopModular() {
            BP.OA.Desktop.DesktopPanels panels = new BP.OA.Desktop.DesktopPanels();
            panels.RetrieveAll();   
            foreach(BP.OA.Desktop.DesktopPanel model in panels)
            {
                String isEnalbledStr = "";
                if (model.IsEnabled==true)
                {
                    isEnalbledStr = "checked='true'";
                }
                treejsonStr += "<tr><td>" + model.Name + "</td><td><input type='checkbox' id='deskTopModular' value='" + model.No + "' " + isEnalbledStr + " /></td></tr>";                         
            }
            return treejsonStr;
        }

        public class JsonModelDivLoad {
            public IList<JsonModelLoad> appL;
            public IList<JsonModelLoad> appM;
            public IList<JsonModelLoad> appR;
        }

        public class JsonModelLoad
        {
            public string id;  //编号
            public string cnName; //
            public string enName;
            public string avatar;
            public string url;
            public int Idx;
            public string IsEnabled;
            public string PanelType;
        }
                
       
                       
    }
}