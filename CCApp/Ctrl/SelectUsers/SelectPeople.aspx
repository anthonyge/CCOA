﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectPeople.aspx.cs" Inherits="wwwroot.App_Ctrl.SelectPeople" ClientIDMode="Static" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body{font-size:12px;}
        table.t2
        {   
            display:table;
            border-collapse:collapse;
            border: 1px solid #cad9ea;
            color: #666;
            width: 100%;
        }
        table.t2 td
        {
            border: 1px solid #cad9ea;
        }
        select.listbox
        {
            border-style:none;
            }
        table.t1
        {
            display:table;
            border-collapse:collapse;
         }
    </style>
    <script src="/Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="/Js/QueryString.js" type="text/javascript"></script>
    <script type="text/javascript">
        var rOut = GetQueryString("out");
        var rIn = GetQueryString("in");
        $(function () {
            $('#btnSubmit').click(function () {
                var length = $('#lbRight option').length;
                var value = new Array();
                var text = new Array();
                var r = null;
                if (length > 0) {
                    $('#lbRight option').each(function (i, selected) {
                        value.push($(selected).val());
                        text.push($(selected).text());
                    });
                }
                var rOut_arr = rOut.split(',');
                window.parent.document.getElementById(rOut_arr[0]).value = value;
                window.parent.document.getElementById(rOut_arr[1]).value = text;
                window.parent.zDialog_close();
            });
            $('#btnRight').click(function () {
                var model = GetQueryString("SelectModel");
                if (model == "Single") {
                    var count = $("#lbLeft option:selected").length;
                    var length = $('#lbRight option').length;
                    if (count > 1) {
                        alert("当前模式下只能单选，不能多选！");
                        return false;
                    }
                    if (length == 1) {
                        alert("您已经选择了一人，不能多选！");
                        return false;
                    }
                }
            });
            $('#btnClose').click(function () {
                //window.parent.document.getElementById("dialogCase").style.display = 'none';
                var rOut_arr = rOut.split(',');
                //window.parent.document.getElementById(rOut_arr[0]).value = "";
                //window.parent.document.getElementById(rOut_arr[1]).value = "";
                window.parent.zDialog_close();
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin-left: 10px; text-align: left; margin-top: 5px;">
        <div id="panLogin">
            <p>
                <strong>您当前选择了 <span id="number" style="color: #ff0000; font-weight: bold;"></span><span
                    id="total" style="color: blue; font-weight: bold;">
                    <asp:Literal runat="server" ID="liSelCount"></asp:Literal></span> 个人员!</strong> &nbsp;&nbsp; <span><asp:LinkButton runat="server" ID="lbClearSelAll" OnClick="ClearSelAll">清除已选</asp:LinkButton>&nbsp;&nbsp;&nbsp;</span>&nbsp;</p>
        </div>
        <table width="450px" border="0" class="t1">
            <tr>
                <td colspan="3">
                    <asp:DropDownList ID="ddlDept" runat="server" Height="20px" Width="90px">
                    </asp:DropDownList>
                    <asp:CheckBox ID="cbContainChild" runat="server" Text="搜索子部门" />&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlStation" runat="server" Height="19px" Width="90px">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                    <asp:TextBox ID="txtKeyword" runat="server" MaxLength="10" Width="90px"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" Text="搜索" OnClick="btnSearch_Click" />
                </td>
            </tr>
        </table>
        <div style="width: 450px; height: 300px; border: 2 inset; scrollbar-face-color: #DBEBFE;
            scrollbar-shadow-color: #B8D6FA; scrollbar-highlight-color: #FFFFFF; scrollbar-3dlight-color: #DBEBFE;
            scrollbar-darkshadow-color: #458CE4; scrollbar-track-color: #FFFFFF; scrollbar-arrow-color: #458CE4;
            margin-top: 5px;">
            <table width="410px" id="mytab" border="1" class="t2">
                <tr>
                    <td align="right" width="40%">
                        <asp:ListBox ID="lbLeft" runat="server" Height="265px" Width="95%" ToolTip="按Ctrl键全选"
                            CssClass="listbox" SelectionMode="Multiple"></asp:ListBox>
                    </td>
                    <td width="15%" align="center">
                        <asp:Button ID="btnRight" runat="server" Text=" → " CssClass="SmallButtonA" OnClick="btnRight_Click" />
                        <br />
                        <asp:Button ID="btnLeft" runat="server" Text=" ← " CssClass="SmallButtonA" OnClick="btnLeft_Click" />
                    </td>
                    <td width="40%">
                        <asp:ListBox ID="lbRight" runat="server" Height="265px" Width="95%" CssClass="listbox"
                            SelectionMode="Multiple"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <input type="button" id="btnSubmit" value="确定" class="SmallButtonA" />&nbsp;&nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="SmallButtonA" OnClick="ClearSelAll"
                            Text="清除" />&nbsp;&nbsp;
                        <input type="button" id="btnClose" value="取消" class="SmallButtonA" />
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
