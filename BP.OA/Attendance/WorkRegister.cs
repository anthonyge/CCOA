﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using System.Data;

namespace BP.OA.Attendance
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class WorkRegisterAttr : EntityOIDAttr
    {
        public const String Sequence = "Sequence";
        public const String RegisterType = "RegisterType";
        public const String CanRegisterTime = "CanRegisterTime";
        public const String AheadRegisterTime = "AheadRegisterTime";
        public const String BehindRegisterTime = "BehindRegisterTime";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class WorkRegister : EntityOID
    {
        #region 属性
        public string Sequence
        {
            get { return this.GetValStrByKey(WorkRegisterAttr.Sequence); }
            set { this.SetValByKey(WorkRegisterAttr.Sequence, value); }
        }
        public string RegisterType
        {
            get { return this.GetValStrByKey(WorkRegisterAttr.RegisterType); }
            set { this.SetValByKey(WorkRegisterAttr.RegisterType, value); }
        }
        public DateTime CanRegisterTime
        {
            get { return this.GetValDateTime(WorkRegisterAttr.CanRegisterTime); }
            set { this.SetValByKey(WorkRegisterAttr.CanRegisterTime, value); }
        }
        public String AheadRegisterTime
        {
            get { return this.GetValStrByKey(WorkRegisterAttr.AheadRegisterTime); }
            set { this.SetValByKey(WorkRegisterAttr.AheadRegisterTime, value); }
        }
        public String BehindRegisterTime
        {
            get { return this.GetValStrByKey(WorkRegisterAttr.BehindRegisterTime); }
            set { this.SetValByKey(WorkRegisterAttr.BehindRegisterTime, value); }
        }
        #endregion

        #region 重写方法
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "WorkRegister";
                map.EnDesc = "考勤表";                   // 实体的描述.
                map.CodeStruct = "2";
                map.AddTBIntPKOID();
                map.AddTBString(WorkRegisterAttr.Sequence, null, "登记次序", true, false, 2, 50, 50);
                map.AddTBString(WorkRegisterAttr.RegisterType, null, "登记类型", true, false, 2, 50, 50);
                map.AddTBDateTime(WorkRegisterAttr.CanRegisterTime, null, "规定时间", true, false);
                map.AddTBDateTime(WorkRegisterAttr.AheadRegisterTime, null, "提前登记时间", true, false);
                map.AddTBDateTime(WorkRegisterAttr.BehindRegisterTime, null, "超时登记时间", true, false);
                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        #region 自主开发
        public static DataTable GetWorkRegister()
        {
            string strSql = "select * from OA_WorkRegister";
            return BP.DA.DBAccess.RunSQLReturnTable(strSql);
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class WorkRegisters : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new WorkRegister();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public WorkRegisters()
        {
        }
    }
}
