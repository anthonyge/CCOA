﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.OA.Meeting;

namespace CCOA.App.Meeting
{
    public partial class SelectOrding : System.Web.UI.Page
    {
        #region 属性.
        public string CurrDate
        {
            get
            {
                string s = this.Request.QueryString["CurrDate"];
                if (string.IsNullOrEmpty(s))
                    return BP.DA.DataType.CurrentData;
                return s;
            }
        }
        private RoomResources resource = null;
        private RoomResources MTResource
        {
            get
            {
                if (resource == null)
                {
                    resource = new RoomResources();
                    resource.RetrieveAll();
                }
                return resource;
            }
        }
        #endregion 属性

        protected void Page_Load(object sender, EventArgs e)
        {
            BP.OA.Meeting.RoomOrdings rooms = new BP.OA.Meeting.RoomOrdings();
            rooms.RetrieveAll();
            Repeater1.DataSource = rooms;
            Repeater1.DataBind();

        }
        public string GetResource(object obj)
        {
            string result = string.Empty;
            if (obj != null)
            {
                string[] strs = obj.ToString().TrimEnd(',').Split(',');
                foreach (RoomResource res in MTResource)
                {
                    if (strs.Contains(res.No, new MyStringComparer()))
                    {
                        result += res.Name+",";
                    }
                }
                result = result.TrimEnd(',');
            }
            return result;
        }
    }
}