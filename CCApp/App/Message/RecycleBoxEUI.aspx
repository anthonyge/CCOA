﻿<%@ Page Title="" Language="C#" MasterPageFile="../../App/AppMaster/AppSite.Master" AutoEventWireup="true"
    CodeBehind="RecycleBoxEUI.aspx.cs" Inherits="CCOA.App.Message.RecycleBoxEUI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        var GetOid;
        var BackData=false;
        //加载grid后回调函数
        function LoadDataGridCallBack(js, scorp) {
            $("#pageloading").hide();
            if (js == "") js = "[]";

            //系统错误
            if (js.status && js.status == 500) {
                $("body").html("<b>访问页面出错，请联系管理员。<b>");
                return;
            }

            var pushData = eval('(' + js + ')');
            $('#newsGrid').datagrid({
                columns: [[
                 { checkbox: true }, 
                 { field: 'AttachFile', title: ' <img style="height: 12px;" src="img/Attach.png" />', width: 10, align: 'center', formatter: function (value, row, index) {
                     var imgUrl = "";
                     if (row.AttachFile) {
                         imgUrl = "Img/Attach.png";
                         return "<img  style='height: 12px;'  src='" + imgUrl + "'/>"
                     }
                     return ""
                 }
                 },
                     { field: 'Title', title: '主题', width: 200, align: 'left'},
                    { field: 'FromType', title: '邮件类别', width: 40, align: 'center', formatter: function (value, row, index) {
                        if (row.FromType == 0) {
                            return "草稿箱"
                        }
                        else {
                            if (row.FromType == 1) {
                                return "发件箱"
                            }
                            else {
                                return "收件箱";
                            }
                        }
                    }
                    },
                    { field: 'Name', title: '删除人', width: 80, align: 'center' },
                    { field: 'RPT', title: '创建时间', width: 80, align: 'center' }
                ]],
                idField: 'No',
                selectOnCheck: false,
                checkOnSelect: true,
                singleSelect: true,
                data: pushData,
                width: 'auto',
                height: 'auto',
                striped: true,
                rownumbers: true,
                pagination: true,
                remoteSort: false,
                fitColumns: true,
                pageNumber: scorp.pageNumber,
                pageSize: scorp.pageSize,
                pageList: [20, 30, 40,50],
                onDblClickCell: function (index, field, value) {
                },
                onDblClickRow: function (rowIndex, rowData) {
                alert("恢复后方可查看!");
                },
                loadMsg: '数据加载中......'
            });
            //分页
            var pg = $("#newsGrid").datagrid("getPager");
            if (pg) {
                $(pg).pagination({
                    onRefresh: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    },
                    onSelectPage: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    }
                });
            }
        }

        //加载grid
        function LoadGridData(pageNumber, pageSize) {
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            var keyWords = document.getElementById("TB_KeyWords").value;

            var params = {
                method: "RecycleBoxGetNewList",
                keyWords: encodeURI(keyWords),
                impOID: GetOid,
                pageNumber: pageNumber,
                pageSize: pageSize,
                BoolBackData: BackData
            };
            queryData(params, LoadDataGridCallBack, this);
        }

        function RefreshGrid() {
            var grid = $('#newsGrid');
            var options = grid.datagrid('getPager').data("pagination").options;
            var curPage = options.pageNumber;
            var pageSize = options.pageSize;
            LoadGridData(curPage, pageSize);
        }
        //初始化
        $(function () {
            LoadGridData(1, 20);
        });

        //公共方法
        function queryData(param, callback, scope, method, showErrMsg) {
            if (!method) method = 'GET';
            $.ajax({
                type: method, //使用GET或POST方法访问后台
                dataType: "text", //返回json格式的数据
                contentType: "application/json; charset=utf-8",
                url: "EmailEUI.ashx", //要访问的后台地址
                data: param, //要发送的数据
                async: false,
                cache: false,
                complete: function () { }, //AJAX请求完成时隐藏loading提示
                error: function (XMLHttpRequest, errorThrown) {
                    callback(XMLHttpRequest);
                },
                success: function (msg) {//msg为返回的数据，在这里做数据绑定
                    var data = msg;
                    callback(data, scope);
                }
            });
        }
        //恢复
        function BackDataM() {
            var rows = $('#newsGrid').datagrid('getChecked');
            if (rows.length == 0) {
                alert("您没有选中项!");
            }
            else {
                if (confirm("确定恢复所选项?")) {
                    var ids = [];
                    for (var i = 0; i < rows.length; i++) {
                        ids.push(rows[i].No);
                    }
                    GetOid = ids.join(',');
                    BackData = true;
                    //调用
                    RefreshGrid();
                }
            }
        }
        function RecycleBoxgetSelections() {
            var rows = $('#newsGrid').datagrid('getChecked');
            if (rows.length == 0) {
                alert("您没有选中项!");
            }
            else {
                if (confirm("确定删除所选项?")) {
                    var ids = [];
                    for (var i = 0; i < rows.length; i++) {
                        ids.push(rows[i].No);
                    }
                    GetOid = ids.join(',');
                    //调用
                    RefreshGrid();
                }
            }
        }
        function RecycleBoxDelAll() {
            if (confirm("确定清空垃圾箱?")) {
                var ids = [];
                var getAll = $("#newsGrid").datagrid("getRows");
                for (var i = 0; i < getAll.length; i++) {
                    ids.push(getAll[i]['No']);
                }
                GetOid = ids.join(',');
                //调用
                RefreshGrid();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div data-options="region:'center'" border="false" style="margin: 0; padding: 0;
        overflow: hidden;">
        <div id="tb" style="padding: 3px;">
            <div style="padding-left: 20px; padding-right: 20px; color: #444; margin-bottom: 2px;">
                <div style="float: left;">
                    关键字：<input id="TB_KeyWords" type="text" style="width: 250px; border-style: solid;
                        border-color: #aaaaaa;" />
                    <a id="DoQueryByKey" href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'"
                        onclick="LoadGridData(1, 20)">查询</a>
                </div>
                <div class="datagrid-btn-separator">
                </div>
                <a id="A2" href="#" style="float: left;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-back'" onclick="BackDataM()">恢复</a> 
                <a id="ShowAll" href="#" style="float: left;" class="easyui-linkbutton"  data-options="plain:true,iconCls:'icon-delete'" onclick="RecycleBoxgetSelections()">删除所选</a> 
                <a id="A1" href="#" style="float: left;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-delete'"  onclick="RecycleBoxDelAll()">清空垃圾箱</a>
            </div>
            <div style="clear: both;">
            </div>
        </div>
        <table id="newsGrid" fit="true" fitcolumns="true" toolbar="#tb" class="easyui-datagrid">
        </table>
    </div>
</asp:Content>
