﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA.Message
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class DraftBoxAttr : EntityOIDAttr
    {
        public const String FK_MsgNo = "FK_MsgNo";
        public const String AddTime = "AddTime";
        public const String FK_UserNo = "FK_UserNo";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class DraftBox : EntityOID
    {
        #region 属性
        public int FK_MsgNo
        {
            get { return this.GetValIntByKey(DraftBoxAttr.FK_MsgNo); }
            set { this.SetValByKey(DraftBoxAttr.FK_MsgNo, value); }
        }
        public String FK_UserNo
        {
            get { return this.GetValStringByKey(DraftBoxAttr.FK_UserNo); }
            set { this.SetValByKey(DraftBoxAttr.FK_UserNo, value); }
        }
        public DateTime AddTime
        {
            get { return this.GetValDateTime(DraftBoxAttr.AddTime); }
            set { this.SetValByKey(DraftBoxAttr.AddTime, value); }
        }
        #endregion
        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public DraftBox() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public DraftBox(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_MessageDraftBox";
                map.EnDesc = "新闻文章";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBInt(DraftBoxAttr.FK_MsgNo, 0, "消息编号", true, false);
                map.AddTBDateTime(DraftBoxAttr.AddTime, null, "添加时间", true, false);
                map.AddTBString(DraftBoxAttr.FK_UserNo, null, "用户编号", true, false, 0, 50, 30);
                //map.AddTBString(DraftBoxAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class DraftBoxs : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DraftBox();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public DraftBoxs()
        {
        }
    }
}
