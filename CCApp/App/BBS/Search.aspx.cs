﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.DA;
using BP.En;
using BP.OA;
using BP.OA.BBS;
using BP.Sys;
using BP.Web;

namespace CCOA.App.BBS
{
    public partial class Search : WebPage
    {
        public string keywords = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            Localize htmNav = (Localize)this.Page.Master.FindControl("htm_Nav");
            htmNav.Text = "<a href=\"default.aspx\" class=\"ghome\">论坛首页</a> > <a href=\"\" class=\"ghome\">主题搜索</a>";

            keywords = StringFormat.GetQuerystring("k");
            if (keywords == "" || keywords == "搜索关键字")
            {
                this.Alert("关键字不能为空！");
                return;
            }

            if (!IsPostBack)
            {
                DataGridBind(true);
            }
        }

        //绑定列表
        private void DataGridBind(bool start)
        {
            //初始化实例
            BBSMotifs motifs = new BBSMotifs();
            //创建查询对象，并将空实例集合传入查询对象
            QueryObject objInfo = new QueryObject(motifs);
            //添加查询条件
            objInfo.AddWhere(" Name like '%" + keywords + "%'");
            objInfo.addOr();
            objInfo.AddWhere(" Contents like '%" + keywords + "%'");

            if (start)
            {
                //获取符合查询条件总记录数
                this.AspNetPager1.RecordCount = objInfo.GetCount();
            }
            //执行查询符合条件的当前页数据
            objInfo.DoQuery(BBSMotifAttr.No, AspNetPager1.PageSize, AspNetPager1.CurrentPageIndex);
            //分页控件显示内容
            this.AspNetPager1.CustomInfoHTML = string.Format("当前第{0}/{1}页 共{2}条记录 每页{3}条", new object[] { this.AspNetPager1.CurrentPageIndex, this.AspNetPager1.PageCount, this.AspNetPager1.RecordCount, this.AspNetPager1.PageSize });
            //绑定数据
            datalistF.DataSource = motifs;
            datalistF.DataBind();
        }

        //翻页事件
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            DataGridBind(false);
        }

        public string GetClassName(object FK_Class)
        {
            BBSClass bbsClass = new BBSClass(FK_Class.ToString());
            if (bbsClass != null)
                return bbsClass.Name;
            return "";
        }
    }
}