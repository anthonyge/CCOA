var Jh = {	
	Config:{                                                    //CLASS样式配置
		layCls :"layout-list",
		min :"min",
		mintext:"\u6536\u8D77",                                 //收起
		max :"max",
		maxtext:"\u5C55\u5F00",                                 //展开
		close :"close",
		closetext :"\u5173\u95ED",                              //关闭 
		refreshtext:"\u5237\u65B0",                             //刷新
		refresh: "refresh",
		edittext: "\u7f16\u8f91",                               //编辑
        edit:"edit",
		_groupItemContent : "itemContent",
		_groupItemHead : "itemHeader",
		_groupWrapperClass  : "groupWrapper",
		_groupItemClass : "groupItem"
	}	
};
Jh.Layout=function(me){
	var _left = "portal_l",                                     //左侧布局
		_center ="portal_m",                                    //中间布局
		_right ="portal_r";                                     //右侧布局
	return me = {
		location:{//三列容器
			left:_left,
			center:_center,
			right:_right		
		},
		locationId : {
			left:"#"+_left,
			center:"#"+_center,
			right:"#"+_right
		},
		layoutCss : {
			0:"2:3"
		},
		layoutText : {
		    0: "per25 per50 per25"
		}
	}
}();

Jh.Util = {                                            //工具类
	format : function (str, model) {                   //格式化指定键值的模板
		for (var k in model) {
			var re = new RegExp("{" + k + "}", "g");   //g表示全文检索
			str = str.replace(re, model[k])
		}
		return str
	},
	refresh:function(){                                //刷新3个布局
		$("#"+Jh.Layout.left,"#"+Jh.Layout.center,"#"+Jh.Layout.right).sortable('refresh');
	},
	toBody:function(o){                                //往Body添加对象
		$("body").append(o);
	}
};
Jh.Portal = function (me) {                                                                 //Portal对象
    var _Lwdith, _Mwidth, _Rwidth;    
    _Lwdith = ' per25';
    _Mwidth = ' per50';
    _Rwidth = ' per25';

    var _box = "<div id='portal'></div>",
		_template = {                                                                                              //模板
		    l :"<div id='"+Jh.Layout.location.left+"' class='"+Jh.Config._groupWrapperClass+_Lwdith+"'/>",
		    m :"<div id='"+Jh.Layout.location.center+"' class='"+Jh.Config._groupWrapperClass+_Mwidth+"'/>",
		    r :"<div id='"+Jh.Layout.location.right+"' class='"+Jh.Config._groupWrapperClass+_Rwidth+"'/>",
		    portalWrap:"<div id='{key}' class='"+Jh.Config._groupItemClass+"'/>",
		    itemHeader:"<div class='"+Jh.Config._groupItemHead+"'><h3>{name}</h3></div>",
		    itemContent:"<div class='"+Jh.Config._groupItemContent+"'/>"
		};
    return me={	
        init:function(op){                                                            //初始化			
            me._create();
            me._bindData(op);
            me._bindEvent();
        },

        _create:function(){                                                           //创建
            me.box = $(_box);
            me._elements = {};		
            me._createModulesWrap();
            Jh.Util.toBody(me.box);	
        },	
		
        _bindData:function(op){                                                       //绑定数据
            $.each(op,function(key,item){				
                me._createPortal(key,item);
            });
        },
		
        _createModulesWrap:function(){                                                //创建模块外层容器
            me._elements.m_l = $(_template.l);
            me._elements.m_m = $(_template.m);
            me._elements.m_r = $(_template.r);
            me._addPanel(me._elements.m_l);
            me._addPanel(me._elements.m_m);
            me._addPanel(me._elements.m_r);
        },
		
        _addPanel:function(o){                                                        //往容器里添加
            me.box.append(o);
        },
		
        _createPortal:function(key,item){                                             //创建portal
            var mWrap ;
            switch(key){
                case "appL":mWrap = me._elements.m_l;
                    break;
                case "appM":mWrap = me._elements.m_m;
                    break;
                case "appR":mWrap = me._elements.m_r;
                    break;
            }
			
            $.each(item,function(k,o){				
                mWrap.append(me._createPortalOne(o));
            });
		
        },
        _createPortalOne:function(o){                                               //创建单个portal item
            var itemHeader  =  me._createItemHeader(o.cnName),//header
			    itemContent =  me._createItemContent(o.url),//content
			    portalWrap  = $(Jh.Util.format(_template.portalWrap,{"key":o.id}))
							.append(itemHeader)
							.append(itemContent);
            return portalWrap;
        },
        _createItemHeader:function(name){                                                 //创建Head
            var  _itemHeader = $(Jh.Util.format(_template.itemHeader,{"name":name})),     //格式化header			
				_actionWrap =  me._createDiv("action").hide().appendTo(_itemHeader);      //创建一个div
			
            me._createA(Jh.Config.edit, Jh.Config.edittext, true).appendTo(_actionWrap);
            me._createA(Jh.Config.refresh,Jh.Config.refreshtext,true).appendTo(_actionWrap);
            me._createA(Jh.Config.min,Jh.Config.mintext,true).appendTo(_actionWrap);
            me._createA(Jh.Config.max,Jh.Config.maxtext,false).appendTo(_actionWrap);
            me._createA(Jh.Config.close,Jh.Config.closetext,true).appendTo(_actionWrap);
			
            _itemHeader.hover(function(){                                                 //滑过标题出现删除图标
                $(this).find(".action").show();			
            },function(){
                $(this).find(".action").hide();
				
            });
            return _itemHeader;
        },
        _createItemContent:function(_url){//创建content
            var _content = $(_template.itemContent);
            var xmlReq = $.ajax({
                async: false,
                dataType: "html",
                url: _url,
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert(XMLHttpRequest.status);		            
                }
            });
            
            $("<div style='height:auto;padding-bottom:3px;'></div>").append(xmlReq.responseText).appendTo(_content);

            return _content;
        },
        _createDiv:function(classname){
            var _div = $("<div/>").addClass(classname);
            return _div;
        },
        _createA:function(classname,title,isShow){//创建A 
            var _a = $("<a href='javascript:void(0);' class='"+classname+"' title='"+title+"'/>");
            if(!isShow){
                _a.hide();
            }
            return _a ;
        },
        eventMin: function () {
            $(".itemContent").each(function (){
                $(this).hide();
                $("." + Jh.Config.min).each(function () {
                    $(this).hide();
                });
                $("." + Jh.Config.max).each(function () {
                    $(this).show();
                });
            });
        },
        _eventMin :function(){		
            $("."+Jh.Config.min).live("click",function(){//关闭面板
                var _me = $(this);
                var _groupItem = _me.parent().parent().parent();
                _groupItem.find("."+Jh.Config._groupItemContent).hide();
                _groupItem.find("."+Jh.Config.max).show();
                _me.hide();
            });	
        },
        eventMax:function(){
            $(".itemContent").each(function () {
                $(this).show();
                $("." + Jh.Config.min).each(function () {
                    $(this).show();
                });
                $("." + Jh.Config.max).each(function () {
                    $(this).hide();
                });
            });
        },
        _eventMax:function(){			
            $("."+Jh.Config.max).live("click",function(){        //展开面板
                var _me = $(this),
					_groupItem = _me.parent().parent().parent();
                _groupItem.find("."+Jh.Config._groupItemContent).show();
                _groupItem.find("."+Jh.Config.min).show();
                _me.hide();
            });
        },
        saveInfo: function () {
            var _l = $("#" + Jh.Layout.location.left).sortable('toArray'),
			    _m = $("#" + Jh.Layout.location.center).sortable('toArray'),
				_r = $("#" + Jh.Layout.location.right).sortable('toArray'),
				_a = $("." + Jh.Config.layCls + " a"),
				_layout = "";
                _a.each(function () {
                    if ($(this).hasClass("active")) {
                        _layout = $(this).attr("rel");
                    }
                });
                var baseConfig = "" + _l + "#" + _m + "#" + _r + "";  //当前布局    
                var param = { method: 'saveDesktopIdx', baseConfig: baseConfig };
                $.post("desktop/updateDeskTop.aspx", param, function (js) {
                    alert(js);
                });
            
        },
        _eventRemove:function(){
            $("."+Jh.Config.close).live("click",function(){      //移除面板
                var _this  = $(this),
					_p  = _this.parent().parent().parent();      //得到当前父级面板				
                _p.fadeOut('500',function(){//500毫秒后移除
                    var _this = $(this);
                    var _id = _this.attr("id");//得到模块id			
                    var _a  = $(".tag-list").find("a[rel='"+_id+"']");
                    _this.remove();
                    _a.parent().remove();//移除功能列表中的li
                });			
            });
        },		
        _eventEdit:function()
        {
            $("." + Jh.Config.edit).live("click", function () {
                var _this = $(this),
				_p = _this.parent().parent().parent();//得到当前父级面板				
                var _id = _p.attr("id");//得到模块id
                alert(_id);
            });
        },
        _eventRefresh:function(){	
            $("."+Jh.Config.refresh).live("click",function(){                           //刷新
                var _this = $(this),
				_p = _this.parent().parent().parent();//得到当前父级面板				
                var _id = _p.attr("id");//得到模块id
                alert(_id);
            });
        },
		
        _eventSortable:function(){                                                      //绑定排序
            $("."+Jh.Config._groupWrapperClass).sortable({
                connectWith: "."+Jh.Config._groupWrapperClass,								
                opacity :"0.6",	
                dropOnEmpty: true,				
            }).disableSelection();			
        },
                		
		_bindEvent:function(){	                                                       //绑定面板所有事件		
		    me._eventEdit();
			me._eventRefresh();
			me._eventRemove();
			me._eventMax();
			me._eventMin();
			me._eventSortable();
		}	
	
	};
}();