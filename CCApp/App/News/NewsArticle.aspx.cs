﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BP.OA;

namespace CCOA.App.News
{
    public partial class NewsArticle : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        private void Alert(string msg)
        {
            BP.OA.Debug.Alert(this, msg);
        }
        private int rId
        {
            get { return Convert.ToInt32(Request.QueryString["ID"]); }
        }
        private System.Data.DataTable GetNewsDetail(int id)
        {
            //返回数据表格得包含以下字段：Title,KeyWords,ArticleCatagory,AddTime,FK_UserNo,Doc,ArticleSource
            Article ar = new Article();
            ar.OID = id;
            int iR = ar.RetrieveFromDBSources();
            if (iR > 0)
            {
                ar.BrowseCount++;
                this.Title = ar.Title;
                ar.Update();
            }
            return ar.GetNewsDetail(id);
        }
        /// <summary>
        /// 根据存储路径获取老文件名，必须是存储路径中包含老文件名才可。
        /// </summary>
        /// <param name="savePath"></param>
        /// <returns></returns>
        private string GetAttachOldFileName(string savePath)
        {
            string saveFullPath = Server.MapPath(savePath);
            //文件不存在直接反回空
            if (!System.IO.File.Exists(saveFullPath)) return "";

            String attachFile = System.IO.Path.GetFileName(saveFullPath);
            String ext = System.IO.Path.GetExtension(saveFullPath);
            int index = attachFile.LastIndexOf('_');
            string oldName = index == -1 ? attachFile : attachFile.Substring(0, index);
            //String[] arr_attachFile = attachFile.Split('_');
            return String.Format("{0}{1}", oldName, ext);
        }
        public string GetUserNames(object userNos)
        {
            String users = String.Format("{0}", userNos);
            return BP.OA.GPM.GetUserNames(users);
        }
        public string GetAllAttachStr(string savePath, string splitter, string linkFormat)
        {
            if (String.IsNullOrEmpty(splitter)) splitter = "&nbsp;&nbsp;";
            if (String.IsNullOrEmpty(linkFormat)) linkFormat = "<a href='{1}'>{0}</a>";
            StringBuilder sb = new StringBuilder();
            String[] attachFiles = savePath.Split(new String[] { ";", ",", "|" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (String attachFile in attachFiles)
            {
                if (sb.Length > 0) sb.Append(splitter);
                sb.AppendFormat(linkFormat
                    , this.GetAttachOldFileName(attachFile)
                    , attachFile);
            }
            return sb.ToString();
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadData();
                //清除即时消息
                BP.OA.ShortMsg.ReceivedInfo(this.rId.ToString(), "新闻");
            }
        }
        private void LoadData()
        {
            //System.Data.DataTable dt = this.GetNewsDetail(this.rId);
            Article ar = new Article(this.rId);
            if (ar == null) return;
            this.lblTitle.Text = ar.Title;
            this.lblUser.Text = ar.FK_UserNo;
            this.lblNewsCatagory.Text = ar.FK_ArticleCatagory;
            this.lblAddTime.Text = this.f(ar.AddTime, "{0:yyyy年MM月dd日 HH:mm:ss}");
            this.liContent.Text = ar.Doc;
            this.lblArticleSource.Text = ar.ArticleSource;

            string attachPath = ar.AttachFile;
            if (!String.IsNullOrEmpty(attachPath))
            {
                this.pnAttachFile.Visible = true;
                //StringBuilder sb=new StringBuilder();
                //String[] attachFiles = attachPath.Split(new String[]{";",",","|"},StringSplitOptions.RemoveEmptyEntries);
                //foreach (String attachFile in attachFiles)
                //{
                //    if (sb.Length > 0) sb.Append("&nbsp;&nbsp;");
                //    sb.AppendFormat("<a href='{1}'>{0}</a>"
                //        ,this.GetAttachOldFileName(attachFile)
                //        ,attachFile);
                //}
                this.liAttachFiles.Text = this.GetAllAttachStr(attachPath, "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;", null);
            }
        }
        private string f(object eval, string f)
        {
            if (String.IsNullOrEmpty(f)) f = "{0}";
            return String.Format(f, eval);
        }
        #endregion

        #region //3.页面事件(Page Event)

        #endregion
    }
}