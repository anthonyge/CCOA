﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
namespace CCOA.Ctrl.ShortFunc
{
    public partial class ShortCols2 : System.Web.UI.UserControl
    {
        #region //PI
        //0-ConnectionString,1-AppSettings
        private int _Cn_Mode = 0;
        public int Mode
        {
            get { return this._Cn_Mode; }
            set { this._Cn_Mode = value; }
        }
        private string _Cn_Name = null;
        public String Cn
        {
            get { return this._Cn_Name; }
            set { this._Cn_Name = value; }
        }
        private string _Table = null;
        public string Table
        {
            get { return this._Table; }
            set { this._Table = value; }
        }
        private string _ID_Fld=null;
        public string IdFld
        {
            get { return this._ID_Fld; }
            set { this._ID_Fld = value; }
        }
        private string _Name_Fld=null;
        public string NameFld
        {
            get { return this._Name_Fld; }
            set { this._Name_Fld = value; }
        }
        #endregion

        #region //Load Part
        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.NameFld) ||
                String.IsNullOrEmpty(this.Table) ||
                String.IsNullOrEmpty(this.IdFld) ||
                String.IsNullOrEmpty(this.NameFld))
            {
                this.Response.Write("无效参数");
                this.Response.End();

                return;
            }
            this.LoadCtrl();
        }
        private void LoadCtrl()
        {
            if (this.Mode == 0)
                this.SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings[this.Cn].ToString();
            else
                this.SqlDataSource1.ConnectionString = ConfigurationManager.AppSettings[this.Cn].ToString();
            this.SqlDataSource1.SelectCommand = String.Format("SELECT [{0}] as No, [{1}] as Name FROM [{2}] Order By {0}", this.IdFld, this.NameFld, this.Table);
            this.SqlDataSource1.UpdateCommand = String.Format("UPDATE [{2}] SET [{1}] = @Name WHERE [{0}] = @No", this.IdFld, this.NameFld, this.Table);
            this.SqlDataSource1.InsertCommand = String.Format("INSERT INTO [{2}] ([{0}],[{1}]) VALUES (@No,@Name)", this.IdFld, this.NameFld, this.Table);
            this.SqlDataSource1.DeleteCommand = String.Format("DELETE FROM [{1}] WHERE [{0}] = @No", this.IdFld, this.Table);
        }
        #endregion

        #region //Events
        #endregion
    }
}