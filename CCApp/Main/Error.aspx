﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="CCOA.Main.Error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 400px; height: 200px; padding: 190px 30px 30px 30px; margin:100px auto; text-align: center;
        vertical-align: middle; border:dashed 1px #999999; background-color:#eeeeee;">
        <asp:Label runat="server" ID="lbMsg" Font-Size="XX-Large" Font-Bold="true" ForeColor="Red"></asp:Label>
    </div>
    </form>
</body>
</html>
