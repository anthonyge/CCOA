﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.DailySupplies
{
    public partial class GuideHZPlan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM ND3203Dtl1 WHERE RDT LIKE '2014%'";
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);

            this.Pub1.AddTable();
            this.Pub1.AddTR();
            this.Pub1.AddTDTitle("");
            this.Pub1.AddTREnd();
 
            this.Pub1.AddTableEnd();

        }

        public void Save()
        {
            //生成workid.
            Int64 workid = BP.WF.Dev2Interface.Node_CreateBlankWork("037",
                null, null, BP.Web.WebUser.No, null);

            // 删除可能存在的数据.
            //BP.OA.DS.ND3701Dtl mydtl = new BP.OA.DS.ND3701Dtl();
            //mydtl.Delete("RefPK", workid.ToString());


            // 插入数据.
            for (int i = 0; i < 12; i++)
            {
                //BP.OA.DS.ND3701Dtl dtl = new BP.OA.DS.ND3701Dtl();
                //dtl.RefPK = workid.ToString();

                //dtl.Insert();
            }

            //转入到流程.
            this.Response.Redirect("/WF/MyFlow.aspx?FK_Flow=037&WorkID=" + workid, true);
           // ND3701Dtl1
        }
    }
}