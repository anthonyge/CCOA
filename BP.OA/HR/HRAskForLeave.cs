
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.OA
{
    
    /// <summary>
    /// 请假 属性
    /// </summary>
    public class HRAskForLeaveAttr : EntityOIDAttr
    {
        #region 基本属性
        /// <summary>
        /// FK_人事档案
        /// </summary>
        public const string Fk_HrRecord = "Fk_HrRecord";
        /// <summary>
        /// FK_部门
        /// </summary>
        public const string Fk_Dept = "Fk_Dept";
        /// <summary>
        /// FK_岗位
        /// </summary>    
        public const string Fk_Station = "Fk_Station";
        /// <summary>
        /// 请假时间
        /// </summary>
        public const string Fk_NY = "Fk_NY";
        /// <summary>
        /// 请假时间从
        /// </summary>
        public const string StartDT = "StartDT";
        /// <summary>
        /// 请假时间到
        /// </summary>
        public const string EndDT = "EndDT";
        /// <summary>
        /// 请假小时数
        /// </summary>
        public const string Hours = "Hours";
        /// <summary>
        /// 请假类型
        /// </summary>
        public const string AskCate = "AskCate";
        /// <summary>
        /// 请假原因
        /// </summary>
        public const string AskWhy = "AskWhy";
      

        #endregion
    }

    /// <summary>
    /// 请假
    /// </summary>
    public class HRAskForLeave : EntityOID
    {

        #region 基本属性
        /// <summary>
        /// FK_人事档案
        /// </summary>
        public string Fk_HrRecord
        {
            get { return this.GetValStringByKey(HRAskForLeaveAttr.Fk_HrRecord); }
            set { this.SetValByKey(HRAskForLeaveAttr.Fk_HrRecord, value); }
        }
        /// <summary>
        /// FK_部门
        /// </summary>
        public string Fk_Dept
        {
            get { return this.GetValStringByKey(HRAskForLeaveAttr.Fk_Dept); }
            set { this.SetValByKey(HRAskForLeaveAttr.Fk_Dept, value); }
        }
        
        /// <summary>
        /// 请假时间从
        /// </summary>
        public string StartDT
        {
            get { return this.GetValStringByKey(HRAskForLeaveAttr.StartDT); }
            set { this.SetValByKey(HRAskForLeaveAttr.StartDT, value); }
        }
        /// <summary>
        /// 请假时间到
        /// </summary>
        public string EndDT
        {
            get { return this.GetValStringByKey(HRAskForLeaveAttr.EndDT); }
            set { this.SetValByKey(HRAskForLeaveAttr.EndDT, value); }
        }
        /// <summary>
        /// 请假小时数
        /// </summary>
        public string Hours
        {
            get { return this.GetValStringByKey(HRAskForLeaveAttr.Hours); }
            set { this.SetValByKey(HRAskForLeaveAttr.Hours, value); }
        }
        /// <summary>
        /// 请假类型
        /// </summary>
        public string AskCate
        {
            get { return this.GetValStringByKey(HRAskForLeaveAttr.AskCate); }
            set { this.SetValByKey(HRAskForLeaveAttr.AskCate, value); }
        }
        /// <summary>
        /// 请假原因
        /// </summary>
        public string AskWhy
        {
            get { return this.GetValStringByKey(HRAskForLeaveAttr.AskWhy); }
            set { this.SetValByKey(HRAskForLeaveAttr.AskWhy, value); }
        }
        /// <summary>
        /// 请假时间
        /// </summary>
        public string Fk_NY
        {
            get { return this.GetValStringByKey(HRAskForLeaveAttr.Fk_NY); }
            set { this.SetValByKey(HRAskForLeaveAttr.Fk_NY, value); }
        }

        #endregion

        #region 构造函数

        public HRAskForLeave()
        {

        }

        public HRAskForLeave(int oid)
            : base(oid)
        {

        }
        #endregion

        #region 重写父类的方法
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                {
                    return this._enMap;
                }

                Map map = new Map("OA_HRAskForLeave");
                map.EnDesc = "请假";
                //编号
                map.AddTBIntPKOID();

                //字段
                //map.AddDDLEntities(HRAskForLeaveAttr.Fk_HrRecord,null,"请假人",new Emps() , true);
                map.AddTBString(HRAskForLeaveAttr.Fk_HrRecord, null, "请假人", true, true, 0, 1000, 100, true);

                map.AddDDLSysEnum(HRAskForLeaveAttr.AskCate, 0, "请假类型", true, true, HRAskForLeaveAttr.AskCate, "@0=病假@1=婚假@2=产假@3=公事外出@4=其他");

                map.AddTBDateTime(HRAskForLeaveAttr.Fk_NY, null, "请假时间", true, false);
                //map.AddDDLEntities(HRAskForLeaveAttr.Fk_Dept, null, "部门", new Depts(), true);
                map.AddTBString(HRAskForLeaveAttr.Fk_Dept, null, "部门", true, true, 0, 1000, 100, true);
                map.AddTBDateTime(HRAskForLeaveAttr.StartDT,null, "请假时间从", true,false);
                map.AddTBDateTime(HRAskForLeaveAttr.EndDT, null, "请假时间到", true, false);             
                map.AddTBString(HRAskForLeaveAttr.Hours, null, "请假天数", true, true, 0, 1000, 100, true);

                map.AddTBStringDoc(HRAskForLeaveAttr.AskWhy, null, "请假原因", true, false, true);

              

                //查询条件
                //map.AddSearchAttr(HRAskForLeaveAttr.Fk_Dept);
                //map.AddSearchAttr(HRAskForLeaveAttr.Fk_HrRecord);
                map.DTSearchKey = HRAskForLeaveAttr.Fk_NY;
                map.DTSearchWay = Sys.DTSearchWay.ByDate;//查询条件为日期类型
              

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }

    /// <summary>
    /// 请假 s
    /// </summary>
    public class HRAskForLeaves : EntitiesOID
    {

        #region 构造函数

        public HRAskForLeaves()
        {

        }
        #endregion

        #region 重写父类的方法
        public override Entity GetNewEntity
        {
            get { return new HRAskForLeave(); }
        }
        #endregion
    }
}




