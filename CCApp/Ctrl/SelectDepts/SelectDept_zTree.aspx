﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectDept_zTree.aspx.cs" Inherits="CCOA.Ctrl.SelectDepts.SelectDept_zTree" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function getReturnText() {
            return $("#txt_Stations").val();
        }
        function getReturnValue() {
            return $("#txt_StationNames").val();
        }
    </script>
    <link rel="stylesheet" href="/js/zTree_v3.5.14/css/demo.css" type="text/css">
    <link rel="stylesheet" href="/js/zTree_v3.5.14/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <script type="text/javascript" src="/js/zTree_v3.5.14/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="/js/zTree_v3.5.14/js/jquery.ztree.core-3.5.js"></script>
    <script type="text/javascript" src="/js/zTree_v3.5.14/js/jquery.ztree.excheck-3.5.js"></script>
    <script type="text/javascript">
		<!--
        var setting = {
            check: {
                enable: true,
                chkStyle: "radio",
                radioType: "all"
                
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onCheck: zTreeOnCheck
            }
        };
        function zTreeOnCheck(event, treeId, treeNode) {
                 //alert(treeNode.tId + ", " + treeNode.name + "," + treeNode.checked);
                 if(treeNode.checked)
                 {
                   $("#txt_StationNames").val(treeNode.id);
                   $("#txt_Stations").val(treeNode.name);
                 }
        };
        var zNodes = [
			<%= this.JsonTree %>
		];

        $(document).ready(function () {
            $.fn.zTree.init($("#treeDemo"), setting, zNodes);
            var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
            var node = treeObj.getNodeByParam("id", <%=this.rIn %>, null);
        });
		//-->
	</script>
</head>
<body>
    <form id="form1" runat="server">
    <div style=" margin-left:20px;">
        <!--class="zTreeDemoBackground left"--> 
        <div >
            <ul id="treeDemo" class="ztree" style=" padding:10px 10px 10px 10px; width:90%;">
            </ul>
        </div>
        <asp:HiddenField ID="txt_StationNames" runat="server" />
        <asp:HiddenField ID="txt_Stations" runat="server" />
    </div>
    </form>
</body>
</html>
