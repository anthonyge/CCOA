﻿<%@ Page Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true" CodeBehind="InDetail1.aspx.cs" Inherits="CCOA.App.Message.InDetail1" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cph_head">
    <style type="text/css">
       body{
            font-size: 13px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cph_title">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cph_body">
    <div style="font-size: 35px; font-weight: bolder;position:relative; ">
        <a href="InBox.aspx"><img style="width: 40px; height: 40px;" src="img/back.png" alt="收件箱" />收件箱</a></div>
    <div style=" padding-top:10px;">
        <p style="font-size: x-large; text-align: center; font-weight: bolder;height:5px;">
            <asp:Literal runat="server" ID="liTitle"></asp:Literal></p>
        <p style="text-align: left; padding-left: 10px;">
            发送人员：
            <asp:Literal runat="server" ID="liUser"></asp:Literal>&nbsp;&nbsp;&nbsp;&nbsp;发布时间：<asp:Literal runat="server" ID="liRDT"></asp:Literal>
        </p>
        <div style="text-align: left; padding-left: 10px;">
            <div style="float: left;">
                接收人员：</div>
            <div style="float: left; padding: 1px 1px 1px 1px; border: dashed 1px #aaa; width: 80%;height:17px; overflow:visible;">
                <asp:Literal runat="server" ID="liReceivers"></asp:Literal></div>
            <div style="clear: both;">
            </div>
        </div>
        <div style="text-align: left; padding-left: 10px; padding-top: 2px;">
            <div style="float: left;">
                抄送人员：</div>
            <div style="float: left; padding: 1px 1px 1px 1px; border: dashed 1px #aaa; width: 80%;
                height: 17px; overflow: visible;">
                <asp:Literal runat="server" ID="liCopyUsers"></asp:Literal></div>
            <div style="clear: both;">
            </div>
        </div>
        <div style="margin: 10x 10px 10px 10px; padding: 10px 10px 10px 10px; border: dashed 1px #dddddd;
            background-color: #efefef; min-height:300px;">
            <asp:Literal runat="server" ID="liDoc"></asp:Literal>
        </div>
        <asp:Panel runat="server" ID="pnAttachFile" Visible="false" Style="border-top: dashed 1px #777;">
            &nbsp;&nbsp;&nbsp;<img src="img/attach1.png" style="height:18px;">附件下载(右击另存下载)：<asp:Literal runat="server" ID="liAttachFiles"></asp:Literal>
        </asp:Panel>
    </div>
</asp:Content>
