﻿<%@ Page Title="" Language="C#" MasterPageFile="../master/Site1.Master" AutoEventWireup="true"
    CodeBehind="SendShortMsg.aspx.cs" Inherits="CCOA.Main.Sys.SendShortMsg" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <style type="text/css">
        td.title
        {
            width: 100px;
            text-align: right;
        }
        td.text
        {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <h3>
        发送短消息</h3>
    <table cellpadding="3" cellspacing="0" border="1px" style="width: 800px; border-collapse: collapse;">
        <tr>
            <td class="title">
                发送人:
            </td>
            <td class="text">
                <div id="div_SendUsers" style="display: block; margin-top: 1px; padding-top: 1px;">
                    <asp:TextBox ID="txt_SendUsers" runat="server" ReadOnly="true" Width="98%" TextMode="MultiLine"
                        Rows="10" BackColor="#eeeeee"></asp:TextBox>
                    <asp:HiddenField ID="txt_SendUserNames" runat="server"></asp:HiddenField>
                    <br />
                    <input type="button" value="添加人员.." onclick="zDialog_open2('../../ctrl/SelectUsers/SelectUser_Jq.aspx', '选择人员', 650, 430,'#txt_SendUserNames','txt_SendUserNames','txt_SendUsers');" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="title">
                发送内容:
            </td>
            <td class="text">
                <asp:TextBox ID="txt_Text" runat="server" Width="98%" TextMode="MultiLine" Rows="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="title">
                发送到手机:
            </td>
            <td class="text">
                <asp:CheckBox runat="server" Checked="false" ID="cb_IsWap" />
            </td>
        </tr>
    </table>
    <div style="width: 800px; text-align: center; padding-top: 5px;">
        <asp:Button runat="server" ID="btnSend" Text="发送" OnClick="btnSend_OnClick" /></div>
</asp:Content>
