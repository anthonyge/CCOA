﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Sort.aspx.cs" Inherits="CCOA.App.KM.Sort" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Js/Js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/tree.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/easytemplate.js" type="text/javascript"></script>
    <script src="js/CC.MessageLib.js" type="text/javascript"></script>
    <script src="js/AppData.js" type="text/javascript"></script>
    <script src="js/Sort.js" type="text/javascript"></script>
</head>
<body class="easyui-layout">
    <div id="pageloading">
    </div>
    <div data-options="region:'west',split:true" title="知识树" style="width: 360px; padding1: 1px;
        overflow: hidden;">
        <div style="width: 100%; height: 100%; overflow: auto;">
            <div style="padding: 0px; background: #fafafa; width: 100%;">
                <a href="#" class="easyui-menubutton" data-options="menu:'#mm1',iconCls:'icon-new'">
                    新建</a> <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-edit'"
                        onclick="EditNode();">修改</a> <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-cancel'"
                            onclick="DeleteNode();">删除</a> <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-up'"
                                onclick="DoUp();">上移</a> <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-down'"
                                    onclick="DoDown();">下移</a>
            </div>
            <ul id="kmSortTree" class="easyui-tree-line" data-options="animate:false,dnd:false">
            </ul>
            <div id="treeMM" class="easyui-menu" style="width: 120px;">
                <div data-options="iconCls:'icon-new'" onclick="CreateSampleNode();">
                    新建同级</div>
                <div data-options="iconCls:'icon-new'" onclick="CreateSubNode();">
                    新建下级</div>
                <div data-options="iconCls:'icon-edit'" onclick="EditNode();">
                    修改</div>
                <div data-options="iconCls:'icon-cancel'" onclick="DeleteNode();">
                    删除</div>
                <div class="menu-sep">
                </div>
                <div data-options="iconCls:'icon-up'" onclick="DoUp();">
                    上移</div>
                <div data-options="iconCls:'icon-down',disabled:false" onclick="DoDown();">
                    下移</div>
            </div>
            <div id="mm1" style="width: 150px;">
                <div data-options="iconCls:'icon-new'" onclick="CreateSampleNode();">
                    新建同级</div>
                <div data-options="iconCls:'icon-new'" onclick="CreateSubNode();">
                    新建下级</div>
            </div>
        </div>
    </div>
    <div data-options="region:'center'" style="overflow: hidden;">
        <div style="padding: 0px; background: #fafafa; width: 100%;">
            <a href="#" class="easyui-linkbutton" data-options="plain:true"><b><span id="curModelText"
                style="color: Red;">当前选择按人员分配权限</span></b></a> <a href="#" class="easyui-linkbutton"
                    data-options="plain:true,iconCls:'icon-reset'" onclick="CheckedAll()"><span id="ckbAllText">全选</span></a> <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'"
                            onclick="SaveSortRight();">保存</a> <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-rights'"
                                onclick="DistributeRight('emp')">按人员分配权限</a> <a href="#" class="easyui-linkbutton"
                                    data-options="plain:true,iconCls:'icon-rights'" onclick="DistributeRight('station')">
                                    按岗位分配权限</a> <a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-rights'"
                                        onclick="DistributeRight('dept')">按部门分配权限</a>
        </div>
        <div style="overflow: auto; height: 95%;">
            <%-- 下列为easytemplate的控件--%>
            <div id="templatePanel">
            </div>
            <br />
        </div>
    </div>
    <div id="treeSortDialog" data-options="iconCls:'icon-save'">
        <div style="height: 30px; overflow: auto; margin: 10px;">
            知识类别名称:<input type="text" name="sortName" id="sortName" />
        </div>
    </div>
</body>
</html>
