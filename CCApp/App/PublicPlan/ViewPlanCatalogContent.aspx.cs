﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.DA;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.OA.PublicPlan;

namespace CCOA.App.PublicPlan
{
    public partial class ViewPlanCatalogContent : WebPage
    {
        private string PK
        {
            get
            {
                return this.Request.QueryString["PK"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //页面第一次加载
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.PK))
                {
                    BindPageControl();
                }
            }
        }

        /// <summary>
        /// 绑定页面值
        /// </summary>
        private void BindPageControl()
        {
            PlanStageContent planStage = new PlanStageContent();
            planStage.RetrieveByAttr(PlanStageContentAttr.No, this.PK);
            //如果用户信息不存在，说明不属于任何一个人，认为是记录不存在
            if (!string.IsNullOrEmpty(planStage.FK_Emp))
            {
                //获取计划信息
                PlanCatalog planCatalog = new PlanCatalog();
                planCatalog.RetrieveByAttr(PlanCatalogAttr.No, planStage.FK_PlanStageContent);
                ui_PlanName.Text = planCatalog.Name;
                //获取上次计划信息
                PlanStageContent perStageContent = new PlanStageContent();
                QueryObject obj = new QueryObject(perStageContent);
                obj.Top = 1;
                obj.AddWhere(PlanStageContentAttr.FK_PlanStageContent, planCatalog.No);
                obj.addAnd();
                obj.AddWhere(PlanStageContentAttr.FK_Emp, WebUser.No);
                obj.addAnd();
                obj.AddWhere(PlanStageContentAttr.No, "<", planStage.No);
                obj.DoQuery();
                if (perStageContent != null && !string.IsNullOrEmpty(perStageContent.NextComplate))
                    ui_PerComplate.Text = perStageContent.NextComplate;
                else
                    ui_PerComplate.Text = "无";
                //本次计划内容
                ui_StartDate.Text = planStage.StartDate;
                ui_EndDate.Text = planStage.EndDate;
                ui_TargetContent.Text = planStage.TargetContent;
                ui_DegreeItems.Text = planStage.DegreeItem;
                ui_NextComplate.Text = planStage.NextComplate;
                ui_FinishPercent.Text = planStage.FinishPercent.ToString();
            }
            else
            {
                this.Alert("没有获取到相关记录！");
            }
        }
    }
}