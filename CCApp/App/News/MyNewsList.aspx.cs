﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.OA;
namespace CCOA.App.News
{
    public partial class MyNewsList : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;

        private void Alert(string msg)
        {
            BP.OA.Debug.Alert(this, msg);
        }
        private DataTable GetArticleCataogry()
        {
            string sSql = "Select No,Name from OA_ArticleCatagory order by No";
            return BP.DA.DBAccess.RunSQLReturnTable(sSql);
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BP.OA.UI.Dict.BindListCtrl(this.GetArticleCataogry(), this.ddlNewsCatagory, "Name", "No", null, "0#所有类型", "0");
                this.BindData(true);
                //清除即时消息
                BP.OA.ShortMsg.ReceivedInfo(null, "新闻");
            }
        }
        //绑定数据
        public void BindData(bool start)
        {
            string con = null;
            string keyWords = this.tbKeyWords.Text;
            string cataValue = this.ddlNewsCatagory.SelectedValue;
            if (cataValue != "0")
            {
                con += String.Format(" and A.FK_ArticleCatagory='{0}'", cataValue);
            }
            if (!String.IsNullOrEmpty(keyWords))
            {
                con += String.Format(" and Title LIKE '%{0}%' OR KeyWords like '%{0}%'", keyWords);
            }
            if (!String.IsNullOrEmpty(con))
            {
                con = " where 1=1 " + con;
            }
            //UI专用测试数据
            string sSql = String.Format("select OID as id,Title,KeyWords,B.Name as ArticleCatagory,AddTime,FK_UserNo,BrowseCount,SetTop from OA_Article A"
                             + " inner join OA_ArticleCatagory B on A.FK_ArticleCatagory=B.No {0}", con);
            if (start)
            {
                AspNetPager1.RecordCount = BP.OA.Main.GetPagedRowsCount(sSql);  //第一次需要初始化
                AspNetPager1.PageSize = 20;
                AspNetPager1.CurrentPageIndex = 1;
            }
            this.AspNetPager1.CustomInfoHTML = string.Format("当前第{0}/{1}页 共{2}条记录 每页{3}条", new object[] { this.AspNetPager1.CurrentPageIndex, this.AspNetPager1.PageCount, this.AspNetPager1.RecordCount, this.AspNetPager1.PageSize });
            GridView1.DataSource = BP.OA.Main.GetPagedRows(sSql, 0, "Order by CAST(SetTop as datetime) desc,AddTime desc", this.AspNetPager1.PageSize, this.AspNetPager1.CurrentPageIndex);
            GridView1.DataBind();
        }
        private int SetNewsTop(int oid)
        {
            string now = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
            string sSql = String.Format("Update OA_Article set SetTop='{0}' where OID={1}", now, oid);
            int iR = BP.DA.DBAccess.RunSQL(sSql);
            return iR;
        }
        private int CancelNewsTop(int oid)
        {
            string sSql = String.Format("Update OA_Article set SetTop=''  where OID={0}", oid);
            int iR = BP.DA.DBAccess.RunSQL(sSql);
            return iR;
        }
        public string GetLimitedLength(object evalStr)
        {
            string v = Convert.ToString(evalStr);
            if (String.IsNullOrEmpty(v)) return null;
            if (v.Length > 7)
            {
                return v.Substring(0, 7) + "..";
            }
            else
                return v;
        }
        #endregion

        #region //3.页面事件(Page Event)
        protected void SetTop(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            int id = Convert.ToInt32(lb.CommandName);
            int iR = this.SetNewsTop(id);
            //5.（用户及业务对象）统计与状态

            //6.登记日志
            if (iR > 0)
            {
                //WX.Main.AddLog(WX.LogType.Default, String.Format("删除公告({0})成功！", id), "");
            }

            //7.返回处理结果或返回其它页面。
            if (iR > 0)
            {
                this.BindData(false);
            }
            else
            {
                BP.OA.Debug.Alert(this, "置顶失败！");
            }
        }
        protected void CancelTop(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            int id = Convert.ToInt32(lb.CommandName);
            int iR = this.CancelNewsTop(id);
            //5.（用户及业务对象）统计与状态

            //6.登记日志
            if (iR > 0)
            {
                //WX.Main.AddLog(WX.LogType.Default, String.Format("删除公告({0})成功！", id), "");
            }

            //7.返回处理结果或返回其它页面。
            if (iR > 0)
            {
                this.BindData(false);
            }
            else
            {
                BP.OA.Debug.Alert(this, "置顶失败！");
            }
        }

        //删除处理过程
        protected void Del(object sender, EventArgs e)
        {
            //1.验证用户权限
            //2.取得用户变量
            LinkButton lb = (LinkButton)sender;
            int id = Convert.ToInt32(lb.CommandName);
            //3.验证用户变量，包含Request.QueryString及Request.Form

            //4.业务处理过程
            bool bDeal = false;
            Article arSel = new Article(id);
            //4.1.先删除文件
            string files = String.Format("{0}", arSel.AttachFile);
            BP.OA.Main.DeleteFiles_By_StrList(files);
            //4.2.删除数据
            bDeal = arSel.Delete() == 1;
            //填写主要业务逻辑代码
            //5.（用户及业务对象）统计与状态，日志
            //6.返回处理结果或返回其它页面。
            if (bDeal)
            {
                this.BindData(true);
                this.Alert("删除文章成功！");
            }
            else
            {
                this.Alert("删除文章失败！");
            }
        }
        //批量删除处理过程
        protected void DelSel(object sender, EventArgs e)
        {
            //1.验证用户权限
            //2.取得用户变量
            string idList = this.Request.Form["checksel"];
            if (String.IsNullOrEmpty(idList))
            {
                this.Alert(String.Format("没有选择任何要删除的人员"));
                return;
            }
            //3.验证用户变量，包含Request.QueryString及Request.Form

            //4.业务处理过程
            bool bDeal = false;
            //4.1.先删除文件
            String sSql = String.Format("Select AttachFile from OA_Article Where OID in ({0})", idList);
            BP.OA.Main.DeleteFiles_By_Sql(sSql);
            //4.2.再删除数据
            sSql = String.Format("Delete from OA_Article Where OID in ({0})", idList);
            bDeal = BP.DA.DBAccess.RunSQL(sSql) > 0;
            //填写主要业务逻辑代码

            //5.（用户及业务对象）统计与状态,日志
            //7.返回处理结果或返回其它页面。
            if (bDeal)
            {
                //重新绑定数据代码
                this.BindData(true);
                this.Alert("删除文章成功！");
            }
            else
            {
                this.Alert("删除文章失败！");
            }
        }
        //控件事件
        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void Query(object sender, EventArgs e)
        {
            this.BindData(true);
        }
        protected void QueryAll(object sender, EventArgs e)
        {
            this.ddlNewsCatagory.SelectedValue = "0";
            this.tbKeyWords.Text = String.Empty;
            this.BindData(true);
        }
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            this.BindData(false);
        }
        #endregion
    }
}