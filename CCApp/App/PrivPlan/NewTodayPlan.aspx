﻿<%@ Page Title="" Language="C#" MasterPageFile="../../Main/master/Site1.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="NewTodayPlan.aspx.cs" Inherits="CCOA.App.PrivPlan.NewTodayPlan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <script type="text/javascript">
        var theLastIndex = 3;  //至少几个任务，从1开始。
        var theTopIndex = 10;  //至少几个任务，从1开始。
        $(function ($) {
            for (var i = 2; i <= theLastIndex; i++) {
                addTask();
            }
        });
        function addTask() {
           //取得最后一个div_task
            var index = $("#txt_itemCount").val();
            var div = $("#div_task_" + index);
            var html = div.html();            
            //创建一个新的div_task
            var index_New = String(Number(index) + 1);
            var div_New = $("<div id='div_task_"+index_New+"'><div>");
            html = html.replace(/[_][0-9]+/g, "_" + index_New);
            div_New.html(html);
            //添加
            div.after(div_New);
            var chNum=["一","二","三","四","五","六","七","八","九","十"];
            $("#span_no_" + index_New).text('第'+chNum[Number(index_New)-1]+'项');
            //状态
            $("#txt_itemCount").val(index_New);
            refreshButtons();
        }
        function removeTask() {
            //取得最后一个div_task
            var index = $("#txt_itemCount").val();
            var div = $("#div_task_" + index);
            //删除
            div.remove();
            //状态
            var index_New = String(Number(index) - 1);
            $("#txt_itemCount").val(index_New);
            refreshButtons();
        }
        function refreshButtons()
        {
            $(".addTask").hide();
            $(".removeTask").hide();
            var index = $("#txt_itemCount").val();
            if (index < theTopIndex) {
                $("#btn_add_" + index).show();
            }
            if (index > theLastIndex) {
                $("#btn_remove_" + index).show();
            }
        }
        function preSubmit() {
            var planDoc = $("#txtTodayDesc").val();
            if (planDoc.length < 10) { alert("计划简介不能少于10个字！"); return false; }
            var taskCount = $("#txt_itemCount").val();
            if (taskCount == 0) { alert("至少有一个计划项，量化你的任务！"); return false; }
            var hasMain = false;
            var sumHours = 0;
            for(var i=1;i<=taskCount;i++)
            {
                var type = $("#radio_type1_" + String(i)).attr("checked");
                var itemHours = Number($("#select_hours_" + String(i)).val());
                var itemDoc = $("#txt_item_" + String(i)).val();
                if (type=="checked") hasMain = true;
                sumHours += itemHours;
                if (itemDoc.length < 10) {alert("所有计划项内容都不能少于10个字！");  return false; }
                if (type == "checked" && itemHours == 0) { alert("主要任务项必须有一定耗时！"); return false; }
            }
            if (!hasMain) { alert("必须至少有一个主要任务项！"); return false; }
            if (sumHours < 6) { alert("每日任务耗时不能少于6小时！"); return false; }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div>
        <asp:Label runat="server" ID="lblDate"></asp:Label>
    </div>
    <div id="div_mainPlan">
        <fieldset style="padding: 10px 10px 10px 10px">
            <legend>今日计划简介</legend>
            <asp:TextBox runat="server" ID="txtTodayDesc" TextMode="MultiLine" Width="98%" Rows="5"></asp:TextBox>
            <input type="hidden" id="txt_itemCount" name="txt_itemCount" value="1" />
        </fieldset>
        <div id="div_task_1">
            <div style="padding: 5px 5px 1px 5px; margin: 5px 5px 1px 5px">
                <span id="span_no_1">第一项</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" id="radio_type0_1" name="radio_type_1" value="0" />可选任务
                <input type="radio" id="radio_type1_1" name="radio_type_1" value="1" checked="checked" />主要任务 &nbsp;&nbsp;&nbsp;&nbsp;
                耗时:&nbsp;&nbsp;<select id="select_hours_1" name="select_hours_1" style="width: 100px;">
                    <option value="0" selected="selected">不用时间</option>
                    <option value="1">1小时</option>
                    <option value="2">2小时</option>
                    <option value="3">3小时</option>
                    <option value="4">4小时</option>
                    <option value="5">5小时</option>
                    <option value="6">6小时</option>
                    <option value="7">7小时</option>
                    <option value="8">8小时</option>
                </select>&nbsp;&nbsp;&nbsp;&nbsp;<input class="addTask" id="btn_add_1" type="button" value=" + "
                    onclick="addTask()" />
                <input class="removeTask" id="btn_remove_1" type="button" value=" - " onclick="removeTask()" style="display:none;" /></div>
            <div style="padding: 1px 5px 5px 5px; margin: 1px 5px 5px 5px">
                <textarea name="txt_item_1" id="txt_item_1" style="width: 98%" rows="2"></textarea>
            </div>
        </div>        
    </div>
    <div style="text-align: center;">
        <asp:Button runat="server" ID="btnSubmit" OnClick="btnSubmit_Onclick" OnClientClick="return preSubmit()" Text="提交" /></div>
</asp:Content>
