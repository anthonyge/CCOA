﻿using System;
using System.Collections.Generic;
using System.Text;

using BP.DA;
using BP.En;
using System.Data;
using BP.Port;

namespace BP.OA.BBS
{
    //人员信息
    public class BBSUserInfoAttr : EntityNoAttr
    {
        /// <summary>
        /// 人员编号
        /// </summary>
        public const String FK_Emp = "FK_Emp";
        /// <summary>
        /// 头像图片
        /// </summary>
        public const String UserImg = "UserImg";
        /// <summary>
        /// 经验
        /// </summary>
        public const String Exp = "Exp";
        /// <summary>
        /// 积分
        /// </summary>
        public const String Integral = "Integral";
        /// <summary>
        /// 金币
        /// </summary>
        public const String Gold = "Gold";
        /// <summary>
        /// 发布主题数量
        /// </summary>
        public const String ThemeCount = "ThemeCount";
        /// <summary>
        /// 回帖数量
        /// </summary>
        public const String ReplayCount = "ReplayCount";
        /// <summary>
        /// 第一次访问时间
        /// </summary>
        public const String RegDate = "RegDate";
        /// <summary>
        /// 最后访问时间
        /// </summary>
        public const String LastDate = "LastDate";
        /// <summary>
        /// 用户组
        /// </summary>
        public const String UserGroup = "UserGroup";
        /// <summary>
        /// 用户级别
        /// </summary>
        public const String UserLevel = "UserLevel";
        /// <summary>
        /// 用户权限
        /// </summary>
        public const String UserPower = "UserPower";
    }
    /// <summary>
    /// 人员信息
    /// </summary>
    public partial class BBSUserInfo : EntityNo
    {
        #region 属性

        /// <summary>
        /// 人员编号
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(BBSUserInfoAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.FK_Emp, value);
            }
        }
        /// <summary>
        /// 人员名称
        /// </summary>
        public string FK_EmpText
        {
            get
            {
                Emp emp = new Emp(this.FK_Emp);
                return emp.Name;
            }
        }
        /// <summary>
        /// 头像图片
        /// </summary>
        public string UserImg
        {
            get
            {
                return this.GetValStrByKey(BBSUserInfoAttr.UserImg);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.UserImg, value);
            }
        }
        /// <summary>
        /// 经验
        /// </summary>
        public int Exp
        {
            get
            {
                return this.GetValIntByKey(BBSUserInfoAttr.Exp);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.Exp, value);
            }
        }
        /// <summary>
        /// 积分
        /// </summary>
        public int Integral
        {
            get
            {
                return this.GetValIntByKey(BBSUserInfoAttr.Integral);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.Integral, value);
            }
        }
        /// <summary>
        /// 金币
        /// </summary>
        public int Gold
        {
            get
            {
                return this.GetValIntByKey(BBSUserInfoAttr.Gold);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.Gold, value);
            }
        }
        /// <summary>
        /// 发布主题数量
        /// </summary>
        public int ThemeCount
        {
            get
            {
                return this.GetValIntByKey(BBSUserInfoAttr.ThemeCount);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.ThemeCount, value);
            }
        }
        /// <summary>
        /// 回帖数量
        /// </summary>
        public int ReplayCount
        {
            get
            {
                return this.GetValIntByKey(BBSUserInfoAttr.ReplayCount);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.ReplayCount, value);
            }
        }
        /// <summary>
        /// 第一次访问时间
        /// </summary>
        public DateTime RegDate
        {
            get
            {
                return this.GetValDateTime(BBSUserInfoAttr.RegDate);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.RegDate, value);
            }
        }
        /// <summary>
        /// 最后访问时间
        /// </summary>
        public DateTime LastDate
        {
            get
            {
                return this.GetValDateTime(BBSUserInfoAttr.LastDate);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.LastDate, value);
            }
        }
        /// <summary>
        /// 用户组
        /// </summary>
        public int UserGroup
        {
            get
            {
                return this.GetValIntByKey(BBSUserInfoAttr.UserGroup);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.UserGroup, value);
            }
        }
        /// <summary>
        /// 用户级别
        /// </summary>
        public int UserLevel
        {
            get
            {
                return this.GetValIntByKey(BBSUserInfoAttr.UserLevel);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.UserLevel, value);
            }
        }
        /// <summary>
        /// 用户权限
        /// </summary>
        public string UserPower
        {
            get
            {
                return this.GetValStrByKey(BBSUserInfoAttr.UserPower);
            }
            set
            {
                this.SetValByKey(BBSUserInfoAttr.UserPower, value);
            }
        }        
        #endregion

        #region 构造函数
        /// <summary>
        /// 人员信息
        /// </summary>
        public BBSUserInfo() { }
        public BBSUserInfo(string FK_Emp)
        {
            this.FK_Emp = FK_Emp;
            this.RetrieveByAttr(BBSUserInfoAttr.FK_Emp, this.FK_Emp);
        }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_BBSUserInfo";
                map.EnDesc = "论坛版块";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.CodeStruct = "4";
                map.AddTBStringPK(BBSUserInfoAttr.No, null, "编号", true, true, 4, 4, 4);
                map.AddTBString(BBSUserInfoAttr.FK_Emp, null, "人员编号", true, false, 0, 30, 30);
                map.AddTBString(BBSUserInfoAttr.UserImg, null, "头像图片", true, false, 0, 100, 30);
                map.AddTBInt(BBSUserInfoAttr.Exp, 0, "经验", true, false);
                map.AddTBInt(BBSUserInfoAttr.Integral, 0, "积分", true, false);
                map.AddTBInt(BBSUserInfoAttr.Gold, 0, "金币", true, false);
                map.AddTBInt(BBSUserInfoAttr.ThemeCount, 0, "发布主题数量", true, false);
                map.AddTBInt(BBSUserInfoAttr.ReplayCount, 0, "回帖数量", true, false);
                map.AddTBDateTime(BBSUserInfoAttr.RegDate, null, "第一次访问时间", true, false);
                map.AddTBDateTime(BBSUserInfoAttr.LastDate, null, "最后访问时间", true, false);
                map.AddTBInt(BBSUserInfoAttr.UserGroup, 0, "用户组", true, false);
                map.AddTBInt(BBSUserInfoAttr.UserLevel, 0, "用户级别", true, false);
                map.AddTBString(BBSUserInfoAttr.UserImg, null, "用户权限", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///人员信息集合
    /// </summary>
    public class BBSUserInfos : EntitiesNo
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new BBSUserInfo();
            }
        }
        /// <summary>
        /// 人员信息集合
        /// </summary>
        public BBSUserInfos()
        {
        }
    }
}
