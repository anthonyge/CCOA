﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;

using BP.DA;
using BP.En;
using BP.Sys;
using BP.OA.Meeting;
using BP.Tools;


namespace CCOA.App.Meeting
{
    /// <summary>
    /// Ording1 的摘要说明
    /// </summary>
    public class Ording1 : IHttpHandler
    {
        /// <summary>
        /// 封装有关个别 HTTP 请求的所有 HTTP 特定的信息
        /// </summary>
        HttpContext _Context = null;

        public string getUTF8ToString(string param)
        {
            return HttpUtility.UrlDecode(_Context.Request[param], System.Text.Encoding.UTF8);
        }
        public void ProcessRequest(HttpContext context)
        {
            _Context = context;

            if (BP.Web.WebUser.No == null)
                return;

            string method = string.Empty;
            //返回值
            string s_responsetext = string.Empty;
            if (!string.IsNullOrEmpty(_Context.Request["mtd"]))
                method = _Context.Request["mtd"].ToString();

            switch (method)
            {
                case "getmeetingroomlist"://会议室列表
                    s_responsetext = GetMeetingRoomList();
                    break;
                case "getordinglist"://会议预定列表
                    s_responsetext = GetOrdingList();
                    break;
                case "ChangeState"://修改状态
                    s_responsetext = ChangeState();
                    break;
            }
            if (string.IsNullOrEmpty(s_responsetext))
                s_responsetext = "";
            //组装ajax字符串格式,返回调用客户端
            _Context.Response.Charset = "UTF-8";
            _Context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            _Context.Response.ContentType = "text/html";
            _Context.Response.Expires = 0;
            _Context.Response.Write(s_responsetext);
            _Context.Response.End();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 获取会议室列表
        /// </summary>
        /// <returns></returns>
        public string GetMeetingRoomList()
        {
            int totalCount = 0;
            //当前页
            string pageNumber = getUTF8ToString("pageNumber");
            int iPageNumber = string.IsNullOrEmpty(pageNumber) ? 1 : Convert.ToInt32(pageNumber);
            //每页多少行
            string pageSize = getUTF8ToString("pageSize");
            int iPageSize = string.IsNullOrEmpty(pageSize) ? 9999 : Convert.ToInt32(pageSize);

            Rooms Rooms = new Rooms();

            QueryObject obj = new QueryObject(Rooms);
            obj.AddWhere(RoomAttr.RoomSta,"0");
            totalCount = obj.GetCount();
            obj.addOrderByDesc(RoomAttr.No);
            obj.DoQuery(RoomAttr.No, iPageSize, iPageNumber);
            
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(Rooms, totalCount);
        }

        /// <summary>
        /// 获取会议预定信息
        /// </summary>
        /// <returns></returns>
        public string GetOrdingList()
        {
            string FK_RoomNo = getUTF8ToString("FK_RoomNo");
            string curDate = getUTF8ToString("curDate");
            string strSql = "select OID ,DateFrom ,DateTo,Title,a. Fk_Emp,a.Fk_Dept,a.MTResource,a.State,";
            strSql += "b.Name as OrderName,c.Name as RoomName,d.Name as deptName  ,c.Fk_Emp as roomAdmin ,  ";
            strSql += "a.ToEmps,a.ToEmpNames,a.ToStations,a.ToStationNames,a.ToDepts,a.ToDeptNames   ";
            strSql += "from OA_RoomOrding a,Port_Emp b,OA_Room c ,Port_Dept d ";
            strSql += "where a.FK_Emp=b.No and a.FK_Room=c.No and FK_Room='{0}' and a.Fk_Dept=d.No";
            strSql += " and DATEDIFF(day,CONVERT(varchar(100), DateFrom, 23) ,'{1}')>=0";
            strSql += " and DATEDIFF(day,CONVERT(varchar(100), DateTo, 23),'{1}')<=0";
            if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            {
                strSql = "select OID ,DateFrom ,DateTo,Title,a. Fk_Emp,a.Fk_Dept,a.MTResource,a.State,";
                strSql += "b.Name as OrderName,c.Name as RoomName,d.Name as deptName ,c.Fk_Emp  as  roomAdmin  ";
                strSql += "a.ToEmps,a.ToEmpNames,a.ToStations,a.ToStationNames,a.ToDepts,a.ToDeptNames   ";
                strSql += "from OA_RoomOrding a,Port_Emp b,OA_Room c ,Port_Dept d ";
                strSql += "where a.FK_Emp=b.No and a.FK_Room=c.No and FK_Room='{0}' and a.Fk_Dept=d.No";
                strSql += " and DATEDIFF('{1}',DateFrom)>=0 and DATEDIFF(DateTo,'{1}')>=0";
            }

            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(string.Format(strSql, FK_RoomNo, curDate));
            //改变文字
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row["State"] != null && row["State"].ToString() == "3")
                    {
                        MeetingSummary sumary = new MeetingSummary();
                        sumary.RetrieveByAttr(MeetingSummaryAttr.FK_OID, row["OID"]);
                        if (sumary != null && sumary.Content != "")
                            row["State"] = "会议纪要已发";
                        else
                            row["State"] = MyHelper.GetMeetingState(row["DateFrom"], row["DateTo"], row["State"]);
                    }
                    else
                    {
                        row["State"] = MyHelper.GetMeetingState(row["DateFrom"], row["DateTo"], row["State"]);
                    }
                    row["MTResource"] = MyHelper.GetResName(row["MTResource"]);
                }
            }
            return DataTableConvertJson.DataTable2Json(dt, dt.Rows.Count);
        }

        /// <summary>
        /// 修改状态
        /// </summary>
        /// <returns></returns>
        private string ChangeState()
        {
            string oid = getUTF8ToString("OID");
            string state = getUTF8ToString("state");
            string returnVal = "";

            RoomOrding ording = new RoomOrding();
            ording.RetrieveByAttr(RoomOrdingAttr.OID, oid);
            ording.DateTo = BP.OA.Main.CurDateTimeStr;
            ording.State = state;
            int i = ording.Update();
            if (i > 0 && state == "3")
            {
                returnVal = "会议已结束！";
            }
            else if (i > 0 && state == "2")
            {
                returnVal = "会议已取消！";
            }
            return returnVal;
        }
    }
}