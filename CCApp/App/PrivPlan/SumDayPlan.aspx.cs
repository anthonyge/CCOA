﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.OA.PrivPlan;
namespace CCOA.App.PrivPlan
{
    public partial class SumDayPlan : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        private string[] ChineseNo = new String[] { "一", "二", "三", "四", "五", "六", "七", "八", "九", "十" };
        //权限控制
        private string FuncNo = null;
        private DateTime rDate
        {
            get 
            {
                string sDate = Convert.ToString(Request.QueryString["Date"]);
                if (String.IsNullOrEmpty(sDate))
                    return DateTime.Now;
                else
                    return Convert.ToDateTime(sDate);
            }
        }
        public int TheLastIndex = 3;
        public int TheTopIndex = 10;
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
            else
            {
                if (this.Request.Form.Count > 0)
                {
                    //Response.Write(this.Request.Form.Count.ToString());
                    //this.btnSubmit_Onclick(sender, e);
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.lblDate.Text = String.Format("&nbsp;&nbsp;{0:dddd}&nbsp;&nbsp;{0:yyyy年MM月dd日}", this.rDate);  
                this.LoadData();
            }
        }
        private void LoadData()
        {
            BP.OA.PrivPlan.PrivPlan pp = new BP.OA.PrivPlan.PrivPlan();
            bool blnR = pp.RetrieveByAttrAnd(PrivPlanAttr.FK_UserNo, BP.Web.WebUser.No, PrivPlanAttr.PlanDate, String.Format("{0:yyyy-MM-dd}", this.rDate));
            if (blnR)
            {
                this.liTodayDesc.Text = pp.MyDoc;
                string sql = String.Format("select Sort,Task,Hours,Score,Reason,Succeed,Improve,PrivPlanItemType as Type,'{1}' as Checked from OA_PrivPlanItem where FK_PrivPlan='{0}' order by Sort", pp.OID,pp.Checked?"1":"0");
                System.Data.DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);
                this.rptTasks.DataSource = dt;
                this.rptTasks.DataBind();
                this.txt_itemCount.Value = dt.Rows.Count.ToString();
            }
            else
            {
                this.liTodayDesc.Text = "请填写计划简介";
                DataTable dt = new DataTable();
                dt.Columns.Add("Sort");
                dt.Columns.Add("Type");
                dt.Columns.Add("Hours");
                dt.Columns.Add("Task");
                for (int i = 1; i <= this.TheLastIndex; i++)
                {
                    dt.Rows.Add(i, 1, 2, "请填写任务说明！");
                }
                this.rptTasks.DataSource = dt;
                this.rptTasks.DataBind();
                this.txt_itemCount.Value = dt.Rows.Count.ToString();
            }
        }
        public string GetTitleNo(object evalNo)
        {
            int i = Convert.ToInt32(evalNo);
            return String.Format("第{0}项", this.ChineseNo[i - 1]);
        }
        public string GetResultTitle(object evalSucceed, object evalChecked)
        {
            bool ck = Convert.ToInt32(evalChecked)==1;
            int su = Convert.ToInt32(evalSucceed);
            if (!ck)
                return "目标达成";
            else if (su == 0)
                return "失败原因与障碍";
            else
                return "成功方式与方法";
        }
        #endregion

        #region //3.页面事件(Page Event)
        protected void btnSubmit_Onclick(object sender, EventArgs e)
        {
            //0.用户权限限制
            if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            //1.获取用户变量
            int count = Convert.ToInt32(this.txt_itemCount.Value);
            //2.数据验证      
            for (int i = 1; i <= count; i++)
            {
                string reason = this.Request.Form[String.Format("txt_reason_{0}", i)];
                string improve = this.Request.Form[String.Format("txt_improve_{0}", i)];
                if (reason.Length<10||improve.Length<10)
                {
                    BP.OA.Debug.Alert(this, "所有目标达成与方式改进都不能少于10个字！");
                    return;
                }
            }
            //3.数据处理
            BP.OA.PrivPlan.PrivPlan pp = new BP.OA.PrivPlan.PrivPlan();
            bool blnR = pp.RetrieveByAttrAnd(PrivPlanAttr.FK_UserNo, BP.Web.WebUser.No, PrivPlanAttr.PlanDate, String.Format("{0:yyyy-MM-dd}", this.rDate));
            if (!blnR)
            {
                BP.OA.Debug.Alert(this, "没有找到当日计划，出错！");
                return;
            }
            pp.Checked = true;
            pp.CheckTime = DateTime.Now;

            int iR = pp.Update();
            if (iR == 0)
            {
                BP.OA.Debug.Alert(this, "计划总结失败！");
                return;
            }

            for (int i = 1; i <= count; i++)
            {
                string succeed = this.Request.Form[String.Format("hidden_reason_{0}", i)];
                string reason = this.Request.Form[String.Format("txt_reason_{0}", i)];
                string improve = this.Request.Form[String.Format("txt_improve_{0}", i)];
                BP.OA.PrivPlan.PrivPlanItem ppi = new PrivPlanItem();
                bool blnR_ppi = ppi.RetrieveByAttrAnd(PrivPlanItemAttr.FK_PrivPlan, pp.OID, PrivPlanItemAttr.Sort, i);
                if (!blnR_ppi)
                {
                    BP.OA.Debug.Alert(this, "未找到当时计划任务项！");
                    return;
                }
                ppi.Succeed = succeed == "1";
                ppi.Reason = reason;
                ppi.Improve = improve;
                ppi.Update();
            }
            //4.统计/日志
            //5.提交信息
            //BP.OA.Debug.Alert(this, "计划提交成功！");
            //this.LoadData();
            this.Response.Redirect(String.Format("MyPlan.aspx?Date={0:yyyy-MM-dd}",this.rDate));
        }
        protected void lbReturn_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(String.Format("MyPlan.aspx?Date={0:yyyy-MM-dd}", this.rDate));
        }        
        #endregion
    }
}