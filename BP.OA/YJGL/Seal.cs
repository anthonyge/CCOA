﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
using System.Data;

namespace BP.OA.YJGL
{

    public enum SealType
    {
        /// <summary>
        /// 1"@0=主印@2=子印章@3=资质章");
        /// </summary>
        ZY,
        /// <summary>
        /// 子印章
        /// </summary>
        ZYZ,
        /// <summary>
        /// 资质章
        /// </summary>
        ZZZ
    }
   

    /// <summary>
    /// 印鉴属性
    /// </summary>
    public class SealAttr : EntityNoNameAttr
    {
        
        /// <summary>
        /// 印鉴编号
        /// </summary>
        public const string SealID = "SealID";
        /// <summary>
        /// 印鉴名称
        /// </summary>
        public const string SealName = "SealName";
        /// <summary>
        /// 印鉴类型
        /// </summary>
        public const string SealType = "SealType";
        /// <summary>
        /// 管理部门
        /// </summary>
        public const string Unit = "Unit";
        /// <summary>
        /// 保管人
        /// </summary>
        public const string KeepManID = "KeepManID";
        /// <summary>
        /// 经办人
        /// </summary>
        public const string OperatorName = "OperatorName";
        /// <summary>
        /// 登记时间
        /// </summary>
        public const string RegistrationDate = "RegistrationDate";
       /// <summary>
        /// 印鉴状态
       /// </summary>
        public const string SealState = "SealState";
    }

    public class Seal : EntityNoName
    {
        #region  属性
        public string SealName {
            get {
                return this.GetValStringByKey(SealAttr.SealName);
            }
            set{
                this.SetValByKey(SealAttr.SealName,value);
            }
        }
        
        public string SealID
        {
            get
            {
                return this.GetValStrByKey(SealAttr.SealID);
            }
            set
            {
                this.SetValByKey(SealAttr.SealID,value);
            }
        }
        public SealType SealType
        {
            get
            {
                return (SealType)this.GetValIntByKey(SealAttr.SealType);
            }
            set
            {
                this.SetValByKey(SealAttr.SealType, (int)value);
            }
        }
             public string RegistrationDate
        {
            get
            {
                return this.GetValStrByKey(SealAttr.RegistrationDate);
            }
            set
            {
                this.SetValByKey(SealAttr.RegistrationDate,value);
            }
        }
             public string OperatorName
             {
                 get
                 {
                     return this.GetValStrByKey(SealAttr.OperatorName);
                 }
                 set
                 {
                     this.SetValByKey(SealAttr.OperatorName, value);
                 }
             }
        
        #endregion

        #region 权限控制
        #endregion 权限控制

        #region 构造方法
        /// <summary>
        /// 印章
        /// </summary>
        public Seal()
        { }
        /// <summary>
        /// 印章
        /// </summary>
        /// <param name="_No"></param>
        public Seal(string _No) : base(_No) { }
        #endregion 构造方法

        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_YJGLSeal");
                map.EnDesc = "印鉴登记";
                map.IsAutoGenerNo = true;
                map.CodeStruct = "3";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBStringPK(SealAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBStringPK(SealAttr.Name, null, "Name", false, true, 0, 100, 30);
                map.AddDDLSysEnum(SealAttr.SealType, 0, "印鉴类型", true, true, SealAttr.SealType,
                    "@0=公章@2=财务章@3=财务章@3=合同专用章@4=发票专用章@5=其他");
                map.AddTBString(SealAttr.SealName, null, "印鉴名称", true, false, 0, 100, 30, false);
                map.AddTBString(SealAttr.Unit, null, "管理部门", true, false, 0, 100, 30, false);
                map.AddTBString(SealAttr.KeepManID, null, "保管人", true, false, 0, 100, 30, false);

                map.AddTBString(SealAttr.OperatorName, null, "最近使用人", true, false, 0, 100, 30, false);
                map.AddTBDate(SealAttr.RegistrationDate, null, "最近使用时间", true, false);

                //map.AddDDLSysEnum(SealAttr.SealState, 0, "印鉴状态", true, true, SealAttr.SealState,
                //                   "@0=未归还@1=完整@2=损坏");
                //查询条件
              //  map.AddSearchAttr(SealAttr.SealType);

                RefMethod rm = new RefMethod();
                rm.Title = "添加使用信息";
                rm.ClassMethodName = this.ToString() + ".DoUse";
                map.AddRefMethod(rm);
                this._enMap = map;
                return this._enMap;
            }
        }
        /// <summary>
        /// 执行使用.
        /// </summary>
        /// <returns></returns>
        public string DoUse()
        {
            if (BP.OA.YJGL.API.Seal_Use_FlowIsEnable == true)
                BP.Sys.PubClass.WinOpen("/WF/MyFlow.aspx?FK_Flow=" + API.Seal_Use_FlowMark + "&FK_Seal=" + this.No, 500, 600);
            else
                BP.Sys.PubClass.WinOpen("/Comm/UIEn.aspx?EnsName=BP.OA.YJGL.SealUses&FK_Seal=" + this.No, 500, 600);
            return null;
        }

        #region 重写方法
        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {
            return base.beforeUpdate();
        }
        protected override bool beforeUpdateInsertAction()
        {
            this.Name = this.SealName;
            return base.beforeUpdateInsertAction();
        }
        #endregion 重写方法
    }
    public class Seals : SimpleNoNames
    {
        /// <summary>
        /// 卡s
        /// </summary>
        public Seals() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Seal();
            }
        }
    }
}
