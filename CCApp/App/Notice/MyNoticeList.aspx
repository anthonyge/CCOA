﻿<%@ Page Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true" CodeBehind="MyNoticeList.aspx.cs" Inherits="CCOA.App.NOTICE.MyNoticeList" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cph_head">
    <script src="../../js/zDialog/zDialog.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../../CSS/GridviewPager.css" />
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cph_body">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cph_title">
    <div style="padding: 3px 3px 3px 3px;">
        <asp:Literal runat="server" ID="liMyReadState"></asp:Literal>
    </div>
    <asp:GridView ID="GridView1" CssClass="grid" DataKeyNames="OID" Width="98%" runat="server" AutoGenerateColumns="False"
        OnDataBound="GridView1_DataBound">
        <HeaderStyle HorizontalAlign="Left" BackColor="#eeeeee" />
        <Columns>
            <asp:TemplateField HeaderText="标题">
                <ItemTemplate>
                    <img src="<%# GetImportLevel(Eval("ImportLevel")) %>" style="width: 15px; height: 15px;" alt="重要级别" />&nbsp;
                    <a style='<%# GetTitleFormatByReadState(Eval("ReadState")) %>' href="javascript:parent.window.addTab('公告阅读','../App/Notice/NoticeBrowser.aspx?OID=<%# Eval("OID") %>','')">
                        <%# Eval("Title") %></a>
                   <%# Convert.ToInt32(Eval("ReadStateFlag"))==0?"<img alt='新邮件' src='Img/Notice_new.gif'>":"" %>
                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("SetTop")))?"":"<img src='img/top.gif' alt='置顶'"%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="发布者" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <%# GetPublish(Eval("UserName"),Eval("Dept")) %></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="分类" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <%# Eval("NoticeCategory")%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="重要程度" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <%# Eval("Importance")%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="阅读状态" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <%# GetReadState(Eval("ReadState"))%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="发布时间" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<% #GetRelativeTime(Convert.ToDateTime(Eval("RDT"))) %>'
                        ToolTip='<%# Convert.ToDateTime(Eval("RDT")).ToString("yyyy-MM-dd HH:mm:ss")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="发布状态" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<% #Eval("PublishState") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle HorizontalAlign="Center" />
    </asp:GridView>
    <div style="text-align: center; padding-top: 5px;">
        <table cellpadding="0" cellspacing="0" align="center" width="99%" class="border">
        <tr>
            <td align="left" colspan="2">
                <webdiyer:AspNetPager ID="AspNetPager1" CssClass="paginator" CurrentPageButtonClass="cpb"
                    runat="server" AlwaysShow="false" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                     PrevPageText="上一页" ShowCustomInfoSection="Left" ShowInputBox="Never"
                    OnPageChanged="AspNetPager1_PageChanged" CustomInfoTextAlign="Left" LayoutType="Table">
                </webdiyer:AspNetPager>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>
