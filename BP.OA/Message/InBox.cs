﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA.Message
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class InBoxAttr : EntityOIDAttr
    {
        public const String Sender = "Sender";
        public const String FK_ReceiveUserNo = "FK_ReceiveUserNo";
        public const String FK_MsgNo = "FK_MsgNo";
        public const String AddTime = "AddTime";
        public const String ReceiveTime = "ReceiveTime";
        public const String Received = "Received";
        public const String IsCopy = "IsCopy";
        public const String Title = "Title";
        public const String AttachFile = "AttachFile";
        public const String Doc = "Doc";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class InBox : EntityOID
    {
        #region 属性
        public string Sender
        {
            get { return this.GetValStrByKey(InBoxAttr.Sender); }
            set { this.SetValByKey(InBoxAttr.Sender, value); }
        }
        public string FK_ReceiveUserNo
        {
            get { return this.GetValStrByKey(InBoxAttr.FK_ReceiveUserNo); }
            set { this.SetValByKey(InBoxAttr.FK_ReceiveUserNo, value); }
        }
        public int FK_MsgNo
        {
            get { return this.GetValIntByKey(InBoxAttr.FK_MsgNo); }
            set { this.SetValByKey(InBoxAttr.FK_MsgNo, value); }
        }
        public DateTime AddTime
        {
            get { return this.GetValDateTime(InBoxAttr.AddTime); }
            set { this.SetValByKey(InBoxAttr.AddTime, value); }
        }
        public String ReceiveTime
        {
            get { return this.GetValStrByKey(InBoxAttr.ReceiveTime); }
            set { this.SetValByKey(InBoxAttr.ReceiveTime, value); }
        }
        public Boolean Received
        {
            get { return this.GetValBooleanByKey(InBoxAttr.Received); }
            set { this.SetValByKey(InBoxAttr.Received, value); }
        }
        public Boolean IsCopy
        {
            get { return this.GetValBooleanByKey(InBoxAttr.IsCopy); }
            set { this.SetValByKey(InBoxAttr.IsCopy, value); }
        }
        public String AttachFile
        {
            get { return this.GetValStrByKey(InBoxAttr.AttachFile); }
            set { this.SetValByKey(InBoxAttr.AttachFile, value); }
        }
        public string Title
        {
            get { return this.GetValStrByKey(InBoxAttr.Title); }
            set { this.SetValByKey(InBoxAttr.Title, value); }
        }
        public string Doc
        {
            get { return this.GetValStrByKey(InBoxAttr.Doc); }
            set { this.SetValByKey(InBoxAttr.Doc, value); }
        }
        #endregion
        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public InBox() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public InBox(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAllForStation("");
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_MessageInBox";
                map.EnDesc = "新闻文章";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBInt(InBoxAttr.FK_MsgNo, 0, "消息编号", true, false);
                map.AddTBString(InBoxAttr.FK_ReceiveUserNo, null, "接收者", true, false, 2, 50, 20);
                map.AddTBString(InBoxAttr.Sender, null, "发送者", true, false, 2, 50, 20);
                map.AddTBStringDoc(InBoxAttr.Doc, null, "消息内容", true, false);
                map.AddTBDateTime(InBoxAttr.AddTime, null, "添加时间", true, false);
                map.AddTBString(InBoxAttr.ReceiveTime, null, "接收时间", true, false, 0, 20, 30);
                map.AddBoolean(InBoxAttr.Received, false, "是否接收", true, false);
                map.AddBoolean(InBoxAttr.IsCopy, false, "是否抄送", true, false);
                map.AddTBString(InBoxAttr.Title, null, "附件标题", true, false, 0, 2000, 20);
                map.AddTBString(InBoxAttr.AttachFile, null, "附件", true, false, 0, 4000, 50);
                //map.AddTBString(InBoxAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(InBoxAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class InBoxs : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new InBox();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public InBoxs()
        {
        }
    }
}
