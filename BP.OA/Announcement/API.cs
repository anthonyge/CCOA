﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/************************************************************
   Copyright (C), 2005-2014, manstrosoft Co., Ltd.
   FileName: BMGG.cs
   Author:孙成瑞
   Date:2014-5-26
   Description:部门公告
   ***********************************************************/
namespace BP.OA.Announcement
{
    /// <summary>
    /// 公告管理
    /// </summary>
    class API
    {
        #region 部门公告API
        /// <summary>
        /// 是否启用
        /// </summary>
        public static bool RCSWGL_BMGG_FlowIsEnable {
            get {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("RCSWGL_BMGG_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 流程标记
        /// </summary>
        public static string RCSWGL_BMGG_FlowMark
        {
            get
            {
                return "RCSWGLBMGG";
            }
        }
        #endregion
        #region 部门公告管理信息
        public static string RCSWGL_BMGG(string FQR, string SSBM, DateTime FQSJ,string BT, string GGNR, string ISBM,Int64 wordid)
        {
            BMGG en = new BMGG();
            en.CheckPhysicsTable();
            try 
            {
                en.FQR = FQR;
                en.WorkId = wordid;
                en.SSBM = SSBM;
                en.FQSJ = FQSJ.ToString("yyyy-MM-dd HH:mm:ss");
                en.BT = BT;
                en.GGNR = GGNR;
                en.ISBM = ISBM;
                if (ISBM.Equals("0"))
                {
                    en.BMBH = BP.Web.WebUser.FK_Dept;
                }
                else {
                    en.BMBH = "";
                }
                en.WorkId = wordid;
                en.Insert();
                return "部门公告信息添加成功";
            }
            catch (Exception e) {
                en.Delete();
                return "@部门公告信息添加失败:" + e.Message;
            }
        }
        #endregion 
    }
}
