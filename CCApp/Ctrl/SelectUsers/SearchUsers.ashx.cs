﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Data;

using BP.En;
using BP.DA;
using BP.Pub;

namespace CCOA.Ctrl.SelectUsers
{
    /// <summary>
    /// SearchUsers 的摘要说明
    /// </summary>
    public class SearchUsers : IHttpHandler
    {
        private string FuncNo = null;
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            String method = context.Request["method"];
            switch (method)
            {
                case "getdepts":
                    string s_responsetext = string.Empty;
                    DataTable dt_dept = DBAccess.RunSQLReturnTable("select NO,NAME,ParentNo,Idx from port_dept");
                    s_responsetext = GetTreeJsonByTable(dt_dept, "NO", "NAME", "ParentNo", "0");
                    if (string.IsNullOrEmpty(s_responsetext) || s_responsetext == "[]")//如果为空，使用另一种查询
                    {
                        treeResult.Clear();
                        s_responsetext = GetTreeJsonByTable(dt_dept, "NO", "NAME", "ParentNo", "O0");
                    }

                    context.Response.Write(s_responsetext);
                    break;
                case "getusers":
                    //0.用户权限
                    if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
                    //1.获取用户变量
                    String curUserNo = BP.Web.WebUser.No;
                    String deptId = context.Request["DeptId"];
                    bool searchChildDept = Convert.ToBoolean(context.Request["SearchChild"]);
                    String stationId = context.Request["StationId"];
                    String name = Microsoft.JScript.GlobalObject.unescape(context.Request["KeyWord"]);

                    //2.数据验证
                    StringBuilder sbErr = new StringBuilder();

                    //3.数据处理
                    DataTable dt = this.GetSearchedEmps(deptId, searchChildDept, stationId, name);
                    //4.统计/日志
                    //5.提示处理信息
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        BP.OA.UI.DataTableLine2Json dj = new BP.OA.UI.DataTableLine2Json(dt);
                        dj.FormatMode = 2;

                        string json = dj.ToJsonStr(null);
                        context.Response.Write(json);
                    }
                    else
                        context.Response.Write("");
                    break;
            }
        }
        private DataTable GetSearchedEmps(string deptId, bool searchChildDept, string stationId, string name)
        {
            DataTable dt = null;
            if (BP.WF.Glo.OSModel == BP.Sys.OSModel.OneMore)
            {
                dt = BP.OA.GPM.GetSearchedEmps(deptId, searchChildDept, stationId, name);
            }
            else
            {
                dt = BP.OA.GPM.GetSearchedEmpOA(deptId, searchChildDept, stationId, name);
            }
            return dt;
        }

        /// <summary>
        /// 根据DataTable生成Json树结构
        /// </summary>
        /// <param name="tabel">数据源</param>
        /// <param name="idCol">ID列</param>
        /// <param name="txtCol">Text列</param>
        /// <param name="rela">关系字段</param>
        /// <param name="pId">父ID</param>
        ///<returns>easyui tree json格式</returns>
        StringBuilder treeResult = new StringBuilder();
        StringBuilder treesb = new StringBuilder();
        public string GetTreeJsonByTable(DataTable tabel, string idCol, string txtCol, string rela, object pId)
        {
            string treeJson = string.Empty;
            string treeState = "close";
            treeResult.Append(treesb.ToString());

            treesb.Clear();
            if (treeResult.Length == 0)
            {
                treeState = "open";
            }
            if (tabel.Rows.Count > 0)
            {
                treesb.Append("[");
                string filer = string.Empty;
                if (pId.ToString() == "")
                {
                    filer = string.Format("{0} is null", rela);
                }
                else
                {
                    filer = string.Format("{0}='{1}'", rela, pId);
                }
                DataRow[] rows = tabel.Select(filer,"Idx");
                if (rows.Length > 0)
                {
                    foreach (DataRow row in rows)
                    {
                        treesb.Append("{\"id\":\"" + row[idCol]
                                + "\",\"text\":\"" + row[txtCol]
                                + "\",\"state\":\"" + treeState + "\"");


                        if (tabel.Select(string.Format("{0}='{1}'", rela, row[idCol])).Length > 0)
                        {
                            treesb.Append(",\"children\":");
                            GetTreeJsonByTable(tabel, idCol, txtCol, rela, row[idCol]);
                            treeResult.Append(treesb.ToString());
                            treesb.Clear();
                        }
                        treeResult.Append(treesb.ToString());
                        treesb.Clear();
                        treesb.Append("},");
                    }
                    treesb = treesb.Remove(treesb.Length - 1, 1);
                }
                treesb.Append("]");
                treeResult.Append(treesb.ToString());
                treeJson = treeResult.ToString();
                treesb.Clear();
            }
            return treeJson;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}