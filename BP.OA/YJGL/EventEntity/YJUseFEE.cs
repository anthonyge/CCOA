﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;
using BP.WF.Data;

namespace BP.OA.YJGL
{
    /// <summary>
    /// 车辆年检 流程事件实体
    /// </summary>
    public class YJUseFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 车辆年检 流程事件实体
        /// </summary>
        public YJUseFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return API.Seal_Use_FlowMark; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;

           // BP.OA.YJGL.YJUseFEE
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            string deptNo = this.GetValStr(NDXRptBaseAttr.FK_Dept);
            string userNo = this.GetValStr(NDXRptBaseAttr.FlowStarter);
            string seal = this.GetValStr("OA_YJGLSeal");
            string bDT = this.GetValStr("BeginDT");
            string endDT = this.GetValStr("EndDT");
            string note = this.GetValStr("UseReason");
            Int64 workid = this.WorkID;

            API.Seal_Use(seal, deptNo, userNo, bDT, endDT, note, workid);
            return "写入成功....";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}

