﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="NewPlanCatalogContent.aspx.cs" ValidateRequest="false" ClientIDMode="Static"
    Inherits="CCOA.App.PublicPlan.NewPlanCatalogContent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        .doc-table
        {
            border-collapse: collapse;
            border-spacing: 0;
            margin-left: 9px;
            display: table;
            border-color: gray;
            width: 99%;
        }
        .doc-table th
        {
            background: #EEE;
        }
        .doc-table th, .doc-table td
        {
            border: 1px solid #BDDBEF;
            padding: 3px 3px;
        }
        td.title
        {
            width: 120px;
            text-align: right;
        }
        td.title3
        {
            width: 80px;
            text-align: right;
        }
        td.text
        {
            text-align: left;
            padding-left: 10px;
        }
    </style>
    <script src="../../js/js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <script src="../../js/js_EasyUI/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../js/js_EasyUI/plugins/jquery.datebox.js" type="text/javascript"></script>
    <script src="../../js/zDialog/zDialog.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../ctrl/kindeditor/themes/default/default.css" />
    <link rel="stylesheet" href="../../ctrl/kindeditor/plugins/code/prettify.css" />
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/kindeditor.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/lang/zh_CN.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/plugins/code/prettify.js"></script>
    <script type="text/javascript">
//        KindEditor.ready(function (K) {
//            var editor1 = K.create('#ui_TargetContent', {
//                cssPath: '../../ctrl/kindeditor/plugins/code/prettify.css',
//                uploadJson: '../../ctrl/kindeditor/asp.net/upload_json.ashx',
//                fileManagerJson: '../../ctrl/kindeditor/asp.net/file_manager_json.ashx',
//                allowFileManager: true
//            });
//            editor1.sync();
//            prettyPrint();
        //        });
        function Change(ctrl) {
            if (ctrl.value > 100) {
                alert("不能大于100.");
                ctrl.value = 100;
            }
        }
        //检查数字
        function VirtyNum(ctrl, type) {

            if (event.keyCode == 190) {
                if (type == 'int')
                    return false;
                else {
                    if (ctrl.value.indexOf('.') == -1) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
            if (type == 'int') {
                //负号不能有
                if (event.keyCode == 45)
                    return false;
                //不能包含小数点
                if (event.keyCode == 46)
                    return false;
            }

            if (type == 'float') {
                if (ctrl.value.indexOf('.') > 0 && event.keyCode == 46)
                    return false;
                if (ctrl.value.indexOf('-') > 0 && event.keyCode == 45)
                    return false;
            }
            // alert(event.keyCode);
            if (event.keyCode >= 37 && event.keyCode <= 40)
                return true;

            if (event.keyCode >= 96 && event.keyCode <= 105)
                return false;
            if (event.keyCode == 8)
                return true;

            //   alert(event.keyCode);
            var txtval = ctrl.value;
            var key = event.keyCode;
            if ((key < 48 || key > 57) && key != 45 && key != 46) {
                event.keyCode = 0;
            }
            else {
                if (key == 45) {
                    if (txtval.indexOf("-") != -1)
                        event.keyCode = 0;
                }
                if (key == 46) {
                    if (txtval.indexOf(".") != -1 || txtval.length == 0)
                        event.keyCode = 0;
                }
            }
            //非0-9，
            if ((key < 48 || key > 57) && key != 45 && key != 46) {
                event.keyCode = 0;
            }
            //数字0-9
            if (event.keyCode >= 48 && event.keyCode <= 57)
                return true;

            if (event.keyCode == 229)
                return true;

            if (event.keyCode == 8 || event.keyCode == 190)
                return true;

            if (event.keyCode == 13)
                return true;

            if (event.keyCode == 46)
                return true;

            if (event.keyCode == 45)
                return true;
            return false;
        } 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="width: 840px;">
        <table class="doc-table">
            <caption style="text-align: left; font-size: 14px; font-weight: bold;">
                <img src="../../Images/System/RptDir.gif" style="width: 18px; height: 18px;" />反馈执行情况</caption>
            <tr>
                <td class="title">
                    &nbsp;计划名称:
                </td>
                <td class="text" colspan="5">
                    <asp:Label ID="ui_PlanName" runat="server" Width="90%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;上次计划内容:
                </td>
                <td class="text" colspan="5">
                    <div style="width: 97%;height: 30px;margin: 10x 10px 10px 10px; padding: 10px 10px 10px 10px;border: dashed 1px #dddddd;">
                        &nbsp;<asp:Literal runat="server" ID="ui_PerComplate"></asp:Literal>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; margin-left:5px; font-weight:bold;" colspan="6">
                    &nbsp;本次反馈情况如下：
                </td>
            </tr>
            <tr>
                <td class="title">
                    <span style="color: Red;">*</span>&nbsp;开始时间:
                </td>
                <td class="text">
                    &nbsp;<asp:TextBox ID="ui_StartDate" runat="server" CssClass="easyui-datebox"></asp:TextBox>
                </td>
                <td class="title3">
                    <span style="color: Red;">*</span>&nbsp;结束时间:
                </td>
                <td class="text">
                    &nbsp;<asp:TextBox ID="ui_EndDate" runat="server" CssClass="easyui-datebox"></asp:TextBox>
                </td>
                <td class="title3">
                    <span style="color: Red;">*</span>&nbsp;完成进度:
                </td>
                <td class="text" style="width:150px;">
                    &nbsp;<asp:TextBox ID="ui_FinishPercent" runat="server" Width="30px"></asp:TextBox>%
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;<span style="color: Red;">*</span>&nbsp;本阶段完成工作:
                </td>
                <td class="text" colspan="5">
                    &nbsp;<asp:TextBox ID="ui_TargetContent" runat="server" TextMode="MultiLine" Style="width: 98%;
                        height: 100px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;重难点问题:
                </td>
                <td class="text" colspan="5">
                    &nbsp;<asp:TextBox ID="ui_DegreeItems" runat="server" TextMode="MultiLine" Style="width: 98%;
                        height: 100px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;下次计划完成工作:
                </td>
                <td class="text" colspan="5">
                    &nbsp;<asp:TextBox ID="ui_NextComplate" runat="server" TextMode="MultiLine" Style="width: 98%;
                        height: 100px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center;">
                    <asp:Button runat="server" Text="保存" ID="btnSubmit" onclick="btnSubmit_Click" />        
                </td>
            </tr>
        </table>
        <div style="text-align: center; margin-top: 5px;">
            <asp:HiddenField ID="ui_PK_NO" runat="server"></asp:HiddenField>
        </div>
    </div>
</asp:Content>
