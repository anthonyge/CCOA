﻿using BP.En;
using BP.Port;
using BP.Sys;
using BP.WebLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using BP.Tools;
namespace CCOA.App.WebLogs
{
    /// <summary>
    /// WebLoagsSevice 的摘要说明
    /// </summary>
    public class WebLoagsSevice : IHttpHandler
    {
        /// <summary>
        /// 封装有关个别 HTTP 请求的所有 HTTP 特定的信息
        /// </summary>
        HttpContext _Context = null;
        public string getUTF8ToString(string param)
        {
            return HttpUtility.UrlDecode(_Context.Request[param], System.Text.Encoding.UTF8);
        }
        public void ProcessRequest(HttpContext context)
        {
            _Context = context;
            string mtd = string.Empty;
            //返回值
            string s_responsetext = string.Empty;
            if (!string.IsNullOrEmpty(_Context.Request["mtd"]))
                mtd = _Context.Request["mtd"].ToString();

            switch (mtd)
            {
                case "getnewcatagory"://获取日志类别
                    s_responsetext = GetNewCatagory();
                    break;
                case "getnewslist"://获取日志列表
                    s_responsetext = GetNewsList();
                    break;
                case "del":
                    Del();
                    break;
                case "add":
                    Add();
                    break;
                case "edit":
                    Edit();
                    break;
               
            }
            if (string.IsNullOrEmpty(s_responsetext))
                s_responsetext = "";
            //组装ajax字符串格式,返回调用客户端
            _Context.Response.Charset = "UTF-8";
            _Context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            _Context.Response.ContentType = "text/html";
            _Context.Response.Expires = 0;
            _Context.Response.Write(s_responsetext);
            _Context.Response.End();
        }
        /// <summary>
        /// 获取日志类别
        /// </summary>
        /// <returns></returns>
        private string GetNewCatagory()
        {
            LogTypes catagorys = new LogTypes();
            catagorys.RetrieveAll();
            return Entitis2Json.ConvertEntities2ListJson(catagorys);
        }
        /// <summary>
        /// 获取日志列表
        /// </summary>
        /// <returns></returns>
        public string GetNewsList()
        {
            int totalCount = 0;
            string keyWords = getUTF8ToString("keyWords");
            string cataValue = getUTF8ToString("newsCatagory");
            string fromdate = getUTF8ToString("fromdate");
            string todate = getUTF8ToString("todate");
            string currentUser =BP.Web.WebUser.No;
            //当前页
            string pageNumber = getUTF8ToString("pageNumber");
            int iPageNumber = string.IsNullOrEmpty(pageNumber) ? 1 : Convert.ToInt32(pageNumber);
            //每页多少行
            string pageSize = getUTF8ToString("pageSize");
            int iPageSize = string.IsNullOrEmpty(pageSize) ? 9999 : Convert.ToInt32(pageSize);

            DRSheets DRSheets = new DRSheets();
            QueryObject obj = new QueryObject(DRSheets);
            obj.AddWhere("1=1");
            if (fromdate != ""&&todate!="")
            {
                obj.AddWhere(String.Format(" and ( RDT between '{0}' and '{1}' )", fromdate,todate));
            }
            if (cataValue != "0")
            {
                obj.AddWhere(String.Format(" and DRType='{0}'", cataValue));
            }

            if (!String.IsNullOrEmpty(keyWords))
            {
                obj.AddWhere(String.Format(" and Doc LIKE '%{0}%'", keyWords));
            }
            if (!String.IsNullOrEmpty(currentUser))
            {
                obj.AddWhere(String.Format(" and Rec ='{0}' ", currentUser));
            }
            totalCount = obj.GetCount();
            obj.addOrderByDesc( DRSheetAttr.RDT);
            obj.DoQuery(DRSheetAttr.OID, iPageSize, iPageNumber);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(DRSheets, totalCount);
        }
        //删除的方法
        private void Del()
        {
            //获取到选中行的id
            string id = HttpContext.Current.Request["id"];
            string sSql = String.Format("Delete FROM ZZ_DRSheet where (OID in ({0}))", id);
            int count = BP.DA.DBAccess.RunSQL(sSql);
            if (count > 0)
            {
                HttpContext.Current.Response.Write("共删除了" + count + "条数据");
            }
            else
            {
                HttpContext.Current.Response.Write("error");
            }
        }
        //添加
        private void Add()
        {
            DRSheet dt = new DRSheet();
            StringBuilder sb = new StringBuilder();
            //遍历获取传递过来的字符串
            foreach (string s in HttpContext.Current.Request.Form.AllKeys)
            {
                sb.AppendFormat("{0}:{1}\n", s, HttpContext.Current.Request.Form[s]);
            }
            string ss = sb.ToString();
            string[] str = ss.Split('&');
            string logType = str[1].Split('=')[1];//从第二个元素开始
            string date = str[2].Split('=')[1];
            string Doc = str[3].Split('=')[1];
            dt.Rec = BP.Web.WebUser.No; dt.RDT = DateTime.Now.ToString("yyyy-MM-dd"); dt.DRType = logType; dt.Doc = Doc;
            BP.GPM.DeptEmps ds = new BP.GPM.DeptEmps();
            BP.GPM.DeptEmp ed = (BP.GPM.DeptEmp)ds.Filter("FK_Emp", BP.Web.WebUser.No);
            dt.date = date;
            if (ed!=null)
            {
                 dt.FK_Dept = ed.FK_Dept;
            }
            
            int count = dt.Insert();
            if (count>0)
            {
                HttpContext.Current.Response.Write("添加成功！");
            }
            else
            {
                HttpContext.Current.Response.Write("添加失败！");
            }
        }
        /// <summary>
        /// 编辑
        /// </summary>
        private void Edit()
        {
            StringBuilder sb = new StringBuilder();
            //遍历获取传递过来的字符串
            foreach (string s in HttpContext.Current.Request.Form.AllKeys)
            {
                sb.AppendFormat("{0}:{1}\n", s, HttpContext.Current.Request.Form[s]);
            }
            string ss = sb.ToString();
            string[] str = ss.Split('&');
            string id = str[0].Split('=')[1];
            string logType = str[1].Split('=')[1];
            string date = str[2].Split('=')[1];
            string Doc = str[3].Split('=')[1];
            DRSheet dt = new DRSheet(int.Parse(id));
            dt.Rec = BP.Web.WebUser.No;  dt.DRType = logType; dt.Doc = Doc;
            dt.date = date;
            if (date != DateTime.Now.ToString("yyyy-MM-dd"))
            {
                dt.DRSta = "1";
            }
            else 
            {
                dt.DRSta = "0";
            }
            dt.RDT = DateTime.Now.ToString("yyyy-MM-dd");
            int count = dt.Update();
            if (count > 0)
            {
                HttpContext.Current.Response.Write("修改成功！");
            }
            else
            {
                HttpContext.Current.Response.Write("修改失败！");
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}