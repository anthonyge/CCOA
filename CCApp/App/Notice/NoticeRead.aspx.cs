﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.NOTICE
{
    public partial class NoticeRead : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        private int rNoticeID
        {
            get
            {
                return Convert.ToInt32(Request.QueryString["OID"]);
            }
        }
        private System.Data.DataTable GetNoticeByNo(int oid)
        {
            string sql = String.Format("select FK_UserNo,FK_NoticeNo,AddTime,ReadTime,AdviceValue,AdviceRemark from OA_NoticeReader where FK_NoticeNo={0}", oid);
            return BP.DA.DBAccess.RunSQLReturnTable(sql);
        }
        private String NotReadStateFormat = "<span style='color:red'>未读</span>";
        private String ReadStateFormat = "<span style='color:green'>已读</span>";
        public string GetUserNames(object userNos)
        {
            String users = String.Format("{0}", userNos);
            return BP.OA.GPM.GetUserNames(users);
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadData();
            }
        }
        private void LoadData()
        {
            System.Data.DataTable dt = this.GetNoticeByNo(this.rNoticeID);
            this.GridView1.DataSource = dt;
            this.GridView1.DataBind();
        }
        public string GetReadState(object evalAddTime,object evalReadTime)
        {
            if (evalReadTime == Convert.DBNull || evalReadTime == null || String.IsNullOrEmpty(Convert.ToString(evalReadTime)))
            {
                return this.NotReadStateFormat;
            }
            else
            {
                return this.ReadStateFormat;
            }
        }
        #endregion

        #region //3.页面事件(Page Event)

        #endregion
    }
}