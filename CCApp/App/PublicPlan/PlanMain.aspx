﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/AppMaster/AppSite.Master" AutoEventWireup="true"
    CodeBehind="PlanMain.aspx.cs" Inherits="CCOA.App.PublicPlan.PlanMain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/AppData.js" type="text/javascript"></script>
    <script src="js/PlanMain.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="pageloading">
    </div>
    <div data-options="region:'center'" border="false" style="margin: 0; padding: 0;overflow: hidden;">
        <div id="tb" style="padding: 5px; height: 22px;">
            <div style="float: left; margin-left: 5px;">
                筛选：<input type="text" id="TB_KeyWord" />
            </div>
            <div style="float:left;">
                <input type="checkbox" id="CB_Catalog" onclick="LoadGrid()" style="vertical-align:middle;" />
                <label for="CB_Catalog" style="vertical-align:middle;">显示阶段</label>
            </div>
            <div class="datagrid-btn-separator"></div>
            <a href="javascript:void(0)" id="mb1" style="float: left;" class="easyui-menubutton" data-options="menu:'#mm',iconCls:'icon-edit'">功能操作</a>
            <div class="datagrid-btn-separator"></div>
            <a id="btnBackPrePlan" style="float: left;" href="#" onclick="BackPrePlan()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reset'">返回上一层</a> 
            <a id="btnSetTop" style="float: left;" href="#" onclick="SetToTopView()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-new'">置顶显示</a> 
            <div class="datagrid-btn-separator"></div>
            <a id="btnTranUp" style="float: left;" href="#" onclick="TranUp()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-up'">上移</a> 
            <a id="btnTranDown" style="float: left;" href="#" onclick="TranDown()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-down'">下移</a> 
        </div>
        <table id="planMianGrid" fit="true" fitcolumns="true" toolbar="#tb" class="easyui-datagrid"></table>
        <div id="mm" class="easyui-menu" style="width: 120px;">
            <div onclick="BackPrePlan()" data-options="iconCls:'icon-reset'">返回上一层</div>
            <div onclick="SetToTopView()" data-options="iconCls:'icon-new'">置顶显示</div>
            <div class="menu-sep"></div> 
            <div onclick="EditPlanTick()" data-options="iconCls:'icon-mulu'">反馈情况</div>
            <div class="menu-sep"></div> 
            <div data-options="iconCls:'icon-add'">
                <span>新建</span>
                <div>
                    <div onclick="AddSampleNode()" data-options="iconCls:'icon-addfolder'">同级计划</div>
                    <div onclick="AddChildNode()" data-options="iconCls:'icon-addfile'">下级计划</div>
                    <div onclick="AddStageNode()" data-options="iconCls:'icon-icon_Backlog'">阶段</div>
                </div>
            </div>
            <div onclick="EditNode()" data-options="iconCls:'icon-edit'">编辑</div>
            <div onclick="DelNode()" data-options="iconCls:'icon-cancel'">删除</div>
            <div data-options="iconCls:'icon-ok'">
                <span>查看</span>
                <div>
                    <div onclick="ViewPlanBisc()">基本信息</div>
                    <div onclick="EditPlanTick()">反馈信息</div>
                    <div onclick="PlanProgress()">进度监控</div>
                </div>
            </div>
            <div class="menu-sep"></div> 
            <div>
                <span>状态控制</span>
                <div>
		            <div onclick="StartUpPlan()">启动</div>
		            <div onclick="ShutUpPlan()">结束</div>
                </div>
            </div>
            <div class="menu-sep"></div> 
            <div onclick="TranUp()" data-options="iconCls:'icon-up'">上移</div>
            <div onclick="TranDown()" data-options="iconCls:'icon-down'">下移</div>
        </div>
    </div>
    <input type="hidden" id="curRootNo" />
    <input type="hidden" id="curTreeNo" />
    <input type="hidden" id="curPerson" />
</asp:Content>
