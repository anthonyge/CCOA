﻿<%@ Page Title="" Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true" CodeBehind="Start.aspx.cs" Inherits="CCOA.App.PrivPlan.Start" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
<style type="text/css">
      body{ font-size:13px;}
      p{font-size:15px;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="margin: 30px 30px 30px 30px;">
        <p>
            <span style="font-size: 18px; font-weight: bold; color: deepblue;">尊敬的员工！</span></p>
        <p>
            欢迎你来到我们公司。你还没有使用过计划功能，请开启这个应用，随便了解下个人计划功能。
        </p>
        <p>
            1、每天早上你都必须告诉自己，你准备做什么。计划代表每个人的意志，很多情况下，我们不能完成它，但并不代表我们的意志不重要，它比实际的成果更重要。
        </p>
        <p>
            2、每天晚上你都必须告诉自己，你做了什么。总结自己很重要。如果今天没有做好，请找出原因与障碍；如果我们做好了，请分析下方式与方法。最后尽力去想想有什么需要改进的。
        </p>
        <p>
            3、如果一天没有计划，则今天则直接扣5分。
        </p>
        <p>
            4、一天可以制定多个任务，一个任务如果成功，则其分数为其准备用小时数。
        </p>
        <p>
            5、任务分为主要任务与可选任务。可选任务永不扣分。主要任务未完成，扣相应的分，可选任务不得分；主要任务完成，得相应的分，可选任务如果完成，才能得分。
        </p>
        <p>
            6、此分每天8-12分，如果每月能达300以上，说明自己状态特别棒！只供个人参考，不供公司考勤使用！万请注意。
        </p>
        <p style="text-align:center;"><asp:Button runat="server" ID="btnStart" OnClick="StartPrivPlan"  Text="开启我的计划" /></p>
    </div>
</asp:Content>
