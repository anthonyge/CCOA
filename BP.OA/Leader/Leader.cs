using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA
{
    /// <summary>
    /// 领导属性
    /// </summary>
    public class LeaderAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 状态
        /// </summary>
        public const string XB = "XB";
        /// <summary>
        /// 年龄
        /// </summary>
        public const string Age = "Age";
        /// <summary>
        /// 简历
        /// </summary>
        public const string JianLi = "JianLi";
        /// <summary>
        /// 职务
        /// </summary>
        public const string Duty = "Duty";
      
    }
    
    /// <summary>
    ///  领导
    /// </summary>
    public class Leader : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 职务
        /// </summary>
        public string Duty
        {
            get
            {
                return this.GetValStrByKey(LeaderAttr.Duty);
            }
            set
            {
                this.SetValByKey(LeaderAttr.Duty, value);
            }
        }
        /// <summary>
        /// 简历
        /// </summary>
        public string JianLi
        {
            get
            {
                return this.GetValStrByKey(LeaderAttr.JianLi);
            }
            set
            {
                this.SetValByKey(LeaderAttr.JianLi, value);
            }
        }
        /// <summary>
        /// 性别
        /// </summary>
        public int XB
        {
            get
            {
                return this.GetValIntByKey(LeaderAttr.XB);
            }
            set
            {
                this.SetValByKey(LeaderAttr.XB, value);
            }
        }
        /// <summary>
        /// 性别Text
        /// </summary>
        public string XBText
        {
            get
            {
                return this.GetValRefTextByKey(LeaderAttr.XB);
            }
        }
        /// <summary>
        /// 年龄
        /// </summary>
        public int Age
        {
            get
            {
                return this.GetValIntByKey(LeaderAttr.Age);
            }
            set
            {
                this.SetValByKey(LeaderAttr.Age, value);
            }
        }
        #endregion 属性
        #region 附件属性
        /// <summary>
        /// 附件路径
        /// </summary>
        public string WebPath
        {
            get
            {
                string str = this.GetValStringByKey("WebPath");
                str = str.Replace("//", "/");
                return str;
            }
        }
        #endregion 附件属性。
        #region 权限控制属性.
        #endregion 权限控制属性.
        #region 构造方法
        /// <summary>
        /// 领导
        /// </summary>
        public Leader()
        {
        }
        /// <summary>
        /// 领导
        /// </summary>
        /// <param name="_No"></param>
        public Leader(string _No) : base(_No) { }
        #endregion
        /// <summary>
        /// 领导Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_Leader");
                map.EnDesc = "领导";
                map.CodeStruct = "3";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.AddTBStringPK(LeaderAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBString(LeaderAttr.Name, null, "名称", true, false, 1, 100, 30, false);
                map.AddTBString(LeaderAttr.Duty, null, "职务", true, false, 0, 100, 30, false);
                map.AddDDLSysEnum(LeaderAttr.XB, 0, "性别", true, true, LeaderAttr.XB,
                    "@0=女@1=男");
                map.AddTBInt(LeaderAttr.Age, 30, "年龄", true, false);
                map.AddTBStringDoc(LeaderAttr.JianLi, null, "简历", true, false, true);
                map.AddMyFile("图片");
                map.AddDtl(new LeaderDtls(), LeaderDtlAttr.FK_Leader);
                RefMethod rm = new RefMethod();
                rm.Title = "增加活动";
                rm.ClassMethodName = this.ToString() + ".DoAddAction";
                rm.HisAttrs.AddTBString("Title", null, "活动名称", true, false, 2, 100, 10);
                rm.HisAttrs.AddTBDateTime("DTFrom", null, "从", true, false);
                rm.HisAttrs.AddTBDateTime("DTTo", null, "到", true, false);
                map.AddRefMethod(rm);
                //查询条件
                map.AddSearchAttr(LeaderAttr.XB);
                this._enMap = map;
                return this._enMap;
            }
        }
        //public string DoActivityMange()
        //{
        //    PubClass.WinOpen("Comm/RefFunc/UIEn.aspx?EnsName=BP.OA.Leaders");
        //    return null;
        //}
        /// <summary>
        /// 增加一个活动
        /// </summary>
        /// <param name="title"></param>
        /// <param name="dtfrom"></param>
        /// <param name="dtto"></param>
        /// <returns></returns>
        public string DoAddAction(string title, string dtfrom, string dtto)
        {
            LeaderDtl dtl = new LeaderDtl();
            dtl.FK_Leader = this.No;
            dtl.Title = title;
            dtl.DTFrom = dtfrom;
            dtl.DTTo = dtto;
            dtl.Insert();
            return "给（" + this.No + " - " + this.Name + "）增加活动成功";
        }
        //public override UAC HisUAC
        //{
        //    get
        //    {
        //        UAC uac = new UAC();
        //        uac.OpenForSysAdmin();
        //        return uac;
        //    }
        //}
    }
    /// <summary>
    /// 领导s
    /// </summary>
    public class Leaders : EntitiesNoName
    {
        /// <summary>
        /// 领导s
        /// </summary>
        public Leaders() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Leader();
            }
        }
    }
}
