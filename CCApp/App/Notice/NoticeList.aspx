﻿<%@ Page Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="NoticeList.aspx.cs" Inherits="CCOA.App.NOTICE.NoticeList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cph_head">
    <script src="../../js/zDialog/zDialog.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../../js/js_EasyUI/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../../js/js_EasyUI/themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="../../CSS/GridviewPager.css" />
    <script type="text/javascript" src="../../js/js_EasyUI/jquery.easyui.min.js"></script>
    <script src="../../js/js_EasyUI/plugins/jquery.menubutton.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cph_title">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cph_body">
    <div style="padding: 3px 3px 3px 3px;">
        请选择：<asp:RadioButtonList runat="server" ID="rbl_Category" AutoPostBack="true" RepeatLayout="Flow"
            RepeatDirection="Horizontal" OnSelectedIndexChanged="rbl_Category_SelectedIndexChanged">
        </asp:RadioButtonList>
    </div>
    <asp:GridView ID="GridView1" CssClass="grid" DataKeyNames="OID" Width="98%" runat="server"
        AutoGenerateColumns="False" OnDataBound="GridView1_DataBound">
        <HeaderStyle HorizontalAlign="Left" BackColor="#eeeeee" />
        <Columns>
            <asp:TemplateField HeaderText="标题">
                <ItemTemplate>
                    <img src="<%# GetImportLevel(Eval("ImportLevel")) %>" style="width: 15px; height: 15px;" />&nbsp;<a
                        style='<%# GetTitleFormatByState(Eval("PublishStateFlag")) %>' href="javascript:zDialog_open0('NoticeBrowser.aspx?OID=<%# Eval("OID") %>','公告',600,600,false)"><%# Eval("Title") %></a>
                    <%# BP.OA.Main.GetTimeImg(Eval("RDT"),2,"新公告","img/Notice_New.gif",0,0)%>
                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("SetTop")))?"":"<img src='img/top.gif' alt='置顶'"%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="发布者" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <%# GetPublish(Eval("UserName"),Eval("Dept")) %></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="分类" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <%# Eval("NoticeCategory")%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="重要程度" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <%# Eval("Importance")%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="阅读状态" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <a href="javascript:zDialog_open0('NoticeRead.aspx?OID=<%# Eval("OID") %>','公告阅读情况',600,600,false)">
                        已读(<%# Eval("ReadState") %>)</a></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="发布时间" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<% #GetRelativeTime(Convert.ToDateTime(Eval("RDT"))) %>'
                        ToolTip='<%# Convert.ToDateTime(Eval("RDT")).ToString("yyyy-MM-dd HH:mm:ss")%>'></asp:Label>&nbsp;
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="发布状态" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<% #Eval("PublishState") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False" HeaderText="操作" ItemStyle-Width="190px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <a href="NoticeEdit.aspx?OID=<%# Eval("OID") %>">编辑</a>
                    <asp:Literal ID="Literal2" runat="server">&nbsp;|&nbsp;</asp:Literal><asp:LinkButton
                        ID="LinkButton2" runat="server" CausesValidation="False" OnClick="ChangeState"
                        CommandName='<% #Eval("OID") %>' Text='<%#Eval("NoticeStaTitle") %>' />
                    &nbsp;|&nbsp; <a href="javascript:void(0)" class="easyui-menubutton" menu="#mm_<%#Eval("OID") %>">
                        更多</a>
                    <div id="mm_<%#Eval("OID") %>" style="width: 120px; display: none;">
                        <div iconcls="icon-edit">
                            <a href="NoticeEdit.aspx?OID=<%# Eval("OID") %>">编辑</a>
                        </div>
                        <div iconcls="icon-delete">
                            <asp:LinkButton runat="server" CausesValidation="False" OnClick="Del" OnClientClick="return confirm('是否真的要删除这条公告吗？');"
                                CommandName='<% #Eval("OID") %>' Text="删除" /></div>
                        <div>
                            <asp:LinkButton runat="server" CausesValidation="False" OnClick="SetTop" CommandName='<% #Eval("OID") %>'
                                Text=' 置顶' ToolTip='<%#Eval("SetTop") %>' /></div>
                        <div>
                            <asp:LinkButton runat="server" CausesValidation="False" OnClick="CancelTop" Enabled='<%# !String.IsNullOrEmpty(Convert.ToString(Eval("SetTop"))) %>'
                                CommandName='<% #Eval("OID") %>' Text=' 取消置顶' ToolTip='<%#Eval("SetTop") %>' /></div>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle HorizontalAlign="Center" />
    </asp:GridView>
    <table cellpadding="0" cellspacing="0" align="center" width="99%" class="border">
        <tr>
            <td align="left" colspan="2">
                <webdiyer:AspNetPager ID="AspNetPager1" CssClass="paginator" CurrentPageButtonClass="cpb"
                    runat="server" AlwaysShow="false" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                     PrevPageText="上一页" ShowCustomInfoSection="Left" ShowInputBox="Never"
                    OnPageChanged="AspNetPager1_PageChanged" CustomInfoTextAlign="Left" LayoutType="Table">
                </webdiyer:AspNetPager>
            </td>
        </tr>
    </table>
</asp:Content>
