using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA
{
    /// <summary>
    /// 员工属性
    /// </summary>
    public class ALEmpAttr : BP.En.EntityNoNameAttr
    {
        #region 基本属性
        /// <summary>
        /// 部门
        /// </summary>
        public const string Dept = "Dept";
        /// <summary>
        /// 手机号码
        /// </summary>
        public const string Tel = "Tel";
        /// <summary>
        /// 电子邮箱
        /// </summary>
        public const string Email = "Email";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Remarks = "Remarks";
        /// <summary>
        /// 住址
        /// </summary>
        public const string Address = "Address";
        /// <summary>
        /// 添加人
        /// </summary>
        public const string FK_Emp = "FK_Emp";
        /// <summary>
        /// 传真号
        /// </summary>
        public const string FaxNo = "FaxNo";
        /// <summary>
        /// 生日
        /// </summary>
        public const string Birthday = "Birthday ";
        /// <summary>
        /// 个人主页
        /// </summary>
        public const string MyHomePage = "MyHomePage";
        /// <summary>
        /// 组织
        /// </summary>
        public const string Organization = "Organization";
        /// <summary>
        /// 职位
        /// </summary>
        public const string PosiTion = "PosiTion";
        /// <summary>
        /// 角色
        /// </summary>
        public const string Role = "Role";
        #endregion
    }

    /// <summary>
    /// 员工 的摘要说明。
    /// </summary>
    public class ALEmp : EntityNoName
    {
        #region 扩展属性

        /// <summary>
        /// 添加人
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(ALEmpAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(ALEmpAttr.FK_Emp, value);
            }
        }
        /// <summary>
        /// 组织
        /// </summary>
        public string Organization
        {
            get { return this.GetValStrByKey(ALEmpAttr.Organization); }
            set { this.SetValByKey(ALEmpAttr.Organization, value); }
        }
        /// <summary>
        /// 职位
        /// </summary>
        public string PosiTion
        {
            get { return this.GetValStrByKey(ALEmpAttr.PosiTion); }
            set { this.SetValByKey(ALEmpAttr.PosiTion, value); }
        }
        /// <summary>
        /// 角色
        /// </summary>
        public string Role
        {
            get { return this.GetValStrByKey(ALEmpAttr.Role); }
            set { this.SetValByKey(ALEmpAttr.Role, value); }
        }
        /// <summary>
        /// 传真号码
        /// </summary>
        public string faxNo
        {
            get { return this.GetValStrByKey(ALEmpAttr.FaxNo); }
            set { this.SetValByKey(ALEmpAttr.FaxNo, value); }
        }
        /// <summary>
        /// 住址
        /// </summary>
        public string Address
        {
            get { return this.GetValStrByKey(ALEmpAttr.Address); }
            set { this.SetValByKey(ALEmpAttr.Address, value); }
        }
        /// <summary>
        /// 生日
        /// </summary>
        public string Birthday
        {
            get { return this.GetValStrByKey(ALEmpAttr.Birthday); }
            set { this.SetValByKey(ALEmpAttr.Birthday, value); }
        }
        /// <summary>
        /// 个人主页
        /// </summary>
        public string MyHomePage
        {
            get { return this.GetValStrByKey(ALEmpAttr.MyHomePage); }
            set { this.SetValByKey(ALEmpAttr.MyHomePage, value); }
        }
        /// <summary>
        /// 部门
        /// </summary>
        public string Dept
        {
            get { return this.GetValStrByKey(ALEmpAttr.Dept); }
            set { this.SetValByKey(ALEmpAttr.Dept, value); }
        }
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Tel
        {
            get { return this.GetValStrByKey(ALEmpAttr.Tel); }
            set { this.SetValByKey(ALEmpAttr.Tel, value); }
        }
        /// <summary>
        /// 电子邮箱
        /// </summary>
        public string Email
        {
            get { return this.GetValStrByKey(ALEmpAttr.Email); }
            set { this.SetValByKey(ALEmpAttr.Email, value); }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks
        {
            get { return this.GetValStrByKey(ALEmpAttr.Remarks); }
            set { this.SetValByKey(ALEmpAttr.Remarks, value); }
        }
        #endregion

        #region 公共方法

        #endregion 公共方法

        #region 构造函数
        /// <summary>
        /// 员工
        /// </summary>
        public ALEmp()
        {
        }
        /// <summary>
        /// 员工
        /// </summary>
        /// <param name="no">编号</param>
        public ALEmp(string no)
        {
            this.No = no.Trim();
            if (this.No.Length == 0)
                throw new Exception("@要查询的员工编号为空。");
            try
            {
                this.Retrieve();
            }
            catch (Exception ex)
            {
                int i = this.RetrieveFromDBSources();
                if (i == 0)
                    throw new Exception("@用户或者密码错误：[" + no + "]，或者帐号被停用。@技术信息(从内存中查询出现错误)：ex1=" + ex.Message);
            }
        }

        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                //uac.OpenForSysAdmin();
                uac.IsUpdate = true;
                uac.IsDelete = true;
                uac.IsInsert = true;
                uac.IsView = true;
                return uac;

            }


        }


        /// <summary>
        /// 重写基类方法
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();

                #region 基本属性
                //map.EnDBUrl =
                //    new DBUrl(DBUrlType.AppCenterDSN); //要连接的数据源（表示要连接到的那个系统数据库）。
                map.PhysicsTable = "OA_ALEmp";
                map.DepositaryOfMap = Depositary.Application;    //实体map的存放位置.
                map.DepositaryOfEntity = Depositary.Application; //实体存放位置
                map.EnDesc = "个人通讯录"; // "用户"; // 实体的描述.
                map.CodeStruct = "1";
                map.IsAutoGenerNo = true;
                #endregion

                #region 字段
                /*关于字段属性的增加 */
                map.AddTBString(ALEmpAttr.FK_Emp, BP.Web.WebUser.No, "添加人", false, true, 0, 20, 20);
                map.AddTBStringPK(ALEmpAttr.No, null, "编号", false, true, 1, 50, 50);
                map.AddTBString(ALEmpAttr.Name, null, "姓名", true, false, 0, 100, 30);
                map.AddTBString(ALEmpAttr.Email, null, "电子邮箱", true, false, 0, 100, 132);
                map.AddTBString(ALEmpAttr.Tel, null, "手机号码", true, false, 0, 20, 130);
                map.AddTBString(ALEmpAttr.FaxNo, null, "传真号码", true, false, 0, 20, 130);
                map.AddTBString(ALEmpAttr.Address, null, "居住地址", true, false, 0, 20, 130);
                map.AddTBDate(ALEmpAttr.Birthday, null, "生日", true, false);
                map.AddTBString(ALEmpAttr.MyHomePage, null, "个人主页", true, false, 0, 20, 130);
                map.AddTBString(ALEmpAttr.Organization, null, "组织", true, false, 0, 20, 130);
                map.AddTBString(ALEmpAttr.Dept, null, "部门", true, false, 0, 20, 130);
                map.AddTBString(ALEmpAttr.PosiTion, null, "职位", true, false, 0, 20, 130);
                map.AddTBString(ALEmpAttr.Role, null, "角色", true, false, 0, 20, 130);
                map.AddTBStringDoc(ALEmpAttr.Remarks, null, "备注", true, false, true);
                map.AddHidden(ALEmpAttr.FK_Emp, " like ", "'@WebUser.No'");
                #endregion 字段
                this._enMap = map;
                return this._enMap;
            }
        }

        /// <summary>
        /// 获取集合
        /// </summary>
        public override Entities GetNewEntities
        {
            get { return new ALEmps(); }
        }
        #endregion 构造函数
    }
    /// <summary>
    /// 员工s
    // </summary>
    public class ALEmps : EntitiesNoName
    {
        #region 构造方法
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new ALEmp();
            }
        }
        /// <summary>
        /// 员工s
        /// </summary>
        public ALEmps()
        {
        }
        public override int RetrieveAll()
        {
            return base.RetrieveAll("Name");
        }
        #endregion 构造方法
    }
}
