﻿<%@ Page Title="" Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="EditMsg.aspx.cs" ValidateRequest="false" Inherits="CCOA.App.Message.EditMsg" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        .doc-table
        {
            border-collapse: collapse;
            border-spacing: 0;
            margin-left: 3px;
            width:100%;
            display: table;
            border-color: gray;
        }
        .doc-table th
        {
            background: #EEE;
        }
        .doc-table th, .doc-table td
        {
            border: 1px solid #BDDBEF;
            padding: 3px 3px;
        }
        td.title
        {
            width: 100px;
            text-align: right;
        }
        td.text
        {
            
        }
    </style>
    <link rel="stylesheet" href="../../ctrl/kindeditor/themes/default/default.css" />
    <link rel="stylesheet" href="../../ctrl/kindeditor/plugins/code/prettify.css" />
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/kindeditor.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/lang/zh_CN.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/plugins/code/prettify.js"></script>
    <script type="text/javascript">
        KindEditor.ready(function (K) {
            var editor1 = K.create('#txtContent', {
                cssPath: '../../ctrl/kindeditor/plugins/code/prettify.css',
                uploadJson: '../../ctrl/kindeditor/asp.net/upload_json.ashx',
                fileManagerJson: '../../ctrl/kindeditor/asp.net/file_manager_json.ashx',
                allowFileManager: true
            });
            editor1.sync();
            prettyPrint();
        });
        $(function ($) {
            $(".multi-old-remove").click(function () {
                $(this).parent().fadeOut(500);
                var url = $(this).siblings(".multi-old-url").attr("href");
                var old_urls = $("#hfAttachFiles").val();
                $("#hfAttachFiles").val(old_urls.replace(url + ";", "#" + url + ";"));
            });
        });
    </script>
    <script src="../../Js/jquery.MultiFile.js" charset="gb2312" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="font-size: 35px; font-weight: bolder;">
        <a href="DraftBox.aspx"><img style="width: 40px; height: 40px;" src="img/back.png" alt="发件箱" />草稿箱</a></div>
    <div>
        <div style=" background:#eee; padding:3px 3px 3px 3px;">
        <asp:Button ID="Button2" runat="server" Text="发送" OnClick="btnSendMsg_Click" />&nbsp;&nbsp;
        <asp:Button ID="Button1" runat="server" Text="保存为草稿" OnClick="btnSaveMsg_Click" />
        </div>
        <div style="">
            <table class="doc-table">
                <tr>
                    <td class="title">
                        <span style="color:Red;">*</span>收件人:
                    </td>
                    <td>
                        <div id="div_Users" style="display: block; margin-top: 1px; padding-top: 1px;">
                            <asp:TextBox ID="txt_Users" runat="server" BackColor="#eeeeee" ReadOnly="true" Width="85%"></asp:TextBox>
                            <asp:HiddenField ID="txt_UserNames" runat="server"></asp:HiddenField>
                            <input type="button" value="添加人员" onclick="zDialog_open2('../../ctrl/SelectUsers/SelectUser_Jq.aspx', '选择人员', 650, 430,'#txt_UserNames','txt_UserNames','txt_Users');" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        抄送:
                    </td>
                    <td class="text">
                        <div id="div_CopyUsers" style="display: block; margin-top: 1px; padding-top: 1px;">
                            <asp:TextBox ID="txt_CopyUsers" runat="server" ReadOnly="true" Width="85%" BackColor="#eeeeee"></asp:TextBox>
                            <asp:HiddenField ID="txt_CopyUserNames" runat="server"></asp:HiddenField>
                            <input type="button" value="添加人员" onclick="zDialog_open2('../../ctrl/SelectUsers/SelectUser_Jq.aspx', '选择人员', 650, 430,'#txt_CopyUserNames','txt_CopyUserNames','txt_CopyUsers');" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        <span style="color:Red;">*</span>邮件主题:
                    </td>
                    <td class="text">
                       <asp:TextBox ID="txt_Title" runat="server" Width="85%" ></asp:TextBox>
                    </td>
                </tr>
                <tr style="">
                    <td class="title">
                        附件:
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hfAttachFiles" />
                        <asp:Literal runat="server" ID="liAttachFiles"></asp:Literal><br />
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="300px" CssClass="multi" />
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        <span style="color:Red;">*</span>内容:
                    </td>
                    <td class="text">
                        <asp:TextBox  ID="txtContent" TextMode="MultiLine" runat="server" Style="width: 97%; height: 200px;"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
        <div style="background: #eee; padding: 3px 3px 3px 3px;">
            <asp:Button ID="btnSendMsg" runat="server" Text="发送" OnClick="btnSendMsg_Click" />&nbsp;&nbsp;
             <asp:Button ID="btnSaveMsg" runat="server" Text="保存为草稿" OnClick="btnSaveMsg_Click" />
        </div>
    </div>
</asp:Content>
