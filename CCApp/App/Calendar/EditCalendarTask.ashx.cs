﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Text;
using BP.OA.Calendar;
namespace CCOA.App.Calendar
{
    /// <summary>
    /// EditCalendarTask 的摘要说明
    /// </summary>
    public class EditCalendarTask : IHttpHandler,IReadOnlySessionState
    {
        #region //1.Public Interface
        private string FuncNo = null;
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        private void Alert(string msg)
        {
            HttpContext.Current.Response.Write(msg);
        }
        #endregion

        private enum ActionType
        {
            Insert=0,
            Update=1,
            Delete=2
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //0.用户权限
            //if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            //1.获取用户变量
            //String curUserNo = BP.Web.WebUser.No;            
            string EventId = Convert.ToString(context.Request.Form["EventId"]);
            int OID = Convert.ToInt32(context.Request.Form["EventId"]);
            int CalendarCatagory = Convert.ToInt32(context.Request.Form["CalendarCatagory"]);
            //String FK_UserNo = String.Format("{0}", context.Request.Form["FK_UserNo"]);
            String FK_UserNo = BP.Web.WebUser.No;
            String Title = String.Format("{0}", context.Request.Form["Title"]);
            DateTime StartDate = Convert.ToDateTime(String.Format("{0}", context.Request.Form["StartDate"]));
            DateTime EndDate = Convert.ToDateTime(String.Format("{0}", context.Request.Form["EndDate"]));
            String Location = String.Format("{0}", context.Request.Form["Location"]);
            String Notes = String.Format("{0}", context.Request.Form["Notes"]);
            String Url = String.Format("{0}", context.Request.Form["Url"]);
            bool IsAllDay = Convert.ToBoolean(String.Format("{0}", context.Request.Form["IsAllDay"]));
            String Reminder = String.Format("{0}", context.Request.Form["Reminder"]);
            bool IsNew = Convert.ToBoolean(String.Format("{0}", context.Request.Form["IsNew"]));
            ActionType Action = (ActionType)Convert.ToInt32(context.Request.Form["ActionType"]);
                                //IsNew ? ActionType.Insert : ActionType.Update;
            /*测试专用
            string msg = String.Format("EventId:{0}\r\nOID:{1}\r\nFK_UserNo:{2}\r\nCalendarCatagory:{3}\r\nTitle:{4}"
                                             + "\r\nStartDate:{5}\r\nEndDate:{6}\r\nLocation:{7}\r\nNotes:{8}\r\nUrl:{9}"
                                             + "\r\nIsAllDay:{10}\r\nReminder:{11}\r\nIsNew:{12}\r\nAction:{13}\r\n"
                                             ,EventId,OID,FK_UserNo,CalendarCatagory,Title
                                             ,StartDate,EndDate,Location,Notes,Url
                                             , IsAllDay, Reminder, IsNew, Action);
            context.Response.Write(msg);
            return;
            */
            //2.数据验证
            StringBuilder sbErr = new StringBuilder();
            if (false)
            {
                sbErr.AppendFormat("{0}\r\n", "");
            }
            if (sbErr.Length > 0)
            {
                //this.Alert(sbErr.ToString());
                this.Alert("0");
                return;
            }            
            //3.数据处理
            bool blnOpe = false;
            #region //Oper1
            
            CalendarTask ct = new CalendarTask();
            if (Action == ActionType.Delete)
            {
                ct.OID = Convert.ToInt32(OID);
                try
                {
                    blnOpe = ct.Delete() == 1;
                }
                catch (Exception ex)
                {
                    context.Response.Write(ex.Message);
                    return;
                }
            }
            else
            {
                if (Action == ActionType.Update)
                {
                    ct.OID = Convert.ToInt32(OID);
                    ct.RetrieveFromDBSources();
                }
                ct.FK_UserNo = FK_UserNo;
                ct.Title = Title;
                ct.StartDate = StartDate;
                ct.EndDate = EndDate;
                ct.Location = Location;
                ct.Notes = Notes;
                ct.Url = Url;
                ct.IsAllDay = IsAllDay;
                ct.Reminder = Reminder;
                ct.IsNew = IsNew;
                ct.CalendarCatagory = CalendarCatagory;
                if (Action == ActionType.Update)
                {
                    try
                    {
                        blnOpe = ct.Update() == 1;
                    }
                    catch (Exception ex)
                    {
                        context.Response.Write(ex.Message);
                        return;
                    }
                }
                else
                {
                    try
                    {
                        ct.Insert();
                    }
                    catch (Exception ex)
                    {
                        context.Response.Write(ex.Message);
                        return;
                    }
                    blnOpe = ct.OID > 0;
                    EventId = ct.OID.ToString();
                }
            }
            #endregion
            //4.统计/日志
            //5.提示处理信息
            if (blnOpe)
            {
                if (Action == ActionType.Insert)
                    this.Alert(EventId);   //添加
                else
                    this.Alert("1");          //修改/删除文章成功
            }
            else
                this.Alert("0");              //添加/修改/删除文章失败
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}