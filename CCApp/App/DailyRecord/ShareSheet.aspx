﻿<%@ Page Title="" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true"  EnableEventValidation="false" MaintainScrollPositionOnPostback="true"
    CodeBehind="ShareSheet.aspx.cs" Inherits="CCOA.App.DailyRecord.SheareSheet" %>
<%@ Register Src="../Pub.ascx" TagName="Pub" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script type="text/javascript" src="../../Js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">

    function stop() {

        var oEvent = event;
        oEvent.cancelBubble = true; //阻止事件冒泡
    }

    function ok(ele) {
        $(ele).next().find("TextBox").val("");
        $(ele).next().show("slow");

        document.onclick = function (event) {
            var e = event || window.event;
            var elem = e.srcElement || e.target;
            while (elem) {
                if (elem != document) {

                    if (elem.id == "a") {

                        $(ele).next().show("slow");

                        return;
                    }
                    elem = elem.parentNode;
                } else {

                    $(ele).next().hide("slow");
                    return;
                }
            }
        }
    }

   

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style=" float:right"> 姓名:<asp:TextBox  ID="tb_find" runat="server" ></asp:TextBox>
    <asp:Button ID="submit"
    runat="server" Text="查询" onclick="submit_Click" />
</div>
    <table style="width: 100%; border-spacing:5px; border-collapse:separate;table-layout:fixed; word-wrap:break-word">
        <asp:Repeater ID="rep_List" runat="server">
            <ItemTemplate>
                <tr>
                    <td rowspan="2" valign="top" style="width:100px">
                        <img src='/DataUser/UserIcon/<%#Eval("Rec")%>.png' width='100px' onerror="this.src='/DataUser/UserIcon/Default.png'"
                            border="0" /><br />
                              <a href="PublicSheet.aspx?Rec=<%#Eval("Rec")%>">
                   <h4 style="text-align:center;  font-size:15px; font-weight:normal"> <%#GetName(Convert.ToString( Eval("Rec")))%></h4></a>  
                    </td>
                    <td>
                      <div style="float:left; font-weight:bold; font-size:15px;word-wrap:break-word;word-break:break-all"> <h3>      <img src="./Img/<%#Eval("DRShe")%>.png" class="Icon" />  <%#Eval("Title")%> </h3></div> 
                      <div style="float:right;  font-weight:bold; font-size:15px; color:Green" >
          <%#Eval("RDT") %>&nbsp&nbsp<%#GetWeekNameOfDay( Convert.ToDateTime( Eval("RDT")))%></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <%#Eval("Doc")%>
                            <br>
                            </br>
                            <br>
                            </br>
                             <a href=  "javascript:void(0)" onclick=" ok(this) " id="a">评论</a>
                            <div id="pp<%# Container.ItemIndex %>"  style="width: 200px; height: 50px; display: none;">
                                <asp:TextBox ID="TB_Com" onclick="stop()" runat="server" TextMode="MultiLine"   Width="600"  Height="45" > </asp:TextBox>
                                <asp:Button ID="sbmt" runat="server" Text="发表" OnClick="btnSub_Click" />
                            </div>
                            <br />
                            <br />
                        </div>
                     
                           <!--由于Repeater控件没有如GridView的DataKeyNames属性，因此放一个HiddenField控件-->
                        <div>
                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("OID") %>' />
                              <asp:Repeater ID="sublist" runat="server" DataSource='<%# getdt(Eval("OID").ToString()) %>'>
                                 <ItemTemplate>
                                  <h5 style="font-size: 15px; font-weight: normal; height: 5px">
                                        &nbsp&nbsp&nbsp<a style=" color:Green"><%#Eval("Critic") %>:</a><%# BP.DA.DataType.ParseText2Html(Convert.ToString( Eval("Comm")))%>&nbsp&nbsp<a style=" font-size:10px"><%#Eval("CommDate") %></a></h5><br /><br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <hr>
    <uc1:Pub ID="Pub1" runat="server" />
</asp:Content>
