using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.Demo
{
    /// <summary>
    /// 报销流程 - 开始节点.
    /// </summary>
    public class ND904 : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 报销流程事件
        /// </summary>
        public ND904()
        {
        }
        #endregion 属性.

        #region 重写属性.
        public override string FlowMark
        {
            get { return BP.DS.API.DailySupplie_Buy_FlowMark; }
        }
        //public override string NodeMarks
        //{
        //    get { return "03"; }
        //}
        #endregion 重写属性.

        #region 重写节点表单事件.
        /// <summary>
        /// 表单载入前
        /// </summary>
        public override string FrmLoadAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单载入后
        /// </summary>
        public override string FrmLoadBefore()
        {
            return null;
        }
        /// <summary>
        /// 表单保存后
        /// </summary>
        public override string SaveAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单保存前
        /// </summary>
        public override string SaveBefore()
        {

            return null;

        }
        #endregion 重写节点表单事件

        #region 重写节点运动事件.
        /// <summary>
        /// 发送前:用于检查业务逻辑是否可以执行发送，不能执行发送就抛出异常.
        /// </summary>
        public override string SendWhen()
        {
            //获取设计器节点表单字段,进行判断
            string BiaoTi = this.GetValStr("BiaoTi");
            if (BiaoTi == "")
                throw new Exception("标题不可为空");

            int num = this.GetValInt("ShuLiang");
            if (num == 0)
                throw new Exception("数量不可为空");

            float JinE = this.GetValInt("JinE");
            if (JinE == 0)
                throw new Exception("金额不可为空");

            return "合计已经在发送前事件完成.";
        }
        /// <summary>
        /// 发送成功后
        /// </summary>
        public override string SendSuccess()
        {
            //BP.OA.DS.API.DailySupplie_Buy(this.GetValStr("OA_DSMain"),
            //    this.GetValInt("ShuLiang"),
            //    this.GetValInt("JinE"),
            //    this.GetValStr("GongYingShang"),
            //    BP.Web.WebUser.Name,
            //    this.GetValStr("ShuoMing"));
            return "写入成功....";
        }
        /// <summary>
        /// 发送失败后
        /// </summary>
        public override string SendError()
        {
            return null;
        }
        /// <summary>
        /// 退回前
        /// </summary>
        public override string ReturnBefore()
        {
            return null;
        }
        /// <summary>
        /// 退回后
        /// </summary>
        public override string ReturnAfter()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
