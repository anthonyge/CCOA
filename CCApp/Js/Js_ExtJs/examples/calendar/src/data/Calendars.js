Ext.define('Ext.calendar.data.Calendars', {
    statics: {
        getData: function(){
            return {
                "calendars":[{
                    "id": 1,
                    "title": "回家"
                }, {
                    "id": 2,
                    "title": "工作"
                }, {
                    "id": 3,
                    "title": "上学"
                }, {
                    "id": 4,
                    "title": "约会"
                },{
                    "id": 5,
                    "title": "休假"
                }]
            };    
        }
    }
});