﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.BH.EventEntity
{
    /// <summary>
    /// 社情业务流转 流程事件实体
    /// </summary>
    public class SqWorkFlowFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 社情业务流转 流程事件实体
        /// </summary>
        public SqWorkFlowFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return "SqWorkFlow"; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            //把领用信息写入api.
            API.SqWorkFlow(
               this.GetValStr("ShiJianBiaoTi"),
               this.GetValStr("ShiJianLeiBie"),
               this.GetValStr("Port_Dept"),
               this.GetValStr("PaiChaRenXingMing"),
               this.GetValStr("PaiChaRenYuanDianHua"),
               //this.GetValStr("PaiChaShiJian"),
               this.GetValStr("PaiChaShiJian"),
               this.GetValStr("WTJTDZ"),
               this.GetValStr("WTMS"),
                this.GetValStr("BanLiFangShi")
               );
            return "写入成功....";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
