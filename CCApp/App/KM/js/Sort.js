﻿if ($.fn.datagrid) {
    $.fn.datagrid.defaults.loadMsg = '正在处理，请稍待。。。';
}
if ($.fn.treegrid && $.fn.datagrid) {
    $.fn.treegrid.defaults.loadMsg = $.fn.datagrid.defaults.loadMsg;
}
if ($.messager) {
    $.messager.defaults.ok = '确定';
    $.messager.defaults.cancel = '取消';
}
//当前模式
var curModel = "emp";
//加载节点树
function LoadSortTree() {
    $("#pageloading").show();
    OA.data.getKMSorts(0, "false", showMsSortTree, this);
}
//加载类别
function showMsSortTree(js, scope) {
    if (js.readyState && js.readyState == 4) js = "[]";
    var pushData = eval('(' + js + ')');
    //加载类别树
    $("#kmSortTree").tree({
        data: pushData,
        iconCls: 'tree-folder',
        collapsed: true,
        lines: true,
        onExpand: function (node) {
            if (node) {
                LoadChildNodes(node, false);
            }
        },
        onClick: function (node) {
            if (node) {
                LoadChildNodes(node, true);
            }
        }
    });
    $("#kmSortTree").bind('contextmenu', function (e) {
        e.preventDefault();
        $('#treeMM').menu('show', {
            left: e.pageX,
            top: e.pageY
        });
    });
    $("#pageloading").hide();
}

//加载子节点
function LoadChildNodes(node, expand) {
    var childNodes = $('#menuTree').tree('getChildren', node.target);
    if (childNodes && childNodes.length > 0 && childNodes[0].text == '加载中...') {
        $('#kmSortTree').tree('remove', childNodes[0].target);
        OA.data.getKMSorts(node.id, "true", function (js) {
            if (js && js != '[]') {
                var pushData = eval('(' + js + ')');
                $('#kmSortTree').tree('append', { parent: node.target, data: pushData });
                if (expand) $('#kmSortTree').tree('expand', node.target);
            }
            GetTemplatePanel(node);
        }, this);
    } else {
        GetTemplatePanel(node);
    }
}

//获取知识树节点的权限
function GetTemplatePanel(node) {
    if (node) {
        OA.data.getTemplateData(node.id, curModel, function (js, scope) {
            if (js) {
                var data = eval('(' + js + ')');
                var panel = document.getElementById('templatePanel');
                panel.innerHTML = '正在加载 ......';
                try { eval(data); } catch (e) { alert('Data Format Error!'); }
                //默认按人员分配菜单模版
                var template = " <#macro userlist data> "
                             + " <#list data.bmList as bmList> "
                             + " <caption>${bmList.Name}</caption><hr>"
                             + " <#list data.empList as empList> "
                             + " <#if (empList.FK_Dept==bmList.No)>"
                             + "   <#if (empList.isCheck==1)>"
                             + "       <input type='checkbox' checked='true'  name='ckgroup'  id='${empList.No}'> ${empList.Name} "
                             + "   <#else>"
                             + "       <input type='checkbox'   name='ckgroup'  id='${empList.No}'> ${empList.Name} "
                             + "   </#if>"
                             + " </#if>"
                             + " </#list> "
                             + " <br><br> "
                             + " </#list>"
                             + " </#macro>";
                //按岗位分配模版
                if (curModel == "station") {
                    template = " <#macro stationlist data> "
                             + " <#list data.station as stationlist> "
                             + " <#if (stationlist.isCheck==1)>"
                             + "     <input type='checkbox' checked='true' name='ckgroup'  id='${stationlist.No}'> ${stationlist.Name} <hr>"
                             + " <#else>"
                             + "     <input type='checkbox' name='ckgroup' id='${stationlist.No}'> ${stationlist.Name} <hr>"
                             + " </#if>"
                             + " </#list>"
                             + " </#macro>";
                }
                //按部门分配模版
                if (curModel == "dept") {
                    template = " <#macro deptlist data> "
                             + " <#list data.dept as deptList> "
                             + " <#if (deptList.isCheck==1)>"
                             + "     <input type='checkbox' checked='true' name='ckgroup'  id='${deptList.No}'> ${deptList.Name} <hr>"
                             + " <#else>"
                             + "     <input type='checkbox' name='ckgroup' id='${deptList.No}'> ${deptList.Name} <hr>"
                             + " </#if>"
                             + " </#list>"
                             + " </#macro>";
                }
                //得到内容
                var source = easyTemplate(template, data);
                panel.innerHTML = source;
            }
        }, this);
    }
}

//全选
function CheckedAll() {
    var checkedSta = false;
    var ckbAll = document.getElementById("ckbAllText");

    var selected = $('#kmSortTree').tree('getSelected');
    //如果没有选择项就返回
    if (!selected) return;

    if (ckbAll.innerHTML == "全选") {
        checkedSta = true;
        ckbAll.innerHTML = "清空";
    } else {
        ckbAll.innerHTML = "全选";
    }
    var elts = document.getElementsByTagName("input");
    for (var i = 0; i < elts.length; i++) {
        if (elts[i].getAttribute("name") == "ckgroup") {
            elts[i].checked = checkedSta;
        }
    }

}

//创建同级目录
function CreateSampleNode() {
    var node = $('#kmSortTree').tree('getSelected');
    if (node) {
        OA.data.treeSortManage("sample", node.id, function (js) {
            if (js) {
                LoadSortTree();
                var selectNode = $('#kmSortTree').tree('find', node.id);
                $('#kmSortTree').tree('select', selectNode.target);
            }

        }, this);
    } else {
        CC.Message.showError("提示", "请选择节点。");
    }
}
//创建下级目录
function CreateSubNode() {
    var node = $('#kmSortTree').tree('getSelected');
    if (node) {
        OA.data.treeSortManage("children", node.id, function (js) {
            if (js) {
                LoadSortTree();
                var selectNode = $('#kmSortTree').tree('find', node.id);
                $('#kmSortTree').tree('select', selectNode.target);
            }

        }, this);
    } else {
        CC.Message.showError("提示", "请选择节点。");
    }
}

//修改
function EditNode() {
    var node = $('#kmSortTree').tree('getSelected');
    if (node) {
        $("#sortName").val(node.text);
        //弹出窗体
        $('#treeSortDialog').dialog({
            title: "修改节点名称",
            width: 300,
            height: 130,
            modal: true,
            resizable: true,
            buttons: [{
                text: '确定',
                iconCls: 'icon-ok',
                handler: function () {
                    var sortName = $("#sortName").val();
                    if (sortName == "") {
                        CC.Message.showError("提示", "知识类型名称不能为空。");
                        return;
                    }
                    OA.data.updateSortName(node.id, sortName, function (js) {
                        if (js == "true") {
                            $('#kmSortTree').tree('update', { target: node.target, text: sortName });
                            $('#treeSortDialog').dialog('close');
                        } else {
                            CC.Message.showError("提示", "修改知识类型名称失败。");
                        }
                    }, this);
                }
            }, {
                text: '取消',
                handler: function () {
                    $('#treeSortDialog').dialog('close');
                }
            }]
        });
    } else {
        CC.Message.showError("提示", "请选择节点。");
    }
}

//删除节点
function DeleteNode() {
    var node = $('#kmSortTree').tree('getSelected');
    if (node) {
        //消息提醒
        $.messager.confirm('提示', "确定删除所选节点？", function (r) {
            if (r) {
                OA.data.treeSortManage("delete", node.id, function (js) {
                    LoadSortTree();
                }, this);
            }
        });
    } else {
        CC.Message.showError("提示", "请选择节点。");
    }
}

//上移
function DoUp() {
    var node = $('#kmSortTree').tree('getSelected');
    if (node) {
        OA.data.treeSortManage("doup", node.id, function (js) {
            LoadSortTree();
        }, this);
    } else {
        CC.Message.showError("提示", "请选择节点。");
    }
}
//下移
function DoDown() {
    var node = $('#kmSortTree').tree('getSelected');
    if (node) {
        OA.data.treeSortManage("dodown", node.id, function (js) {
            LoadSortTree();
        }, this);
    } else {
        CC.Message.showError("提示", "请选择节点。");
    }
}

//保存权限
function SaveSortRight() {
    var saveNos = "";
    var selected = $('#kmSortTree').tree('getSelected');
    
    if (selected) {
        //获取选择项
        var elts = document.getElementsByTagName("input");
        for (var i = 0; i < elts.length; i++) {
            if (elts[i].getAttribute("name") == "ckgroup") {
                if (elts[i].checked) {
                    saveNos += elts[i].id + ",";
                }
            }            
        }

        if (saveNos.length > 0)
            saveNos = saveNos.substring(0, saveNos.length - 1);
        
        var childNodes = $('#kmSortTree').tree('getChildren', selected.target);
        if (childNodes && childNodes.length > 0) {
            //消息提醒
            $.messager.confirm('提示', "所选知识类别下含有子项，是否对子项进行授权？", function (r) {
                if (r) {
                    SaveRightData(selected.id, saveNos, true);
                }
                else {
                    SaveRightData(selected.id, saveNos, false);
                }
            });
        }
        else {
            SaveRightData(selected.id, saveNos, false);
        }

    } else {
        CC.Message.showError("系统提示", "请选择知识类别！");
    }
}
//执行保存
function SaveRightData(TreeNo, ckNos, saveChildNode) {
    $("#pageloading").show();
    //保存菜单权限
    OA.data.saveSortRights(TreeNo, ckNos, curModel, saveChildNode, function (js, scope) {
        if (js) {
            CC.Message.showError("系统提示", "保存成功！");
        }
        $("#pageloading").hide();
    }, this);
}

//何种模式分配权限
function DistributeRight(model) {
    curModel = model;
    if (curModel == "dept") {
        $("#curModelText").html("当前选择按部门分配权限");
    }
    if (curModel == "station") {
        $("#curModelText").html("当前选择按岗位分配权限");
    }
    if (curModel == "emp") {
        $("#curModelText").html("当前选择按人员分配权限");
    }
    var selected = $('#kmSortTree').tree('getSelected');
    //菜单如果有选择项就刷新
    if (selected) GetTemplatePanel(selected);
}

//初始化
$(function () {
    LoadSortTree();
});