﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

namespace CCOA.App.Notice
{
    public partial class MyNoteListEUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.BindMyReadState();
                //清除即时消息
                //BP.OA.ShortMsg.ReceivedInfo(null, "公告");
            }
        }
        [WebMethod]
        public static string GetReadState(object evalState)
        {
            if (evalState == null || evalState == Convert.DBNull) return String.Empty;
            int state = Convert.ToInt32(evalState);
            if (state == 0)
                return NotReadStateLabel;
            else if (state == 1)
                return ReadStateLabel;
            else
                return String.Empty;
        }
        [WebMethod]
        public static string GetRelativeTime(string evalTime)
        {
           
            return BP.OA.Main.GetTimeEslapseStr(Convert.ToDateTime(evalTime), null, null);

 
        }
        //我的公告-最顶上公告阅读提示方案
        private string NotReadTopMsg = "<span style='color:red'>目前未读信息有{0}条！</span>";
        private string ReadTopMsg = "<span style='color:#333'>目前没有未读信息！</span>";
        //我的公告-阅读状态栏格式方案
        private static string NotReadStateLabel = "<span style='color:red'>未读</span>";
        private static string ReadStateLabel = "<span style='color:Green'>已读</span>";
        //我的公告-标题状态格式方案
        private string NotReadStateTitleStyle = "color:#333333;";
        private string ReadStateTitleStyle = "color:gray;text-decoration:line-through;";
        private int GetNotReadCount(string userNo)
        {
            string sSql = String.Format("select count(*) from OA_NoticeReader A "
                + "   inner join OA_Notice B on A.FK_NoticeNo=B.OID"
                + "   where A.FK_UserNo='{0}' and B.NoticeSta=1 and ReadTime=''"
                + "      or A.FK_UserNo='{0}' and B.NoticeSta=0 and ReadTime='' "
                + "         and GetDate() between B.StartTime and B.StopTime", userNo);
            int notRead = Convert.ToInt32(BP.DA.DBAccess.RunSQLReturnVal(sSql));
            return notRead;
        }
        public void BindMyReadState()
        {
            int notRead = this.GetNotReadCount(BP.Web.WebUser.No);
            if (notRead > 0)
                this.liMyReadState.Text = String.Format(this.NotReadTopMsg, notRead);
            else
                this.liMyReadState.Text = String.Format(this.ReadTopMsg);
        }
    }
}