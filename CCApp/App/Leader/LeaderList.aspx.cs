﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace CCOA.App.OA
{
    public partial class LeaderList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // 演示矩阵输出. 也叫N宫格输出.
            BP.OA.Leaders ens = new BP.OA.Leaders();
                        
            ens.RetrieveAll();
            int cols = 4; //定义显示列数 从0开始。
            decimal widthCell = 100 / cols;
            this.Pub1.AddTable("width=100% border=0");
            if (BP.Web.WebUser.No=="admin")
                this.Pub1.AddCaption("<img src='all.png' class=Icon border=0/>领导风采 | <a href='RiCheng.aspx'><img src='rili.png' class=Icon border=0/>领导日程</a> | <a href='/Comm/Search.aspx?EnsName=BP.OA.Leaders' target=_self ><img src='setup.png' class=Icon border=0/>领导维护</a>");
            else
                this.Pub1.AddCaption("<img src='all.png' class=Icon border=0/>领导风采 | <a href='RiCheng.aspx'><img src='rili.png' class=Icon border=0/>领导日程</a> ");
            int idx = -1;
            bool is1 = false;
            foreach (BP.OA.Leader en in ens)
            {
                idx++;
                if (idx == 0)
                    is1 = this.Pub1.AddTR(is1);
                this.Pub1.AddTDBegin("width='" + widthCell + "%' border=0 valign=top");
                #region 输出内容.
                this.Pub1.Add("<img src='" + en.WebPath + "'style='with:200px;height:300px; line-height:28px;' >");
                this.Pub1.Add("<h2 style='line-height:28px;'>姓名:" + en.Name + "</h2>");
                this.Pub1.Add("职务:" + en.Duty);
                this.Pub1.Add("<br><a style='line-height:28px;' href=\"javascript:OpenDtl('" + en.No + "');\" ><b>打开日程</b></a>");
                this.Pub1.Add("<br>简历:" + en.JianLi.Substring(0,20)+"...");
                #endregion 输出内容.
                this.Pub1.AddTDEnd();
                if (idx == cols - 1)
                {
                    idx = -1;
                    this.Pub1.AddTREnd();
                }
            }

            while (idx != -1)
            {
                idx++;
                if (idx == cols - 1)
                {
                    idx = -1;
                    this.Pub1.AddTD();
                    this.Pub1.AddTREnd();
                }
                else
                {
                    this.Pub1.AddTD();
                }
            }
            this.Pub1.AddTableEnd();
        }
    }
}