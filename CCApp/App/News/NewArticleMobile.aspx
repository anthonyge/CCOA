﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Mobile.Master" AutoEventWireup="true"
    CodeBehind="NewArticleMobile.aspx.cs" Inherits="CCOA.App.News.NewArticleMobile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .article_intro
        {
            padding: 10px 10px 10px 10px;
            background-color: #eeeeee;
            color: #444444;
        }
        .ari
        {
            font-weight: bold;
        }
        .arl
        {
        }
        div.article
        {
            padding-left: 10px;
            min-height: 300px;
        }
        p.article p
        {
            min-height: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div data-role="page" data-theme="c">
        <div data-role="header" data-position="fixed" data-theme="b">
            <a href="NewsListMobile.aspx" data-icon="black" data-role="button ">返回</a>
              <h2>新闻内容</h2>
        </div>
        <div class="ui-content" data-role="main" style="padding: 0px;">
           
            <p style="font-size: 25px; font-weight: bolder; padding-left: 10px; text-align: center;">
                <asp:Label runat="server" ID="lblTitle"></asp:Label>
            </p>
            <p class="article_intro">
                <span class="ari">作者：</span><asp:Label runat="server" ID="lblUser" CssClass="arl"></asp:Label><br /><span class="ari">
                    发布时间：</span><asp:Label runat="server" ID="lblAddTime" CssClass="arl"></asp:Label>                    
            </p>
            <div class="article">
                <asp:Literal ID="liContent" runat="server"></asp:Literal>
            </div>
            <asp:Panel runat="server" ID="pnAttachFile" Visible="false" Style="border-top: dashed 1px #777;">
        &nbsp;&nbsp;&nbsp;<img src="img/attach1.png" style="height: 18px;" alt="附件">附件下载(右击另存下载)：
       <asp:Literal runat="server" ID="liAttachFiles"></asp:Literal>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
