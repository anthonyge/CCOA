﻿using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class ArticleCatagoryAttr : EntityNoNameAttr
    {
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public class ArticleCatagory : EntityNoName
    {
        #region 属性
        #endregion

        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public ArticleCatagory() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public ArticleCatagory(string no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {

            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        protected override bool beforeDelete()
        {
            int rowsCount = DBAccess.RunSQLReturnValInt("SELECT COUNT(*) FROM OA_Article WHERE FK_ArticleCatagory='"+this.No+"'", 0);
            if (rowsCount > 0)
            {
                throw new Exception("此类型下包含数据，请先删除相应公告后再进行删除此类型！");
            }
            return base.beforeDelete();
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_ArticleCatagory";
                map.EnDesc = "新闻类别";   // 实体的描述.
                map.CodeStruct = "2";
                map.IsAllowRepeatName = false;
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.

                map.AddTBStringPK(ArticleCatagoryAttr.No, null, "编号", true, true, 2, 2, 20);
                map.AddTBString(ArticleCatagoryAttr.Name, null, "名称", true, false, 1, 100, 30);

                //map.AddTBString(ArticleCatagoryAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(ArticleCatagoryAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class ArticleCatagorys : EntitiesNoName
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new ArticleCatagory();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public ArticleCatagorys()
        {
        }
    }
}