using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.WebLog
{
    /// <summary>
    /// 日志薄 属性
    /// </summary>
    public class DRSheetAttr : EntityOIDAttr
    {
        /// <summary>
        /// 日志内容
        /// </summary>
        public const string Doc = "Doc";
        /// <summary>
        /// 类型
        /// </summary>
        public const string DRType = "DRType";
        /// <summary>
        /// 状态
        /// </summary>
        public const string DRSta = "DRSta";
        /// <summary>
        /// 提交人
        /// </summary>
        public const string Rec = "Rec";
        /// <summary>
        /// 记录时间
        /// </summary>
        public const string RDT = "RDT";
        /// <summary>
        /// 部门
        /// </summary>
        public const string FK_Dept = "FK_Dept";
        /// <summary>
        /// 日志日期
        /// </summary>
        public const string DRDate = "DRDate";
    }

    /// <summary>
    /// 日志薄
    /// </summary>
    public partial class DRSheet : EntityOID
    {
        #region 属性
        /// <summary>
        /// 内容
        /// </summary>
        public string Doc
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.Doc);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.Doc, value);
            }
        }
        /// <summary>
        /// 日志类型
        /// </summary>
        public string DRType
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.DRType);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.DRType, value);
            }
        }
        /// <summary>
        /// 日志状态
        /// </summary>
        public string DRSta
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.DRSta);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.DRSta, value);
            }
        }

        /// <summary>
        /// 记录人
        /// </summary>
        public string Rec
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.Rec);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.Rec, value);
            }
        }
        /// <summary>
        /// 记录时间
        /// </summary>
        public string RDT
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.RDT);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.RDT, value);
            }
        }
        /// <summary>
        /// 部门
        /// </summary>
        public string FK_Dept
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.FK_Dept);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.FK_Dept, value);
            }
        }
        /// <summary>
        /// 日期
        /// </summary>
        public string date
        {
            get
            {
                return this.GetValStringByKey(DRSheetAttr.DRDate);
            }
            set
            {
                this.SetValByKey(DRSheetAttr.DRDate, value);
            }
        }
        #endregion
        #region 构造函数
        /// <summary>
        /// 日志薄
        /// </summary>
        public DRSheet() { }
        /// <summary>
        /// 日志薄
        /// </summary>
        /// <param name="no">编号</param>
        public DRSheet(int oid) : base(oid) { }
        #endregion
        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.Readonly();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map();
                map.PhysicsTable = "ZZ_DRSheet"; //物理表名
                map.EnDesc = "日志薄";   // 实体的描述.
                //基本信息
                map.AddTBIntPKOID();
                map.AddTBStringDoc(DRSheetAttr.Doc, null, "内容", true, true);
                map.AddDDLSysEnum(DRSheetAttr.DRSta, 0, "日志状态", true, true,
                    DRSheetAttr.DRSta, "@0=正常@1=补写");
                map.AddTBDate(DRSheetAttr.DRDate, null, "日期", true, false);
                map.AddDDLEntities(DRSheetAttr.DRType, null, "日志类型", new BP.WebLog.LogTypes(), true);
                map.AddDDLEntities(DRSheetAttr.Rec, null, "记录人", new BP.Port.Emps(), true);
                map.AddTBString(DRSheetAttr.Rec, null, "记录人", true, false, 0, 100, 30);
                map.AddTBDateTime(DRSheetAttr.RDT, null, "记录时间", true, false);
                map.AddDDLEntities(DRSheetAttr.FK_Dept, null, "部门", new BP.Port.Depts(), true);
                //map.DTSearchKey = DRSheetAttr.RDT;
                //map.DTSearchWay = Sys.DTSearchWay.ByDate;
                map.AddSearchAttr(DRSheetAttr.FK_Dept);
                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
        //protected override bool beforeInsert()
        //{

        //}
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class DRSheets : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DRSheet();
            }
        }
        /// <summary>
        /// 日志薄集合
        /// </summary>
        public DRSheets()
        {
        }
    }
}
